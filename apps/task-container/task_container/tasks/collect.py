from collect.services.fmp import *


def saveBatchEODPrice(startDatetime, endDatetime=datetime.now().strftime("%Y%m%d")):
    saveBatchEODPrices(startDatetime, endDatetime)


def testFunction(a, b, c):
    print(f"task function testFunction({a}, {b}, {c}) is called!")
