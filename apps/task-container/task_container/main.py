import argparse
from . import tasks


def run():
    parser = argparse.ArgumentParser(description="Task container.")
    parser.add_argument("--taskId", nargs=1, type=str, help="the id of task")
    parser.add_argument("--params", action="extend", nargs="+", type=str, help="params")

    args = parser.parse_args()

    functionName = args.taskId[0].strip()
    params = args.params
    print(params)

    print(f"{functionName} is running!")
    print("possible tasks", tasks.__dict__)
    if params is None:
        getattr(tasks, functionName)()
    else:
        getattr(tasks, functionName)(*params)
    print(f"{functionName} is done!")
