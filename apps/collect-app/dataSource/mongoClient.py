import pymongo
from dataSource.envs import getEnv


def getDBName():
    return getEnv("STOCK_DB_NAME")


def makeIndex():
    mc[getDBName()]["collectService_dartFinancialStatements"].create_index(
        [
            "재무제표종류",
            "항목코드",
            "항목명",
            "결산기준일",
            "보고서종류",
            "종목코드",
            "file",
        ],
        unique=True,
    )


def getDBAddress():
    return getEnv("EXTERNAL_MONGO_DATABASE_PATH")


def getClient():
    return mc


def getDB():
    return mc[getDBName()]


def getCollection(collection):
    return getDB()[collection]


try:
    mc = pymongo.MongoClient(getEnv("EXTERNAL_MONGO_DATABASE_PATH"))
    makeIndex()
except Exception as e:
    print(e)
