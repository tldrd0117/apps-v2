from dotenv import load_dotenv, dotenv_values
import os

load_dotenv(verbose=True, override=True)
config = dotenv_values(".remote.env")


def getEnv(key):
    if key in config:
        return config[key]
    if key in os.environ:
        return os.environ[key]
    return None
