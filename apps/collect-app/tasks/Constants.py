from enum import Enum

EACH_SYMBOL_DIR = "resources/usahistory/eachsymbol"
EACH_SYMBOL_HISTORY_DIR = "resources/usahistory/eachsymbol/history"
EACH_YEAR_DIR = "resources/usahistory/eachyear"
EACH_YEAR_HISTORY_DIR = "resources/usahistory/eachyear/history"
USA_HISTORY_DIR = "resources/usahistory"
PORTPOLIO_DIR = "resources/portfolios"


class EntityType(Enum):
    USA_STOCK = "usaStock"
    CRYPTO = "crypto"


def getSymbolCollectionName(entityType: EntityType):
    if entityType == EntityType.CRYPTO:
        return "binance_crypto_symbols"
    if entityType == EntityType.USA_STOCK:
        return "fmp_stock_symbols"
    return None


def getHistoryCollectionName(entityType: EntityType):
    if entityType == EntityType.CRYPTO:
        return "binance_crypto_price"
    if entityType == EntityType.USA_STOCK:
        return "fmp_usa_stock_history"
    return None


def getBaseDir(entityType: EntityType):
    if entityType == EntityType.CRYPTO:
        return "resources/cryptohistory"
    if entityType == EntityType.USA_STOCK:
        return "resources/usahistory"
    return None
