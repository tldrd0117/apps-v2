from tasks.baseTask import BaseTask, WrapperBaseTask
from datetime import datetime, date
import luigi
from tasks.targets.MongoFindTarget import MongoFindTarget
from tasks.convert.downloadable.common.services.convert_history_downloadable import (
    convertToDownloadableEachSymbol,
    convertToDownloadableEachYear,
)
from dataSource.mongoClient import getClient, getDB, getDBName
from tasks.Constants import EntityType


class ConvertHistoryDownloadableTask(BaseTask):
    workers = 1
    today = luigi.DateParameter(default=date.today())
    entityType = luigi.Parameter(default="usaStock")

    def getKey(self):
        if self.today is None:
            self.today = date.today()
        return self.today.strftime("%Y%m%d")

    def run(self):
        convertToDownloadableEachSymbol(EntityType(self.entityType))
        convertToDownloadableEachYear(EntityType(self.entityType))
        self.output().write({self.getKey(): 1})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "common_ConvertHistoryDownloadableTask",
            [self.getKey()],
            "lastUpdateDate",
        )
