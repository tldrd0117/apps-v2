from tasks.convert.downloadable.common.services.convert_history_downloadable import (
    loadSymbols,
    convertToDownloadableEachSymbol,
    convertToDownloadableEachYear,
)
from tasks.Constants import EntityType


def test_loadSymbols():
    symbols = loadSymbols(EntityType.CRYPTO)
    print(len(symbols))
    symbols = loadSymbols(EntityType.USA_STOCK)
    print(len(symbols))


def test_eachsymbol():
    convertToDownloadableEachSymbol(EntityType.CRYPTO)


def test_eachyear():
    convertToDownloadableEachYear(EntityType.CRYPTO)
