from dataSource.mongoClient import getCollection
import glob
import pathlib
from datetime import datetime, date, timedelta
import polars as pl
import os
from tasks.utils.dateUtils.dateUtils import getTodayDatetime

EACH_SYMBOL_DIR = "resources/usahistory/eachsymbol"
EACH_YEAR_DIR = "resources/usahistory/eachyear"
USA_HISTORY_DIR = "resources/usahistory"

HISTORY_DTYPE = {
    "_id": pl.Utf8,
    "date": pl.Datetime,
    "open": pl.Float64,
    "high": pl.Float64,
    "low": pl.Float64,
    "close": pl.Float64,
    "adjClose": pl.Float64,
    "volume": pl.Int64,
    "unadjustedVolume": pl.Int64,
    "change": pl.Float64,
    "changePercent": pl.Float64,
    "vwap": pl.Float64,
    "label": pl.Utf8,
    "changeOverTime": pl.Float64,
    "symbol": pl.Utf8,
}


def loadSymbolsFromUSAHistory(today=getTodayDatetime()):
    collection = getCollection("fmp_usa_stock_history")
    files = glob.glob(f"{USA_HISTORY_DIR}/symbols/*")
    filenames = [datetime.strptime(pathlib.Path(file).name, "%Y%m%d") for file in files]
    print("loadSymbolsFromUSAHistory", str(filenames), str(today))
    symbols = None
    if len(filenames) == 0 or max(filenames) < today:
        cursor = collection.distinct("symbol")
        symbols = list(cursor)
        pl.DataFrame({"symbol": symbols}).write_parquet(
            f"{USA_HISTORY_DIR}/symbols/{today.strftime('%Y%m%d')}"
        )
    else:
        symbols = pl.read_parquet(
            f"{USA_HISTORY_DIR}/symbols/{today.strftime('%Y%m%d')}"
        )["symbol"].to_list()
    print(len(symbols))
    return symbols


def prepareDirs():
    os.makedirs(f"{EACH_SYMBOL_DIR}/history", exist_ok=True)
    os.makedirs(f"{USA_HISTORY_DIR}/symbols", exist_ok=True)
    os.makedirs(f"{EACH_YEAR_DIR}/history", exist_ok=True)


def convertUSAStockHistoryToDownloadableEachSymbol(today=getTodayDatetime()):
    prepareDirs()
    collection = getCollection("fmp_usa_stock_history")
    startDate = datetime.strptime("19000101", "%Y%m%d")
    endDate = datetime.strptime("21000101", "%Y%m%d")
    symbols = loadSymbolsFromUSAHistory(today)

    files = glob.glob(f"{EACH_SYMBOL_DIR}/history/*")
    filenames = [pathlib.Path(file).name for file in files]
    requestList = {}
    fileList = {}
    for symbol in symbols:
        requestList[symbol] = [startDate, endDate]

    for filename in filenames:
        data = filename.split("_")
        if len(data) < 3:
            continue
        fsymbol = data[0]
        fstartDate = datetime.strptime(data[1], "%Y%m%d")
        fendDate = datetime.strptime(data[2], "%Y%m%d")
        fileList[fsymbol] = filename
        if fendDate < today:
            requestList[fsymbol] = [fendDate + timedelta(days=1), endDate]
    for sym, dates in requestList.items():
        print("query", sym, dates[0], dates[1])
        cursor = collection.find(
            {
                "symbol": sym,
                "date": {
                    "$gte": dates[0],
                    "$lte": dates[1],
                },
            }
        )
        data = [{**item, "_id": str(item["_id"])} for item in cursor]
        df = pl.DataFrame(data, schema=HISTORY_DTYPE)

        if startDate != dates[0]:
            beforeDf = pl.read_parquet(f"{EACH_SYMBOL_DIR}/history/{fileList[sym]}")
            beforeDf = beforeDf.cast(HISTORY_DTYPE)
            df = pl.concat([df, beforeDf])
            df = df.sort("date", descending=True)
        maxDate = df["date"].max()
        minDate = df["date"].min()
        df.write_parquet(
            f"{EACH_SYMBOL_DIR}/history/{sym}_{minDate.strftime('%Y%m%d')}_{maxDate.strftime('%Y%m%d')}"
        )
        if sym in fileList:
            os.remove(f"{EACH_SYMBOL_DIR}/history/{fileList[sym]}")


def convertUSAStockHistoryToDownloadableEachYear(today=getTodayDatetime()):
    prepareDirs()
    collection = getCollection("fmp_usa_stock_history")
    startYear = 1960
    currentYear = today.year
    files = glob.glob(f"{EACH_YEAR_DIR}/history/*")
    filenames = [pathlib.Path(file).name for file in files]
    exceptList = []
    for filename in filenames:
        year = int(filename)
        if year >= startYear and year < currentYear:
            exceptList.append(year)
    for year in range(startYear, currentYear + 1, 1):
        if year in exceptList:
            continue
        startDate = datetime.strptime(f"{year}0101", "%Y%m%d")
        endDate = datetime.strptime(f"{year}1231", "%Y%m%d")
        cursor = collection.find(
            {
                "date": {
                    "$gte": startDate,
                    "$lte": endDate,
                },
            }
        )
        print("query", year)
        data = [{**item, "_id": str(item["_id"])} for item in cursor]
        df = pl.DataFrame(data, schema=HISTORY_DTYPE)
        df.write_parquet(f"{EACH_YEAR_DIR}/history/{year}")
