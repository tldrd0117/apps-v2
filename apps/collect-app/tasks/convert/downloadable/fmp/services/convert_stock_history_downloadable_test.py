import convert_stock_history_downloadable
import os
from datetime import datetime


def test_loadSymbolsFromUSAHistory():
    convert_stock_history_downloadable.loadSymbolsFromUSAHistory(
        datetime.strptime("20240127", "%Y%m%d")
    )


def test_convertUSAStockHistoryToDownloadable():
    convert_stock_history_downloadable.convertUSAStockHistoryToDownloadableEachSymbol()


def test_convertUSAStockHistoryToDownloadableEachYear():
    convert_stock_history_downloadable.convertUSAStockHistoryToDownloadableEachYear()
