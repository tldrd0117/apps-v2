from tasks.baseTask import BaseTask, WrapperBaseTask
from datetime import datetime, date
import luigi
from tasks.targets.MongoFindTarget import MongoFindTarget
from tasks.utils.dateUtils.dateUtils import getDateRangeStr
from dataSource.mongoClient import getClient, getDB, getDBName
import tasks.convert.downloadable.fmp.services.convert_stock_history_downloadable as convert_stock_history_downloadable
from tasks.collect.fmp.tasks.CollectUSAAllTask import CollectUSAStockHistoryTaskLatest
import time
from tasks.utils.dateUtils.dateUtils import getTodayDatetime, convertDateToDatetime


class ConvertStockHistoryDownloadableTaskEachSymbol(BaseTask):
    workers = 1
    today = luigi.DateParameter(default=date.today())

    def getKey(self):
        if self.today is None:
            self.today = date.today()
        return self.today.strftime("%Y%m%d")

    def run(self):
        convert_stock_history_downloadable.convertUSAStockHistoryToDownloadableEachSymbol(
            convertDateToDatetime(self.today)
        )
        self.output().write({self.getKey(): 1})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_ConvertStockHistoryDownloadableTaskEachSymbol",
            [self.getKey()],
            "lastUpdateDate",
        )


class ConvertStockHistoryDownloadableTaskEachYear(BaseTask):
    workers = 1
    today = luigi.DateParameter(default=date.today())

    def getKey(self):
        if self.today is None:
            self.today = date.today()
        return self.today.strftime("%Y%m%d")

    def run(self):

        convert_stock_history_downloadable.convertUSAStockHistoryToDownloadableEachYear(
            convertDateToDatetime(self.today)
        )
        self.output().write({self.getKey(): 1})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_ConvertStockHistoryDownloadableTaskEachYear",
            [self.getKey()],
            "lastUpdateDate",
        )


class ConvertStockHistoryDownloadableTask(BaseTask):
    workers = 1
    today = luigi.DateParameter(default=date.today())

    def getKey(self):
        if self.today is None:
            self.today = date.today()
        return self.today.strftime("%Y%m%d")

    def run(self):
        convert_stock_history_downloadable.convertUSAStockHistoryToDownloadableEachSymbol(
            convertDateToDatetime(self.today)
        )
        convert_stock_history_downloadable.convertUSAStockHistoryToDownloadableEachYear(
            convertDateToDatetime(self.today)
        )
        self.output().write({self.getKey(): 1})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_ConvertStockHistoryDownloadableTask",
            [self.getKey()],
            "lastUpdateDate",
        )


class ConvertLatestHistoryAndDownloadableTask(BaseTask):
    workers = 1
    today = None

    def getKey(self):
        if self.today is None:
            self.today = datetime.today()
        return self.today.strftime("%Y%m%d")

    def run(self):
        startTime = time.time()
        yield CollectUSAStockHistoryTaskLatest()
        yield ConvertStockHistoryDownloadableTask()
        excutionTime = time.time() - startTime
        self.output().write({self.getKey(): excutionTime})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_ConvertLatestHistoryAndDownloadableTask",
            [self.getKey()],
            "lastUpdateDate",
        )
