
from dataSource.mongoClient import getDBAddress, getClient, getCollection
def getKrxBusinessDates():
    collection = getCollection("collectService_krxMarcap")
    data = sorted(collection.find({}, {"date": 1}).distinct("date"))
    return data

def getAllStockCodes():
    collection = getCollection("collectService_krxMarcap")
    data = collection.find({}, {"종목코드": 1}).distinct("종목코드")
    return data

def makeRange(businessDates, stockCode):
    collection = getCollection("collectService_krxMarcap")
    dates = collection.find({"종목코드": stockCode}, {"date": 1}).distinct("date")
    start = None
    end = None
    result = []
    for date in businessDates:
        if date in dates:
            if start is None:
                start = date
            end = date
        elif start is not None:
            result.append([start, end])
            start = None
            end = None
    return result

def makeStockCodeRange():
    target = getKrxBusinessDates()
    stocks = getAllStockCodes()
    collection = getCollection("convertService_stockCodeRange")
    for stock in stocks:
        collection.insert_one({"stockCode": stock, "range": makeRange(target, stock)})
