from dataSource.mongoClient import getDBAddress, getClient, getCollection
from datetime import datetime, timedelta
import polars as pl
from tasks.utils.dateUtils.dateUtils import getDateRange
import pymongo
from stock.models import SeibroStockNumEvent
from stock.models import KrxStock
from stock.models import KrxStockList


def makeAdjustedStock(startDate, endDate, market):
    # table = getStockTable(startDate, endDate, market)
    # dateRange = getDateRange(startDate, endDate)
    # for date in reversed(dateRange):
    #     codes = getStockCodes(date, table)
    #     for code in codes:
    #         events = getCurrentEvents(date, code)
    #         calAdjustedPrice(date, code, events, table)
    table = getStockTable(startDate, endDate, market)
    table = table.with_columns(pl.lit(0).alias("액면가"))
    print(table)
    codes = getStockCodesAll(table)
    for code in codes:
        events = getEventByCode(code)
        if len(events) > 0:
            for event in events:
                if event.액면가 == None or event.액면가 <= 0:
                    continue
                table = table.with_columns(
                    pl.when(
                        (pl.col("종목코드") == code)
                        & (pl.col("date") > event.발행일)
                        & (pl.col("액면가") == 0)
                    )
                    .then(event.액면가)
                    .otherwise(pl.col("액면가"))
                    .alias("액면가")
                )
                print(table)
    table.write_csv("test.csv", separator=",")


def calAdjustedPrice(date, code, events, table):
    row = table.filter(
        (pl.col("date") <= date + timedelta(days=1)) & (pl.col("종목코드") == code)
    ).head(2)
    if len(events) == 0:
        return row
    for event in events:
        if event["발행사유"] == "국내CB행사":
            print(event)
        if event["발행사유"] == "STOCKOPTION행사":
            print(event)
        if event["발행사유"] == "국내BW행사":
            print(event)
        if event["발행사유"] == "국내CB행사":
            print(event)
        if event["발행사유"] == "무상소각":
            print(event)
        if event["발행사유"] == "무상증자":
            print(event)
            print(row)
        if event["발행사유"] == "배당/분배":
            print(event)
        if event["발행사유"] == "분할합병":
            print(event)
        if event["발행사유"] == "사무인수":
            print(event)
        if event["발행사유"] == "상호변경":
            print(event)
        if event["발행사유"] == "액면병합":
            print(event)
            print(row)
        if event["발행사유"] == "액면분할":
            print(event)
            print(row)
        if event["발행사유"] == "유상증자":
            print(event)
            print(row)
        if event["발행사유"] == "이익소각":
            print(event)
        if event["발행사유"] == "자본감소":
            print(event)
        if event["발행사유"] == "주식교환":
            print(event)
        if event["발행사유"] == "주식분할":
            print(event)
        if event["발행사유"] == "주식상환":
            print(event)
        if event["발행사유"] == "주식이전":
            print(event)
        if event["발행사유"] == "주식전환":
            print(event)
        if event["발행사유"] == "합병":
            print(event)
        if event["발행사유"] == "해외BW행사":
            print(event)
        if event["발행사유"] == "해외CB행사":
            print(event)
        if event["발행사유"] == "회사분할":
            print(event)


def getStockTable(startDate: datetime, endDate: datetime, market):
    krxStockList = getStockPrice(startDate, endDate, market)
    rows = krxStockList.model_dump(by_alias=True).get("rows")
    schema = pl.from_dict(rows[0]).schema
    table = pl.from_dicts(
        rows,
        schema,
    )
    print(schema)
    return table


def getEventByCode(code):
    collection = getCollection("collectService_seibroStockNum")
    rows = collection.find({"종목코드": code}).sort("발행일", pymongo.DESCENDING)

    def convert(row):
        row["_id"] = str(row["_id"])
        row["주당발행가"] = row["1주당발행가"]
        return SeibroStockNumEvent(**row)

    return list(map(lambda x: convert(x), rows))


def getEventOfPreferenceShare():
    collection = getCollection("collectService_seibroStockNum")
    rows = collection.find({"주식종류": {"$ne": "보통주"}}).sort("발행일", pymongo.DESCENDING)

    def convert(row):
        row["_id"] = str(row["_id"])
        row["주당발행가"] = row["1주당발행가"]
        return SeibroStockNumEvent(**row)

    return list(map(lambda x: convert(x), rows))


def getEventByReaseon(startDate, endDate, code, reason):
    collection = getCollection("collectService_seibroStockNum")
    rows = collection.find(
        {"종목코드": code, "발행사유": reason, "발행일": {"$gte": startDate, "$lte": endDate}}
    ).sort("발행일", pymongo.DESCENDING)

    def convert(row):
        row["_id"] = str(row["_id"])
        row["주당발행가"] = row["1주당발행가"]
        return SeibroStockNumEvent(**row)

    return list(map(lambda x: convert(x), rows))


def getCurrentEvents(date, code):
    collection = getCollection("collectService_seibroStockNum")
    rows = collection.find({"종목코드": code, "발행일": {"$eq": date}})
    return list(rows)


def getClosetEvent(date, code):
    collection = getCollection("collectService_seibroStockNum")
    rows = (
        collection.find({"종목코드": code, "발행일": {"$lt": date}})
        .sort("발행일", pymongo.DESCENDING)
        .limit(2)
    )
    rows = list(rows)
    if len(rows) == 0:
        return None
    return rows[0]


def getSplitRatioTable(startDate: datetime, endDate: datetime):
    collection = getCollection("collectService_seibroStockNum")
    rows = collection.find({"발행일": {"$gte": startDate, "$lte": endDate}})
    table = pl.from_dicts(rows)
    return table


def getFaceValue(startDate, endDate):
    collection = getCollection("collectService_seibroStockNum")
    rows = collection.find({})
    return rows


def getStockCodes(date: datetime, table: pl.DataFrame):
    curDateTable = table.filter(pl.col("date") == date)
    codes = curDateTable.select("종목코드").unique().get_columns()[0].to_list()
    return codes


def getStockCodesAll(table: pl.DataFrame):
    codes = table.select("종목코드").unique().get_columns()[0].to_list()
    return codes


def getStockPrice(startDate: datetime, endDate: datetime, market):
    collection = getCollection("collectService_krxMarcap")
    rows = collection.find(
        {"market": market, "date": {"$gte": startDate, "$lte": endDate}}
    ).sort("date", pymongo.DESCENDING)

    def convert(row):
        row["_id"] = str(row["_id"])
        return KrxStock(**row)

    rows = list(map(lambda x: convert(x), rows))
    return KrxStockList(rows=rows, total=len(rows))


def getYesterdayAdjustedPrice(code, date, priceTable):
    currentAdjustedPrice = getCurrentAdjustedPrice(code, date, priceTable)
    currentSplitRatio = getCurrentSplitRatio(code, date, priceTable)
    currentPrice = getCurrentPrice(code, date, priceTable)
    yesterDayPrice = getYesterdayPrice(code, date, priceTable)
    if currentSplitRatio <= 1:
        yesterdayAdjustedPrice = (
            currentAdjustedPrice * yesterDayPrice * currentSplitRatio / currentPrice
        )
    return yesterdayAdjustedPrice


def getYesterdayPrice(code, date, table):
    pass


def getCurrentAdjustedPrice(code, date, table):
    pass


def getCurrentPrice(code, date, table):
    pass


# 현재 분할 비율
def getCurrentSplitRatio(code, date, table):
    pass
