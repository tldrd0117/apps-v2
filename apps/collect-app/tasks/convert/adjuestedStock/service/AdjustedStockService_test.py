from datetime import datetime
import AdjustedStockService


def test_getStockPrice():
    startDate = datetime.strptime("20210101", "%Y%m%d")
    endDate = datetime.strptime("20210104", "%Y%m%d")
    rows = AdjustedStockService.getStockPrice(startDate, endDate, "kospi")
    assert len(list(rows)) > 0


def test_getStockTable():
    startDate = datetime.strptime("20210101", "%Y%m%d")
    endDate = datetime.strptime("20210104", "%Y%m%d")
    table = AdjustedStockService.getStockTable(startDate, endDate, "kospi")
    print(table)


def test_getStockCodes():
    startDate = datetime.strptime("20210101", "%Y%m%d")
    endDate = datetime.strptime("20210104", "%Y%m%d")
    table = AdjustedStockService.getStockTable(startDate, endDate, "kospi")
    codes = AdjustedStockService.getStockCodes(endDate, table)
    print(codes)
    assert len(codes) > 0


def test_getEventOfPreferenceShare():
    events = AdjustedStockService.getEventOfPreferenceShare()
    print(events)


def test_getFaceValue():
    rows = AdjustedStockService.getFaceValue()
    print(list(rows))
    assert len(list(rows)) > 0


def test_getSplitRatioTable():
    startDate = datetime.strptime("20210101", "%Y%m%d")
    endDate = datetime.strptime("20210104", "%Y%m%d")
    table = AdjustedStockService.getSplitRatioTable(startDate, endDate)
    print(table)


def test_makeAdjustedStock():
    startDate = datetime.strptime("20180503", "%Y%m%d")
    endDate = datetime.strptime("20180510", "%Y%m%d")
    AdjustedStockService.makeAdjustedStock(startDate, endDate, "kospi")
