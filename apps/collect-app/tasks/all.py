from tasks.collect.dartFinancialStatement.DartFinancialStatementTask import (
    InsertDartFinancialStatementTask,
)
from tasks.collect.krxMarcap.KrxMarcapTask import KrxMarcapRangeTask
from tasks.collect.seibroDividend.SeibroDividendTask import (
    CrawlingAllSeibroDividendTask,
    UpdateDateFormatSeibroDividendTask,
)
from tasks.collect.seibroStockNum.SeibroStockNumTask import (
    CrawlingAllSeibroStockNumTask,
    UpdateDateFormatSeibroStockNumTask,
)
from tasks.collect.fmp.tasks.CollectUSAAllTask import (
    CollectUSAStockSymbolTask,
    CollectUSAStockHistoryTask,
    CollectUSAStockHistoryTaskLatest,
    CollectUSAFinancialTask,
    CollectUSABalanceSheetsTask,
    CollectUSAIncomeStatementTask,
    CollectUSACashFlowStatementsTask,
    CollectAllEODPriceTask,
    CollectEODPriceTask,
)

from tasks.convert.downloadable.fmp.tasks.ConvertStockHistoryDownloadableTask import (
    ConvertStockHistoryDownloadableTask,
    ConvertLatestHistoryAndDownloadableTask,
    ConvertStockHistoryDownloadableTaskEachSymbol,
    ConvertStockHistoryDownloadableTaskEachYear,
)

from tasks.collect.binance.task.CollectCryptoTask import (
    CollectCryptoPriceTask,
    CollectCryptoSymbolsTask,
)

from tasks.convert.downloadable.common.tasks.ConvertHistoryDownloadableTask import (
    ConvertHistoryDownloadableTask,
)

taskMap = {}


def addTaskMap(task):
    taskMap[str(task.__name__)] = task


def fillTaskMap():
    addTaskMap(InsertDartFinancialStatementTask)
    addTaskMap(KrxMarcapRangeTask)
    addTaskMap(CrawlingAllSeibroDividendTask)
    addTaskMap(CrawlingAllSeibroStockNumTask)
    addTaskMap(UpdateDateFormatSeibroStockNumTask)
    addTaskMap(UpdateDateFormatSeibroDividendTask)
    addTaskMap(CollectUSAStockHistoryTask)
    addTaskMap(CollectUSAStockHistoryTaskLatest)
    addTaskMap(CollectUSAFinancialTask)
    addTaskMap(CollectUSAStockSymbolTask)
    addTaskMap(CollectUSABalanceSheetsTask)
    addTaskMap(CollectUSAIncomeStatementTask)
    addTaskMap(CollectUSACashFlowStatementsTask)
    addTaskMap(ConvertStockHistoryDownloadableTask)
    addTaskMap(ConvertLatestHistoryAndDownloadableTask)
    addTaskMap(ConvertStockHistoryDownloadableTaskEachSymbol)
    addTaskMap(ConvertStockHistoryDownloadableTaskEachYear)
    addTaskMap(CollectCryptoPriceTask)
    addTaskMap(CollectCryptoSymbolsTask)
    addTaskMap(ConvertHistoryDownloadableTask)
    addTaskMap(CollectAllEODPriceTask)
    addTaskMap(CollectEODPriceTask)


def getRunnableTasks():
    fillTaskMap()
    return taskMap.keys()


def getTaskParams(name):
    fillTaskMap()
    return taskMap[str(name)].get_params()
