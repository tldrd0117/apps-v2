import BAAAgressiveService


def test_selectAggressiveMomentumScore():
    df = BAAAgressiveService.selectAggressiveMomentumScore()
    df.write_csv("resources/csv/AggressiveAssetMomentumScore.csv")


def test_selectAggressiveMomentumProfit():
    df = BAAAgressiveService.selectAggressiveProfit()
    df.write_csv("resources/csv/AggressiveAssetProfit.csv")


def test_selectAggressiveClose():
    df = BAAAgressiveService.selectAggressiveClose()
    df.write_csv("resources/csv/AggressiveAssetClose.csv")


def test_selectDefensiveClosePerMovingAvg():
    df = BAAAgressiveService.selectDefensiveClosePerMovingAvg()
    df.write_csv("resources/csv/DefensiveAssetClosePerMovingAvg.csv")


def test_selectDefensiveClose():
    df = BAAAgressiveService.selectDefensiveClose()
    df.write_csv("resources/csv/DefensiveAssetClose.csv")


def test_selectCanaryMomentumScore():
    df = BAAAgressiveService.selectCanaryMomentumScore()
    df.write_csv("resources/csv/CanaryAssetMomentumScore.csv")
