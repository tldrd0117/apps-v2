from dataSource.mongoClient import getCollection
import polars as pl
import os
import glob
from datetime import datetime, timedelta

targetEntity = [
    "SPY",
    "VWO",
    "VEA",
    "BND",
    "QQQ",
    "BIL",
    "IEF",
    "TLT",
    "LQD",
    "TIP",
    "DBC",
]

aggresiveEntity = ["QQQ", "VWO", "VEA", "BND"]

canaryEntity = ["SPY", "VWO", "VEA", "BND"]

momentumEntity = ["QQQ", "VWO", "VEA", "BND", "SPY"]

defensiveEntity = [
    "BIL",
    "IEF",
    "TLT",
    "LQD",
    "TIP",
    "DBC",
    "BND",
]


def createEntityToFile():
    collection = getCollection("fmp_usa_tradable_history")
    dailyEntity = {}
    paths = {}
    for code in targetEntity:
        files = glob.glob(f"tasks/simulate/baa/datasets/{code}*")
        if len(files) != 0:
            path = files[0]
            paths[code] = path
            continue
        cursor = collection.find({"symbol": code})
        data = [{**item, "_id": str(item["_id"])} for item in cursor]
        df = pl.DataFrame(data)

        min_date = dailyEntity[code]["date"].min().strftime("%Y-%m-%d")
        max_date = dailyEntity[code]["date"].max().strftime("%Y-%m-%d")
        path = "tasks/simulate/baa/datasets/{}.parquet".format(
            code + "_" + str(min_date) + "_" + str(max_date)
        )
        df.write_parquet(path)
        paths[code] = path

    return paths


def loadFileDailyEntity(paths):
    dailyEntity = {}
    for code in paths:
        dailyEntity[code] = pl.read_parquet(paths[code])
    return dailyEntity


def calculateMomentum(dailyEntity):
    for entity in momentumEntity:
        df: pl.DataFrame = dailyEntity[entity]

        def getCloseByDate(x):
            dateDf = df.filter(df["date"] >= x).sort("date", descending=False)
            if dateDf.height == 0:
                return None
            else:
                return dateDf["close"][0]

        df = df.with_columns(
            [
                df["date"]
                .dt.offset_by("-1y_saturating")
                .apply(getCloseByDate)
                .alias("-1y"),
                df["date"]
                .dt.offset_by("-1mo_saturating")
                .apply(getCloseByDate)
                .alias("-1mo"),
                df["date"]
                .dt.offset_by("-3mo_saturating")
                .apply(getCloseByDate)
                .alias("-3mo"),
                df["date"]
                .dt.offset_by("-6mo_saturating")
                .apply(getCloseByDate)
                .alias("-6mo"),
            ]
        )

        df = df.with_columns(
            [
                ((df["close"] / df["-1y"] - 1.0) * 100).alias("1yReturn"),
                ((df["close"] / df["-1mo"] - 1.0) * 100).alias("1moReturn"),
                ((df["close"] / df["-3mo"] - 1.0) * 100).alias("3moReturn"),
                ((df["close"] / df["-6mo"] - 1.0) * 100).alias("6moReturn"),
            ]
        )

        df = df.with_columns(
            [
                (
                    df["1moReturn"] * 12
                    + df["3moReturn"] * 4
                    + df["6moReturn"] * 2
                    + df["1yReturn"]
                ).alias("momentumScore")
            ]
        )
        dailyEntity[entity] = df
    return dailyEntity


def calculateMovingAverage(dailyEntity):
    for entity in defensiveEntity:
        df: pl.DataFrame = dailyEntity[entity]

        def getCloseByDate(x):
            dateDf = df.filter(
                (df["date"] >= x) & (df["date"] <= x + timedelta(days=5))
            ).sort("date", descending=False)
            if dateDf.height == 0:
                return None
            else:
                return dateDf["close"].mean()

        df = df.with_columns(
            [
                df["date"]
                .dt.offset_by("-1y_saturating")
                .apply(getCloseByDate)
                .alias("1yMovingAvg"),
            ]
        )

        df = df.with_columns(
            [
                ((df["close"] / df["1yMovingAvg"] - 1.0) * 100).alias(
                    "close/1yMovingAvg"
                ),
            ]
        )
        dailyEntity[entity] = df
    return dailyEntity


def loadDailyEntiry():
    paths = createEntityToFile()
    dailyEntity = loadFileDailyEntity(paths)
    dailyEntity = calculateMovingAverage(dailyEntity)
    dailyEntity = calculateMomentum(dailyEntity)
    return dailyEntity


# 공격자산의 모멘텀 스코어 차트
def selectAggressiveMomentumScore(dailyEntity=None):
    if dailyEntity is None:
        dailyEntity = loadDailyEntiry()
    merged = None
    for entity in aggresiveEntity:
        df: pl.DataFrame = dailyEntity[entity]
        df = df.rename({"date": "Date"})
        df = df.select(
            [
                "Date",
                "momentumScore",
                "symbol",
            ]
        )
        df = df.pivot(
            index="Date",
            columns="symbol",
            values="momentumScore",
            aggregate_function=None,
        )
        if merged is None:
            merged = df
        else:
            merged = merged.join(df, on="Date", how="outer")
    merged = merged.sort("Date")
    return merged


def selectAggressiveProfit(dailyEntity=None):
    if dailyEntity is None:
        dailyEntity = loadDailyEntiry()
    merged = None
    for entity in aggresiveEntity:
        df: pl.DataFrame = dailyEntity[entity]
        df = df.rename({"date": "Date"})
        df = df.select(
            ["Date", "1yReturn", "1moReturn", "3moReturn", "6moReturn", "symbol"]
        )
        pivotDf = None
        for col in ["1yReturn", "1moReturn", "3moReturn", "6moReturn"]:
            dfPivot = df.pivot(
                index="Date",
                columns="symbol",
                values=col,
                aggregate_function=None,
            )
            dfPivot = dfPivot.rename({entity: entity + "_" + col})
            if pivotDf is None:
                pivotDf = dfPivot
            else:
                pivotDf = pivotDf.join(dfPivot, on="Date", how="outer")
        if merged is None:
            merged = pivotDf
        else:
            merged = merged.join(pivotDf, on="Date", how="outer")
    merged = merged.sort("Date")
    return merged


def selectAggressiveClose(dailyEntity=None):
    if dailyEntity is None:
        dailyEntity = loadDailyEntiry()
    merged = None
    for entity in aggresiveEntity:
        df: pl.DataFrame = dailyEntity[entity]
        df = df.rename({"date": "Date"})
        df = df.select(["Date", "close", "symbol"])
        pivotDf = None
        for col in ["close"]:
            dfPivot = df.pivot(
                index="Date",
                columns="symbol",
                values=col,
                aggregate_function=None,
            )
            dfPivot = dfPivot.rename({entity: entity + "_" + col})
            if pivotDf is None:
                pivotDf = dfPivot
            else:
                pivotDf = pivotDf.join(dfPivot, on="Date", how="outer")
        if merged is None:
            merged = pivotDf
        else:
            merged = merged.join(pivotDf, on="Date", how="outer")
    merged = merged.sort("Date")
    return merged


def selectDefensiveClosePerMovingAvg(dailyEntity=None):
    if dailyEntity is None:
        dailyEntity = loadDailyEntiry()
    merged = None
    for entity in defensiveEntity:
        df: pl.DataFrame = dailyEntity[entity]
        df = df.rename({"date": "Date"})
        df = df.select(["Date", "close/1yMovingAvg", "symbol"])
        pivotDf = None
        for col in ["close/1yMovingAvg"]:
            dfPivot = df.pivot(
                index="Date",
                columns="symbol",
                values=col,
                aggregate_function=None,
            )
            dfPivot = dfPivot.rename({entity: entity + "_" + col})
            if pivotDf is None:
                pivotDf = dfPivot
            else:
                pivotDf = pivotDf.join(dfPivot, on="Date", how="outer")
        if merged is None:
            merged = pivotDf
        else:
            merged = merged.join(pivotDf, on="Date", how="outer")
    merged = merged.sort("Date")
    return merged


def selectDefensiveClose(dailyEntity=None):
    if dailyEntity is None:
        dailyEntity = loadDailyEntiry()
    merged = None
    for entity in defensiveEntity:
        df: pl.DataFrame = dailyEntity[entity]
        df = df.rename({"date": "Date"})
        df = df.select(["Date", "close", "symbol"])
        pivotDf = None
        for col in ["close"]:
            dfPivot = df.pivot(
                index="Date",
                columns="symbol",
                values=col,
                aggregate_function=None,
            )
            dfPivot = dfPivot.rename({entity: entity + "_" + col})
            if pivotDf is None:
                pivotDf = dfPivot
            else:
                pivotDf = pivotDf.join(dfPivot, on="Date", how="outer")
        if merged is None:
            merged = pivotDf
        else:
            merged = merged.join(pivotDf, on="Date", how="outer")
    merged = merged.sort("Date")
    return merged


def selectCanaryMomentumScore(dailyEntity=None):
    if dailyEntity is None:
        dailyEntity = loadDailyEntiry()
    merged = None
    for entity in canaryEntity:
        df: pl.DataFrame = dailyEntity[entity]
        df = df.rename({"date": "Date"})
        df = df.select(
            [
                "Date",
                "momentumScore",
                "symbol",
            ]
        )
        df = df.pivot(
            index="Date",
            columns="symbol",
            values="momentumScore",
            aggregate_function=None,
        )
        if merged is None:
            merged = df
        else:
            merged = merged.join(df, on="Date", how="outer")
    merged = merged.sort("Date")
    return merged
