from dataSource.mongoClient import getCollection
import polars as pl
from datetime import datetime, timedelta


def loadDailyEntity():
    paths = {
        "BIL": "tasks/simulate/baa/datasets/BIL_2007-05-30_2024-01-05.parquet",
        "BND": "tasks/simulate/baa/datasets/BND_2007-04-10_2024-01-05.parquet",
        "DBC": "tasks/simulate/baa/datasets/DBC_2006-02-03_2024-01-05.parquet",
        "IEF": "tasks/simulate/baa/datasets/IEF_2002-07-26_2024-01-05.parquet",
        "LQD": "tasks/simulate/baa/datasets/LQD_2002-07-30_2024-01-05.parquet",
        "QQQ": "tasks/simulate/baa/datasets/QQQ_1999-03-10_2024-01-05.parquet",
        "SPY": "tasks/simulate/baa/datasets/SPY_1993-01-29_2024-01-05.parquet",
        "TIP": "tasks/simulate/baa/datasets/TIP_2003-12-05_2024-01-05.parquet",
        "TLT": "tasks/simulate/baa/datasets/TLT_2002-07-30_2024-01-05.parquet",
        "VEA": "tasks/simulate/baa/datasets/VEA_2007-07-26_2024-01-05.parquet",
        "VWO": "tasks/simulate/baa/datasets/VWO_2005-03-10_2024-01-05.parquet",
    }
    dailyEntity = {}
    for key in paths:
        dailyEntity[key] = pl.read_parquet(paths[key])
    return dailyEntity


def makeDailyEntity():
    collection = getCollection("fmp_usa_tradable_history")
    dailyCodes = [
        "SPY",
        "VWO",
        "VEA",
        "BND",
        "QQQ",
        "BIL",
        "IEF",
        "TLT",
        "LQD",
        "TIP",
        "BND",
        "DBC",
    ]
    dailyEntity = {}
    for code in dailyCodes:
        cursor = collection.find({"symbol": code})
        data = [{**item, "_id": str(item["_id"])} for item in cursor]
        df = pl.DataFrame(data)
        dailyEntity[code] = df
        min_date = dailyEntity[code]["date"].min().strftime("%Y-%m-%d")
        max_date = dailyEntity[code]["date"].max().strftime("%Y-%m-%d")
        dailyEntity[code].write_parquet(
            "tasks/simulate/baa/datasets/{}.parquet".format(
                code + "_" + str(min_date) + "_" + str(max_date)
            )
        )
        print(code, min_date, max_date)
    return dailyEntity


def makeMomentum(dailyEntity, currentDay):
    momentumCodes = ["SPY", "VWO", "VEA", "BND", "QQQ", "VWO"]
    df: pl.DataFrame = dailyEntity["SPY"]

    def getCloseByDate(x):
        dateDf = df.filter(df["date"] >= x).sort("date", descending=False)
        if dateDf.height == 0:
            return None
        else:
            return dateDf["close"][0]

    df = df.with_columns(
        [
            df["date"]
            .dt.offset_by("-1y_saturating")
            .apply(getCloseByDate)
            .alias("-1y"),
            df["date"]
            .dt.offset_by("-1mo_saturating")
            .apply(getCloseByDate)
            .alias("-1mo"),
            df["date"]
            .dt.offset_by("-3mo_saturating")
            .apply(getCloseByDate)
            .alias("-3mo"),
            df["date"]
            .dt.offset_by("-6mo_saturating")
            .apply(getCloseByDate)
            .alias("-6mo"),
        ]
    )

    df = df.with_columns(
        [
            ((df["close"] / df["-1y"] - 1.0) * 100).alias("1yReturn"),
            ((df["close"] / df["-1mo"] - 1.0) * 100).alias("1moReturn"),
            ((df["close"] / df["-3mo"] - 1.0) * 100).alias("3moReturn"),
            ((df["close"] / df["-6mo"] - 1.0) * 100).alias("6moReturn"),
        ]
    )

    df = df.with_columns(
        [
            (
                df["1moReturn"] * 12
                + df["3moReturn"] * 4
                + df["6moReturn"] * 2
                + df["1yReturn"]
            ).alias("momentumScore")
        ]
    )
    print(df)
    # df = df.rename({"date": "Date"})
    # df = df.select(
    #     [
    #         "Date",
    #         "close",
    #         "1moReturn",
    #         "3moReturn",
    #         "6moReturn",
    #         "1yReturn",
    #         "momentumScore",
    #     ]
    # )
    # df.write_csv("tasks/simulate/baa/datasets/momentum.csv")

    # for row in df.rows():
    #     print(row)
    # with pl.Config(tbl_cols=df.width):
    #     print(df_first_days.head(100))


def makeMovingAverage(dailyEntity, currentDay):
    df = dailyEntity["SPY"]
    # Get the date one year ago from today
    print(df)

    def getCloseByDate(x):
        dateDf = df.filter(
            (df["date"] >= x) & (df["date"] <= x + timedelta(days=5))
        ).sort("date", descending=False)
        if dateDf.height == 0:
            return None
        else:
            return dateDf["close"].mean()

    df = df.with_columns(
        [
            df["date"]
            .dt.offset_by("-1y_saturating")
            .apply(getCloseByDate)
            .alias("-1yMovingAvg"),
        ]
    )
    print(df)


def test_BAAAgressive():
    startDate = "2008-01-01"
    endDate = "2024-01-01"
    dailyEntity = loadDailyEntity()
    # makeMomentum(dailyEntity, 1)
    makeMovingAverage(dailyEntity, 1)
