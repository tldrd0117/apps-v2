from tasks.simulate.common.BackTest import (
    BackTest,
    Schedule,
    Interval,
    RebalanceOrders,
    RebalanceOrder,
)
from datetime import datetime, timedelta
from tasks.Constants import EntityType
import polars as pl


class BAABackTest(BackTest):
    def initalize(self):
        self.losscutDf = None
        self.setBalance(30000)
        self.setStartDate("20110131")
        self.setEndDate("20240229")
        self.datasetService.addYieldDates("1mo")
        self.datasetService.addYieldDates("6mo")
        self.datasetService.addYieldDates("3mo")
        self.datasetService.addYieldDates("12mo")
        self.datasetService.addMovingAverage("12mo")
        entities = [
            "SPY",
            "VWO",
            "VEA",
            "BND",
            "QQQ",
            "QLD",
            "TQQQ",
            "BIL",
            "IEF",
            "TLT",
            "LQD",
            "TIP",
            "DBC",
        ]
        for entity in entities:
            self.addEntity(EntityType.USA_STOCK, entity)
        self.PORTFOLIO_NAMES = ["BAAAgressive-2", "BAAAgressive2-2", "BAAAgressive3-2"]
        self.addPortfolio(self.PORTFOLIO_NAMES[0], 0.33)
        self.addPortfolio(self.PORTFOLIO_NAMES[1], 0.5)
        self.addPortfolio(self.PORTFOLIO_NAMES[2], 1)
        self.addSchedules(
            Schedule(
                name="monthly",
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(month=1),
            )
        )
        self.addSchedules(
            Schedule(
                name="daily",
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(day=1),
            )
        )

    def calulateMomentumScore(self, symbol, date):
        dataYield1mo = self.getCurrentData(symbol, date, "1moYield")
        dataYield3mo = self.getCurrentData(symbol, date, "3moYield")
        dataYield6mo = self.getCurrentData(symbol, date, "6moYield")
        dataYield1y = self.getCurrentData(symbol, date, "12moYield")
        print(
            symbol,
            dataYield1mo,
            dataYield3mo,
            dataYield6mo,
            dataYield1y,
            dataYield1mo * 12 + dataYield3mo * 4 + dataYield6mo * 2 + dataYield1y,
        )
        return dataYield1mo * 12 + dataYield3mo * 4 + dataYield6mo * 2 + dataYield1y

    def losscut(self, portfolioName, currentDate):
        buyCost = self.getPortfolio(portfolioName).totalBuyCost
        cost = self.getPortfolio(portfolioName).totalCost
        if buyCost == 0:
            return False
        losscutObj = {
            "date": currentDate,
            "portfolio": portfolioName,
            "cost": cost,
            "buyCost": buyCost,
            "percent": cost / buyCost,
        }
        if self.losscutDf is None:
            self.losscutDf = pl.DataFrame([losscutObj])
        else:
            self.losscutDf = self.losscutDf.vstack(pl.DataFrame([losscutObj]))
        if cost / buyCost < 0.9:
            print("losscut activate", currentDate, portfolioName, cost, buyCost)
            self.getPortfolio(portfolioName).liquidate(currentDate)
            return True
        return False

    def rebalace(self, date: datetime):
        yesterday = date + timedelta(days=-1)
        spyMom = self.calulateMomentumScore("SPY", yesterday)
        vwoMom = self.calulateMomentumScore("VWO", yesterday)
        veaMom = self.calulateMomentumScore("VEA", yesterday)
        bndMom = self.calulateMomentumScore("BND", yesterday)
        print(spyMom, vwoMom, veaMom, bndMom)
        isSafeAsset = False
        # SPY,  VWO, VEA, BND
        # 4개중 하나라도 마이너스 momentum이 나오면 안전자산으로 이동
        if spyMom < 0 or vwoMom < 0 or veaMom < 0 or bndMom < 0:
            isSafeAsset = True
        if isSafeAsset:
            yields = {}

            yields["BIL"] = (
                self.getPrice("BIL", yesterday)
                / self.getCurrentData("BIL", yesterday, "12moMovingAverage")
                - 1
            )
            yields["IEF"] = (
                self.getPrice("IEF", yesterday)
                / self.getCurrentData("IEF", yesterday, "12moMovingAverage")
                - 1
            )
            yields["TLT"] = (
                self.getPrice("TLT", yesterday)
                / self.getCurrentData("TLT", yesterday, "12moMovingAverage")
                - 1
            )
            yields["LQD"] = (
                self.getPrice("LQD", yesterday)
                / self.getCurrentData("LQD", yesterday, "12moMovingAverage")
                - 1
            )
            yields["TIP"] = (
                self.getPrice("TIP", yesterday)
                / self.getCurrentData("TIP", yesterday, "12moMovingAverage")
                - 1
            )
            yields["BND"] = (
                self.getPrice("BND", yesterday)
                / self.getCurrentData("BND", yesterday, "12moMovingAverage")
                - 1
            )
            yields["DBC"] = (
                self.getPrice("DBC", yesterday)
                / self.getCurrentData("DBC", yesterday, "12moMovingAverage")
                - 1
            )
            selectedAsset = []
            for key, value in yields.items():
                print(date, key, value)
                if value < 0:
                    continue
                if len(selectedAsset) < 3:
                    selectedAsset.append({"symbol": key, "value": value})
                else:
                    removeItems = []
                    for item in selectedAsset:
                        if item["value"] < value:
                            removeItems.append(item)
                    if len(removeItems) > 0:
                        minItem = min(removeItems, key=lambda x: x["value"])
                        selectedAsset.remove(minItem)
                        selectedAsset.append({"symbol": key, "value": value})
            if len(selectedAsset) < 3:
                for _ in range(3 - len(selectedAsset)):
                    selectedAsset.append({"symbol": "BIL", "value": yields["BIL"]})
            if len(selectedAsset) != 3:
                raise Exception("selectedAsset not 3")
            orders = {}
            for item in selectedAsset:
                if item["symbol"] not in orders:
                    orders[item["symbol"]] = 0.33
                else:
                    orders[item["symbol"]] = orders[item["symbol"]] + 0.33
            print(date, orders)

            self.rebalanceByPercent(
                self.PORTFOLIO_NAMES[0],
                RebalanceOrders(
                    *list(map(lambda x: RebalanceOrder(x, orders[x]), orders.keys()))
                ),
            )
            self.rebalanceByPercent(
                self.PORTFOLIO_NAMES[1],
                RebalanceOrders(
                    *list(map(lambda x: RebalanceOrder(x, orders[x]), orders.keys()))
                ),
            )
            self.rebalanceByPercent(
                self.PORTFOLIO_NAMES[2],
                RebalanceOrders(
                    *list(map(lambda x: RebalanceOrder(x, orders[x]), orders.keys()))
                ),
            )
        else:
            scores = {}
            scores["QQQ"] = (
                self.getPrice("QQQ", yesterday)
                / self.getCurrentData("QQQ", yesterday, "12moMovingAverage")
                - 1
            )
            scores["VWO"] = (
                self.getPrice("VWO", yesterday)
                / self.getCurrentData("VWO", yesterday, "12moMovingAverage")
                - 1
            )
            scores["VEA"] = (
                self.getPrice("VEA", yesterday)
                / self.getCurrentData("VEA", yesterday, "12moMovingAverage")
                - 1
            )
            scores["BND"] = (
                self.getPrice("BND", yesterday)
                / self.getCurrentData("BND", yesterday, "12moMovingAverage")
                - 1
            )
            maxKey = max(scores, key=scores.get)
            print(maxKey)
            if maxKey == "QQQ":
                maxKey2 = "QLD"
                maxKey3 = "TQQQ"
            else:
                maxKey2 = maxKey
                maxKey3 = maxKey
            self.rebalanceByPercent(
                self.PORTFOLIO_NAMES[0],
                RebalanceOrders(
                    RebalanceOrder(maxKey, 1),
                ),
            )
            self.rebalanceByPercent(
                self.PORTFOLIO_NAMES[1],
                RebalanceOrders(
                    RebalanceOrder(maxKey2, 1),
                ),
            )
            self.rebalanceByPercent(
                self.PORTFOLIO_NAMES[2],
                RebalanceOrders(
                    RebalanceOrder(maxKey3, 1),
                ),
            )

    def onSchedule(self, name: str, date: datetime):
        if name == "monthly":
            self.rebalace(date)

        if name == "daily":
            self.losscut(self.PORTFOLIO_NAMES[0], date)
            self.losscut(self.PORTFOLIO_NAMES[1], date)
            self.losscut(self.PORTFOLIO_NAMES[2], date)

    # def onScheduleEndOfMarket(self, name: str, date: datetime):

    def onEndOfBackTest(self):
        self.losscutDf.write_csv("losscut.csv")
