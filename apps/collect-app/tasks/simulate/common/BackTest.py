from datetime import timedelta, datetime
from tasks.simulate.common.services.DatasetService import DatasetService
import polars as pl
from typing import Dict, List
import math
from functools import wraps, reduce
import time
from dateutil.relativedelta import relativedelta
import tasks.Constants as Constants
import os

DEFAULT_BUY_FEE = 0.00026
DEFAULT_SELL_FEE = 0.00026
DEFAULT_SLIPPAGE = 0.0003


def timeit(func):
    @wraps(func)
    def timeit_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        # first item in the args, ie `args[0]` is `self` {args} {kwargs}
        if total_time > 0.1:
            print(f"Function {func.__name__} Took {total_time:.4f} seconds")
        return result

    return timeit_wrapper


class Interval(object):
    def __init__(self, year: int = 0, month: int = 0, day: int = 0) -> None:
        self.year = year
        self.month = month
        self.day = day


class Schedule(object):
    def __init__(
        self,
        name: str,
        startDate: datetime,
        endDate: datetime,
        interval: Interval,
    ):
        self.name = name
        if isinstance(startDate, str):
            startDate = datetime.strptime(startDate, "%Y%m%d")
        if isinstance(endDate, str):
            startDate = datetime.strptime(startDate, "%Y%m%d")
        self.startDate: datetime = startDate
        self.endDate = endDate
        self.interval = interval
        self.currentDate = self.startDate
        if self.name is None:
            raise Exception("name cannot be None")
        if self.startDate is None:
            raise Exception("startDate cannot be None")
        if self.endDate is None:
            raise Exception("endDate cannot be None")
        if self.interval is None:
            raise Exception("interval cannot be None")

    def next(self):
        if self.interval.year > 0:
            self.currentDate = self.currentDate.replace(
                year=self.currentDate.year + self.interval.year
            )
        if self.interval.month > 0:
            day = self.currentDate.day
            self.currentDate = self.currentDate + relativedelta(
                months=self.interval.month
            )
            endOfMonth = self.currentDate + relativedelta(day=31)
            if endOfMonth.day < day:
                day = endOfMonth.day
            self.currentDate = self.currentDate.replace(day=day)
        if self.interval.day > 0:
            self.currentDate = self.currentDate + timedelta(days=self.interval.day)
        if self.currentDate > self.endDate:
            self.currentDate = self.endDate
        return self.currentDate


class RebalanceOrder(object):
    def __init__(self, symbol: str, percent: float) -> None:
        self.symbol = symbol
        self.percent = percent

    def __repr__(self) -> str:
        return f"{self.symbol} {self.percent}"


class RebalanceOrders(object):
    def __init__(self, *args) -> None:
        self.items: List[RebalanceOrder] = []
        for arg in args:
            self.items.append(arg)

    def __repr__(self) -> str:
        return f"{self.items}"


class OrderHistory(object):
    def __init__(self, symbol: str, date: datetime, count: int, price: float) -> None:
        self.symbol = symbol
        self.date = date
        self.count = count
        self.price = price

    def __repr__(self) -> str:
        return f"{self.symbol} {self.date} {self.count} {self.price}"


class ShareHolding(object):

    def __init__(self, symbol: str, buyDate: datetime, count: int) -> None:
        self.symbol = symbol
        self.buyDate = buyDate
        self.count = count
        self.buyPrice = None

    def __repr__(self) -> str:
        return f"{self.symbol} {self.buyPrice} {self.buyDate} {self.count}"


class ShareHoldingTotal(object):
    def __init__(
        self,
        buyCost: float,
        averageCost: float,
        cost: float,
        count: int,
        percent: float,
        currentValue: float,
        properties: Dict[str, float] = {},
    ) -> None:
        # 매수금액 total
        self.buyCost = buyCost
        # 평균매수가
        self.averageCost = averageCost
        # 현재가 total
        self.cost = cost
        # 보유수량
        self.count = count
        # 수익률
        self.percent = percent
        # 현재가
        self.currentValue = currentValue
        # 추가 정보
        for key, value in properties.items():
            setattr(self, key, value)

    def __repr__(self) -> str:
        return f"buyCost: {self.buyCost}, averageCost: {self.averageCost}, cost: {self.cost}, count: {self.count}"


class PortfolioHistory(object):
    def __init__(
        self,
        date: datetime,
        symbols: List[str],
        holdingTotals: List[ShareHoldingTotal],
        totalAsset: float,
        totalCost: float,
        totalTradeCost: float,
        totalBuyCost: float,
    ) -> None:
        self.date = date
        self.symbols = symbols
        self.holdingTotals = holdingTotals
        self.totalAsset = totalAsset
        self.totalCost = totalCost
        self.totalTradeCost = totalTradeCost
        self.totalBuyCost = totalBuyCost

    def toArray(self):
        # 수정해야함 symobol을 기준으로 데이터를 만들어야함
        return [
            {
                "date": self.date,
                "symbol": self.symbols[index],
                "totalAsset": self.totalAsset,
                "totalCost": self.totalCost,
                "totalTradeCost": self.totalTradeCost,
                "totalBuyCost": self.totalBuyCost,
                **self.holdingTotals[index].__dict__,
            }
            for index in range(len(self.symbols))
        ]


class Portfolio(object):
    def __init__(
        self, datasetService: DatasetService, slippage: float = DEFAULT_SLIPPAGE
    ) -> None:
        # symbol: [ShareHolding]
        self.holdings: Dict[str, List[ShareHolding]] = {}
        # symbol: ShareHoldingTotal
        self.totals: Dict[str, List[ShareHoldingTotal]] = {}
        # 총 보유금액
        self.totalCost: float = 0
        # 총 매수금액
        self.totalBuyCost: float = 0
        # 현금
        self.cash: float = 0
        # 총 자산금액
        self.totalAsset: float = 0
        # 총 수수료
        self.totalFee: float = 0
        # 총 거래대금
        self.totalTradeCost: float = 0
        # 슬리피지
        self.slippage: float = slippage
        self.datasetService = datasetService
        self.orderHistories: List[OrderHistory] = []
        self.histories: List[PortfolioHistory] = []

    @timeit
    def getHoldingsCount(self, symbol: str) -> int:
        counts = 0
        if symbol in self.holdings:
            for holding in self.holdings[symbol]:
                counts = counts + holding.count
        return counts

    def getHolingsAmounts(self, symbol: str):
        amounts = 0
        for holding in self.holdings[symbol]:
            amounts = amounts + (holding.buyPrice * holding.count)
        return amounts

    def applySlippage(self, price: float, count: int):
        # count > 0 이면 매수, count < 0 이면 매도
        return price * (1 + self.slippage) if count > 0 else price * (1 - self.slippage)

    def liquidate(self, date: datetime, exceptSymbols: List[str] = []):
        for symbol in self.holdings:
            # if symbol not in exceptSymbols:
            count = self.getHoldingsCount(symbol)
            self.addHolding(ShareHolding(symbol, date, -count), DEFAULT_SELL_FEE)

    @timeit
    def addHolding(self, share: ShareHolding, fee: float):
        if share.count == 0:
            return
        price = self.getStockPrice(share.symbol, share.buyDate)
        # 슬리피지 적용
        price = self.applySlippage(price, share.count)

        # 수수료 계산
        tradeCost = price * abs(share.count)
        allfee = tradeCost * fee

        if share.symbol not in self.holdings:
            self.holdings[share.symbol] = []
        # 매도
        if share.count < 0:
            # count가 부족하면 매도하지 않는다.
            if abs(share.count) <= self.getHoldingsCount(share.symbol):
                sellPrice = 0
                sellCount = abs(share.count)
                while sellCount > 0:
                    holding = self.holdings[share.symbol][0]
                    if holding.count <= sellCount:
                        sellPrice = sellPrice + price * holding.count
                        sellCount = sellCount - holding.count
                        self.holdings[share.symbol].remove(holding)
                    else:
                        self.holdings[share.symbol][0].count = holding.count - sellCount
                        sellPrice = sellPrice + price * sellCount
                        sellCount = 0
                self.cash = self.cash + (sellPrice - sellPrice * fee)
                share.buyPrice = price
                allfee = sellPrice * fee
                print(
                    "매도",
                    share.symbol,
                    share.count,
                    str(sellPrice - sellPrice * fee),
                    "매도후",
                    self.cash,
                )
            else:
                print("매도하지 않음")
                return
        # 매수
        else:
            while self.cash < (price * share.count) + (price * share.count) * fee:
                if share.count == 0:
                    print("현금이 부족합니다.")
                    return
                else:
                    share.count = share.count - 1
            # 현금이 매수금액보다 많아야 매수한다.
            share.buyPrice = price
            self.holdings[share.symbol].append(share)
            self.cash = self.cash - (price * share.count) - (price * share.count) * fee
            allfee = (price * share.count) * fee
            print(
                "매수",
                share.symbol,
                share.count,
                str((price * share.count) + (price * share.count) * fee),
            )
        self.totalBuyCost = self.totalBuyCost + (price * share.count)
        self.totalCost = self.totalCost + (price * share.count)
        self.totalFee = self.totalFee + allfee
        self.totalTradeCost = self.totalTradeCost + tradeCost

        # record order history
        orderHistory = OrderHistory(
            symbol=share.symbol,
            date=share.buyDate,
            count=share.count,
            price=price,
        )
        self.orderHistories.append(orderHistory)

    # entity 데이터 프레임을 받아서 현재가를 가져온다.
    def getStockPrice(self, symbol, date):
        row = self.datasetService.getEntitySymbolDateDict()[(symbol, date)]
        return row["adjClose"][0]

    def getMovingAverage(self, symbol, date, movingAverage):
        row = self.datasetService.getEntitySymbolDateDict()[(symbol, date)]
        return row[f"{movingAverage}MovingAverage"][0]

    def getMACD(self, symbol, date, minSMA, maxSMA):
        row = self.datasetService.getEntitySymbolDateDict()[(symbol, date)]
        return row[f"{minSMA}_{maxSMA}_MACD"][0]

    def setCash(self, cash):
        self.cash = cash
        self.totalAsset = self.totalCost + self.cash

    def getTimeHigh(self, date, relativedelta, benchType, benchSymbol):
        startTime = date - relativedelta
        entity = self.datasetService.getEntityByDate(
            benchType, benchSymbol, startTime, date
        )
        return entity.filter(pl.col("date") >= startTime)["adjClose"].max()

    def calculateDrawdown(df: pl.DataFrame):
        for column_name in df.columns:
            if not column_name.endswith("totalAsset"):
                continue
            df = df.drop_nulls(column_name)
            rolling_max = (
                pl.col(column_name)
                .rolling_max(window_size="50y_saturating", by="Date")
                .drop_nulls()
            )
            df = df.with_columns(
                ((pl.col(column_name) - rolling_max) / rolling_max * 100).alias(
                    column_name
                )
            )
        return df

    @staticmethod
    def convertDataForChart(reportName, data):
        allDf = pl.DataFrame(data).sort("date", descending=False)
        # symbol을 기준으로 DataFrame 분할
        # 새로운 DataFrame 생성 및 symbol에 따라 열 이름 변경
        new_dfs = []
        for symbol, sub_df in allDf.group_by("symbol", maintain_order=True):
            new_columns = {
                col: (
                    f"{reportName}_{symbol}_{col}"
                    if (
                        col != "date"
                        and col != "symbol"
                        and not col.startswith("total")
                    )
                    else f"{reportName}_{col}"
                )
                for col in sub_df.columns
            }
            del new_columns["symbol"]
            sub_df.drop_in_place("symbol")
            sub_df = sub_df.rename(new_columns)
            sub_df = sub_df.rename({f"{reportName}_date": "Date"})
            new_dfs.append(sub_df)
        merged_df: pl.DataFrame = new_dfs[0]
        for df in new_dfs[1:]:
            merged_df = merged_df.join(
                df,
                on="Date",
                how="left",
            )
            for col in merged_df.columns:
                if col.endswith("_right"):
                    merged_df.drop_in_place(col)
        return merged_df

    def writeChart(self, reportName: str = "report"):
        histories = [item for history in self.histories for item in history.toArray()]
        orders = [order.__dict__ for order in self.orderHistories]
        if len(histories) == 0:
            return

        historieChartData = Portfolio.convertDataForChart(reportName, histories)
        ordersChartData = Portfolio.convertDataForChart(reportName, orders)
        drawDownData = Portfolio.calculateDrawdown(historieChartData)
        os.makedirs(f"{Constants.PORTPOLIO_DIR}/{reportName}/chart", exist_ok=True)
        historieChartData.write_csv(
            f"{Constants.PORTPOLIO_DIR}/{reportName}/chart/history.csv"
        )
        ordersChartData.write_csv(
            f"{Constants.PORTPOLIO_DIR}/{reportName}/chart/order.csv"
        )
        drawDownData.write_csv(
            f"{Constants.PORTPOLIO_DIR}/{reportName}/chart/drawdown.csv"
        )

    def writeReport(self, reportName: str = "report"):
        os.makedirs(f"{Constants.PORTPOLIO_DIR}/{reportName}/history", exist_ok=True)
        os.makedirs(f"{Constants.PORTPOLIO_DIR}/{reportName}/order", exist_ok=True)
        histories = [item for history in self.histories for item in history.toArray()]
        if len(histories) == 0:
            return
        pl.DataFrame(histories).write_csv(
            f"{Constants.PORTPOLIO_DIR}/{reportName}/history/history.csv"
        )
        orders = [order.__dict__ for order in self.orderHistories]
        pl.DataFrame(orders).write_csv(
            f"{Constants.PORTPOLIO_DIR}/{reportName}/order/order.csv"
        )

    @timeit
    def updatePortfolio(self, currentDate):
        self.totals = {}
        self.totalCost = 0
        self.totalBuyCost = 0
        for symbol in self.holdings:
            symAllCurrentValue = 0
            symAllCount = 0
            symAllBuyValue = 0
            for holding in self.holdings[symbol]:
                buyValue = self.getStockPrice(symbol, holding.buyDate)
                # 슬리피지 적용
                buyValue = self.applySlippage(buyValue, holding.count)

                currentValue = self.getStockPrice(symbol, currentDate)
                currentValue = self.applySlippage(currentValue, -1)

                symAllBuyValue = symAllBuyValue + (buyValue * holding.count)
                symAllCurrentValue = symAllCurrentValue + (currentValue * holding.count)
                symAllCount = symAllCount + holding.count
            print(currentDate, symbol, symAllCurrentValue)

            etcProps = {}
            for mov in self.datasetService.movingAverages:
                etcProps[f"{mov}MovingAverage"] = self.getMovingAverage(
                    symbol, currentDate, mov
                )
            for macd in self.datasetService.macds:
                etcProps[f"{macd[0]}_{macd[1]}_MACD"] = self.getMACD(
                    symbol, currentDate, macd[0], macd[1]
                )
            self.totals[symbol] = ShareHoldingTotal(
                buyCost=symAllBuyValue,
                cost=symAllCurrentValue,
                count=symAllCount,
                averageCost=(symAllBuyValue / symAllCount) if symAllCount > 0 else 0,
                currentValue=self.applySlippage(
                    self.getStockPrice(symbol, currentDate), -1
                ),
                percent=(
                    symAllCurrentValue / symAllBuyValue if symAllBuyValue > 0 else 0
                ),
                properties=etcProps,
            )
            self.totalCost = self.totalCost + symAllCurrentValue
            self.totalBuyCost = self.totalBuyCost + symAllBuyValue
            self.totalAsset = self.totalCost + self.cash
        symbols = list(self.totals.keys())
        history = PortfolioHistory(
            currentDate,
            symbols,
            [self.totals[symbol] for symbol in symbols],
            self.totalAsset,
            self.totalCost,
            self.totalTradeCost,
            self.totalBuyCost,
        )
        self.histories.append(history)

    def __repr__(self) -> str:
        return f"holdings: {self.holdings}, totals: {self.totals}, totalCost: {self.totalCost}, totalBuyCost: {self.totalBuyCost}, cash: {self.cash}, totalAsset: {self.totalAsset}, totalFee: {self.totalFee}"


class BackTest(object):
    def __init__(self):
        self.entityDict = {}
        self.entityData = {}
        self.balance: float = 0.0
        self.holdings = []
        self.currentDate = None
        self.startDate = None
        self.endDate = None
        self.schedules: List[Schedule] = []
        self.portfolios: Dict[str, Portfolio] = {}
        self.datasetService: DatasetService = DatasetService()
        self.initalize()

    def initalize(self):
        pass

    def addPortfolio(self, name: str, percent: float = 0, cash: float = 0.0):
        if percent is not None and percent != 0:
            cash = self.balance * percent
        self.portfolios[name] = Portfolio(self.datasetService)
        self.portfolios[name].setCash(cash)
        self.balance = self.balance - cash

    def getPortfolio(self, name: str):
        return self.portfolios[name]

    @timeit
    def addEntity(self, entityType: Constants.EntityType, symbol):
        self.datasetService.getEntity(entityType, symbol)
        self.entityDict = self.datasetService.getEntitySymbolDateDict()

    def setBalance(self, balance):
        self.balance = balance

    def getPrice(self, symbol, date):
        row = self.entityDict[(symbol, date)]
        return row["adjClose"][0]

    def getCurrentData(self, symbol, date, column):
        return self.entityDict[(symbol, date)][column][0]

    def checkLosscutByHigh(
        self, portfolioName: str, benchType, benchSymbol, currentDate, period, percent
    ):
        high = self.getPortfolio(portfolioName).getTimeHigh(
            currentDate, period, benchType, benchSymbol
        )
        cost = self.getPortfolio(portfolioName).getStockPrice(benchSymbol, currentDate)
        return cost / high < percent

    def losscutByHigh(
        self, portfolioName, benchType, benchSymbol, currentDate, period, percent
    ):
        isLosscut = self.checkLosscutByHigh(
            portfolioName, benchType, benchSymbol, currentDate, period, percent
        )
        if isLosscut:
            self.getPortfolio(portfolioName).liquidate(currentDate)
            return True
        return False

    def losscut(self, portfolioName, currentDate, percent):
        buyCost = self.getPortfolio(portfolioName).totalBuyCost
        cost = self.getPortfolio(portfolioName).totalCost
        if buyCost == 0:
            return False
        losscutObj = {
            "date": currentDate,
            "portfolio": portfolioName,
            "cost": cost,
            "buyCost": buyCost,
            "percent": cost / buyCost,
        }
        if self.losscutDf is None:
            self.losscutDf = pl.DataFrame([losscutObj])
        else:
            self.losscutDf = self.losscutDf.vstack(pl.DataFrame([losscutObj]))
        if cost / buyCost < percent:
            self.getPortfolio(portfolioName).liquidate(currentDate)
            return True
        return False

    def buyByCount(
        self,
        portfolioName: str,
        symbol: str,
        date: datetime,
        count: int,
        fee: float = DEFAULT_BUY_FEE,
    ) -> ShareHolding:
        portfolio: Portfolio = self.getPortfolio(portfolioName)
        share = ShareHolding(symbol, date, count)
        portfolio.addHolding(share, fee)
        return share

    def buyByAmounts(
        self,
        portfolioName: str,
        symbol: str,
        date: datetime,
        amounts: float,
        fee: float = DEFAULT_BUY_FEE,
    ) -> ShareHolding:
        portfolio: Portfolio = self.getPortfolio(portfolioName)
        price = self.getPrice(symbol, date)
        count = amounts / price
        share = ShareHolding(symbol, date, count)
        portfolio.addHolding(share, fee)
        return share

    def sellByCount(
        self,
        portfolioName: str,
        symbol: str,
        date: datetime,
        count: int,
        fee: float = DEFAULT_SELL_FEE,
    ) -> ShareHolding:
        portfolio: Portfolio = self.getPortfolio(portfolioName)
        share = ShareHolding(symbol, date, -count)
        portfolio.addHolding(share, fee)
        return share

    def sellByAmounts(
        self,
        portfolioName: str,
        symbol: str,
        date: datetime,
        amounts: float,
        fee: float = DEFAULT_SELL_FEE,
    ) -> ShareHolding:
        portfolio: Portfolio = self.getPortfolio(portfolioName)
        price = self.getPrice(symbol, date)
        count = amounts / price
        share = ShareHolding(symbol, date, count)
        portfolio.addHolding(share, fee)
        return share

    @timeit
    def rebalanceByPercent(
        self,
        portfolioName,
        orders: RebalanceOrders,
        buyFee: float = DEFAULT_BUY_FEE,
        sellFee: float = DEFAULT_SELL_FEE,
    ) -> Dict[str, ShareHolding]:
        portfolio: Portfolio = self.getPortfolio(portfolioName)
        holdings: Dict[str, ShareHolding] = {}
        portfolio.liquidate(self.currentDate, [order.symbol for order in orders.items])
        for order in orders.items:
            price = self.getPrice(order.symbol, self.currentDate)
            if price is 0:
                continue
            count = math.floor((portfolio.totalAsset * order.percent) / price)
            haveCount = portfolio.getHoldingsCount(order.symbol)
            holdings[order.symbol] = ShareHolding(
                order.symbol, self.currentDate, count - haveCount
            )
            portfolio.addHolding(
                holdings[order.symbol],
                buyFee if holdings[order.symbol].count > 0 else sellFee,
            )
        return holdings

    def setStartDate(self, startDate):
        if isinstance(startDate, str):
            startDate = datetime.strptime(startDate, "%Y%m%d")
        self.startDate = startDate
        self.currentDate = startDate

    def setEndDate(self, endDate):
        if isinstance(endDate, str):
            endDate = datetime.strptime(endDate, "%Y%m%d")
        self.endDate = endDate

    def setCurrentDate(self, currentDate):
        self.currentDate = currentDate

    def addSchedules(self, schedules: Schedule):
        if schedules.startDate is None:
            schedules.startDate = self.startDate
        if schedules.endDate is None:
            schedules.endDate = self.endDate
        if schedules.currentDate is None:
            schedules.currentDate = self.currentDate
        self.schedules.append(schedules)

    def onSchedule(self, name: str, date: datetime):
        pass

    def onScheduleEndOfMarket(self, name: str, date: datetime):
        pass

    def onEndOfBackTest(self):
        pass

    def writeAllReport(self):
        if len(self.portfolios) <= 1:
            return
        reportName = "_".join(self.portfolios.keys())
        allHistories = []
        allOrderHistories = []
        for key in self.portfolios:
            histories = [
                item
                for history in self.portfolios[key].histories
                for item in history.toArray()
            ]
            allHistories.extend(histories)
            orders = [order.__dict__ for order in self.portfolios[key].orderHistories]
            allOrderHistories.extend(orders)

        os.makedirs(f"{Constants.PORTPOLIO_DIR}/{reportName}/history", exist_ok=True)
        os.makedirs(f"{Constants.PORTPOLIO_DIR}/{reportName}/order", exist_ok=True)
        if len(allHistories) == 0:
            return
        pl.DataFrame(allHistories).write_csv(
            f"{Constants.PORTPOLIO_DIR}/{reportName}/history/history.csv"
        )
        pl.DataFrame(allOrderHistories).write_csv(
            f"{Constants.PORTPOLIO_DIR}/{reportName}/order/order.csv"
        )

    def writeAllChart(self):
        if len(self.portfolios) <= 1:
            return
        reportName = "_".join(self.portfolios.keys())
        histDfs = []
        orderDfs = []
        drawDownDfs = []

        for key in self.portfolios:
            histories = [
                item
                for history in self.portfolios[key].histories
                for item in history.toArray()
            ]
            orders = [order.__dict__ for order in self.portfolios[key].orderHistories]
            histChartDfs = Portfolio.convertDataForChart(key, histories)
            histDfs.append(histChartDfs)
            orderDfs.append(Portfolio.convertDataForChart(key, orders))
            drawDownDfs.append(Portfolio.calculateDrawdown(histChartDfs))

        if len(histDfs) == 0:
            return
        historieChartData = reduce(
            lambda acc, cur: acc.join(cur, on="Date", how="left"), histDfs
        )
        ordersChartData = reduce(
            lambda acc, cur: acc.join(cur, on="Date", how="left"), orderDfs
        )
        drawDownChartData = reduce(
            lambda acc, cur: acc.join(cur, on="Date", how="left"), drawDownDfs
        )
        os.makedirs(f"{Constants.PORTPOLIO_DIR}/{reportName}/chart", exist_ok=True)
        historieChartData.write_csv(
            f"{Constants.PORTPOLIO_DIR}/{reportName}/chart/history.csv"
        )
        ordersChartData.write_csv(
            f"{Constants.PORTPOLIO_DIR}/{reportName}/chart/order.csv"
        )
        drawDownChartData.write_csv(
            f"{Constants.PORTPOLIO_DIR}/{reportName}/chart/drawdown.csv"
        )

    @timeit
    def run(self):
        self.setCurrentDate(self.startDate)
        if self.portfolios is not None:
            for name in self.portfolios:
                self.portfolios[name].updatePortfolio(self.currentDate)
        while self.currentDate <= self.endDate:
            for schedule in self.schedules:
                if schedule is not None:
                    if (
                        schedule.currentDate.year == self.currentDate.year
                        and schedule.currentDate.month == self.currentDate.month
                        and schedule.currentDate.day == self.currentDate.day
                    ):
                        self.onSchedule(schedule.name, self.currentDate)
            if self.portfolios is not None:
                for name in self.portfolios:
                    self.portfolios[name].updatePortfolio(self.currentDate)
            for schedule in self.schedules:
                if schedule is not None:
                    if (
                        schedule.currentDate.year == self.currentDate.year
                        and schedule.currentDate.month == self.currentDate.month
                        and schedule.currentDate.day == self.currentDate.day
                    ):
                        self.onScheduleEndOfMarket(schedule.name, self.currentDate)
                        schedule.next()
            self.currentDate = self.currentDate + timedelta(days=1)
        print("end cycle")
        for key in self.portfolios:
            self.portfolios[key].writeReport(key)
            self.portfolios[key].writeChart(key)
        print("end portfolio record")
        self.writeAllReport()
        self.writeAllChart()
        self.onEndOfBackTest()
        print("BackTest End!")
