from BackTest import (
    BackTest,
    Portfolio,
    ShareHolding,
    Schedule,
    Interval,
    RebalanceOrder,
)
import services.DatasetService as DatasetService
from datetime import datetime


def test_portfolio():
    portPolio = Portfolio()
    holding = ShareHolding()
    holding.symbol = "AAPL"
    holding.buyDate = datetime.strptime("20100127", "%Y%m%d")
    holding.count = 10
    portPolio.addHolding(holding)
    portPolio.updatePortfolio(
        DatasetService.getEntity("AAPL"), datetime.strptime("20200127", "%Y%m%d")
    )
    assert portPolio.cost == 772.4
    assert portPolio.buyCost == 74.2


class TestBackTest(BackTest):
    def initalize(self):
        self.setStartDate("20080101")
        self.setEndDate("20200110")
        self.addEntity("AAPL")
        self.addEntity("BIL")
        self.setBalance(10000)
        self.addPortfolio("testPortfolio", 1.0)
        self.setSchedules(
            Schedule(
                startDate=self.startDate, endDate=self.endDate, interval=Interval(day=1)
            )
        )

    def onSchedule(self, date: datetime):
        print("onSchedule", date)
        self.rebalanceByPercent(
            "testPortfolio",
            RebalanceeOrders(
                RebalanceOrder("AAPL", 0.5),
                RebalanceOrder("BIL", 0.5),
            ),
        )

    def onScheduleEndOfMarket(self, date: datetime):
        print("portFolio", self.getPortfolio("testPortfolio"))


def test_backtest():
    TestBackTest().run()
