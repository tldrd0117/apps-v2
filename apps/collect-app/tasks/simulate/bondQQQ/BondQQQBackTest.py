from tasks.simulate.common.BackTest import (
    BackTest,
    Schedule,
    Interval,
    RebalanceOrders,
    RebalanceOrder,
)
from tasks.Constants import EntityType
from datetime import datetime


class BondQQQBackTest(BackTest):
    def initalize(self):
        self.losscutDf = None
        self.isLosscut = False
        self.setBalance(40000)
        self.setStartDate("20110131")
        self.setEndDate("20240229")
        self.datasetService.addMACD("12mo", "26mo")
        entities = ["QQQ", "QLD", "TQQQ", "TLT", "SPY"]
        for entity in entities:
            self.addEntity(EntityType.USA_STOCK, entity)
        self.addPortfolio("BondQQQ", 0.25)
        self.addPortfolio("BondQLD", 0.33)
        self.addPortfolio("BondTQQQ", 0.5)
        self.addPortfolio("BondSPY", 1)

        self.addSchedules(
            Schedule(
                name="daily",
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(day=1),
            )
        )

        self.addSchedules(
            Schedule(
                name="monthly",
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(month=1),
            )
        )

    def onSchedule(self, name: str, date: datetime):
        if name == "monthly":
            self.rebalanceByPercent(
                "BondQQQ",
                RebalanceOrders(
                    RebalanceOrder("QQQ", 0.6),
                    RebalanceOrder("TLT", 0.4),
                ),
            )
            self.rebalanceByPercent(
                "BondQLD",
                RebalanceOrders(
                    RebalanceOrder("QLD", 0.6),
                    RebalanceOrder("TLT", 0.4),
                ),
            )
            self.rebalanceByPercent(
                "BondSPY",
                RebalanceOrders(
                    RebalanceOrder("SPY", 0.6),
                    RebalanceOrder("TLT", 0.4),
                ),
            )
            if not self.isLosscut:
                self.rebalanceByPercent(
                    "BondTQQQ",
                    RebalanceOrders(
                        RebalanceOrder("TQQQ", 0.6),
                        RebalanceOrder("TLT", 0.4),
                    ),
                )
            else:
                self.isLosscut = False

        if name == "daily":
            self.losscut("BondQQQ", date, 0.95)
            self.losscut("BondQLD", date, 0.95)
            self.isLosscut = self.losscut("BondTQQQ", date, 0.95)
