from tasks.simulate.common.BackTest import (
    BackTest,
    Schedule,
    Interval,
    RebalanceOrders,
    RebalanceOrder,
)
from tasks.Constants import EntityType
from datetime import datetime
from dateutil.relativedelta import relativedelta


class QQQSeasonLosscutBackTest(BackTest):
    def initalize(self):
        self.losscutDf = None
        self.isLosscut = False
        self.beforeLosscut = True
        self.setBalance(40000)
        self.setStartDate("20110131")
        self.setEndDate("20240229")
        self.datasetService.addMovingAverage("120d")
        entities = ["QQQ", "QLD", "TQQQ", "TLT", "SPY"]
        for entity in entities:
            self.addEntity(EntityType.USA_STOCK, entity)
        self.addPortfolio("QQQSeasonLosscut", 0.25)
        self.addPortfolio("QLDSeasonLosscut", 0.33)
        self.addPortfolio("TQQQSeasonLosscut", 0.5)
        self.addPortfolio("TQQQ", 1)

        self.addSchedules(
            Schedule(
                name="daily",
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(day=1),
            )
        )

        self.addSchedules(
            Schedule(
                name="monthly",
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(month=1),
            )
        )
        self.rebalanceByPercent(
            "TQQQ",
            RebalanceOrders(
                RebalanceOrder("TQQQ", 1),
            ),
        )

    def onSchedule(self, name: str, date: datetime):
        swithYear = [2014, 2016, 2018, 2022]
        # swithYear = []
        if name == "monthly":
            qqqPercent = 1
            qldPercent = 1
            tqqqPercent = 1
            # if date.year in swithYear:
            #     qqqPercent = 0
            #     qldPercent = 0
            #     tqqqPercent = 0
            # print(tqqqPercent)
            self.rebalanceByPercent(
                "QQQSeasonLosscut",
                RebalanceOrders(
                    RebalanceOrder("QQQ", qqqPercent),
                ),
            )
            self.rebalanceByPercent(
                "QLDSeasonLosscut",
                RebalanceOrders(
                    RebalanceOrder("QLD", qldPercent),
                ),
            )
            # if not self.isLosscut:
            self.rebalanceByPercent(
                "TQQQSeasonLosscut",
                RebalanceOrders(
                    RebalanceOrder("TQQQ", tqqqPercent),
                ),
            )
        if name == "daily":
            # self.losscutByHigh("QQQSeasonLosscut", date, relativedelta(months=36), 0.8)
            # self.losscutByHigh("QLDSeasonLosscut", date, relativedelta(months=36), 0.8)

            # self.isLosscut = self.checkLosscutByHigh(
            #     "TQQQSeasonLosscut",
            #     EntityType.USA_STOCK,
            #     "TQQQ",
            #     date,
            #     relativedelta(months=18),
            #     0.8,
            # )

            # if self.isLosscut and not self.beforeLosscut:
            #     self.rebalanceByPercent(
            #         "TQQQSeasonLosscut",
            #         RebalanceOrders(
            #             RebalanceOrder("TQQQ", 0),
            #         ),
            #     )
            # elif not self.isLosscut and self.beforeLosscut:
            #     self.rebalanceByPercent(
            #         "TQQQSeasonLosscut",
            #         RebalanceOrders(
            #             RebalanceOrder("TQQQ", 1),
            #         ),
            #     )
            # self.beforeLosscut = self.isLosscut

            # self.isLosscut = self.losscutByHigh(
            #     "TQQQSeasonLosscut",
            #     EntityType.USA_STOCK,
            #     "TQQQ",
            #     date,
            #     relativedelta(months=18),
            #     0.75,
            # )

            if date.year in swithYear:
                movingAverage = self.getCurrentData("TQQQ", date, "120dMovingAverage")
                currentClose = self.getCurrentData("TQQQ", date, "adjClose")
                if movingAverage > currentClose:
                    self.rebalanceByPercent(
                        "TQQQSeasonLosscut",
                        RebalanceOrders(
                            RebalanceOrder("TQQQ", 0),
                        ),
                    )

            self.losscut("QQQSeasonLosscut", date, 0.8)
            self.losscut("QLDSeasonLosscut", date, 0.8)
            self.losscut("TQQQSeasonLosscut", date, 0.8)
