from tasks.simulate.common.BackTest import (
    BackTest,
    Schedule,
    Interval,
    RebalanceOrders,
    RebalanceOrder,
)
from datetime import datetime, timedelta
from tasks.Constants import EntityType


class BTCLosscutBackTest(BackTest):
    def initalize(self):
        self.losscutDf = None
        self.setBalance(30000)
        self.setStartDate("20180131")
        self.setEndDate("20240229")
        self.addEntity(EntityType.CRYPTO, "BTCUSDT")
        self.addEntity(EntityType.USA_STOCK, "TQQQ")
        self.addPortfolio("BTCLosscut", cash=20000)
        self.addPortfolio("TQQQLosscut", cash=10000)

        # self.rebalanceByPercent(
        #     "BTCMovingAverage", RebalanceOrders(RebalanceOrder("BTCUSDT", 1.0))
        # )
        self.addSchedules(
            Schedule(
                name="daily",
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(day=1),
            )
        )

        self.addSchedules(
            Schedule(
                name="monthly",
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(month=1),
            )
        )

    def onSchedule(self, name: str, date: datetime):
        swithYear = [2022, 2014, 2006]
        if name == "monthly":
            tqqqPercent = 1
            btcPercent = 1
            if date.year in swithYear:
                tqqqPercent = 0
                btcPercent = 0
            self.rebalanceByPercent(
                "BTCLosscut", RebalanceOrders(RebalanceOrder("BTCUSDT", btcPercent))
            )
            self.rebalanceByPercent(
                "TQQQLosscut", RebalanceOrders(RebalanceOrder("TQQQ", tqqqPercent))
            )
        if name == "daily":
            self.isLosscut = self.losscut("TQQQLosscut", date, 0.8)
