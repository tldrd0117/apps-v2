from tasks.simulate.common.BackTest import (
    BackTest,
    Schedule,
    Interval,
    RebalanceOrders,
    RebalanceOrder,
)
from datetime import datetime, timedelta
from tasks.Constants import EntityType


class BTCHodleBackTest(BackTest):
    def initalize(self):
        self.setBalance(90000)
        self.setStartDate("20180131")
        self.setEndDate("20240316")
        self.datasetService.addYieldDates("100d")
        self.datasetService.addYieldDates("20d")
        self.datasetService.addMovingAverage("99d")
        self.datasetService.addMovingAverage("20d")
        self.addEntity(EntityType.CRYPTO, "BTCUSDT")
        self.addEntity(EntityType.CRYPTO, "ETHUSDT")
        self.addEntity(EntityType.USA_STOCK, "QQQ")
        self.addPortfolio("BTCHodle", cash=30000)
        self.addPortfolio("ETHHodle", cash=30000)
        self.addPortfolio("BTCMovingAverage", cash=30000)
        self.rebalanceByPercent(
            "BTCHodle", RebalanceOrders(RebalanceOrder("BTCUSDT", 1.0))
        )
        self.rebalanceByPercent(
            "ETHHodle", RebalanceOrders(RebalanceOrder("ETHUSDT", 1.0))
        )

        # self.rebalanceByPercent(
        #     "BTCMovingAverage", RebalanceOrders(RebalanceOrder("BTCUSDT", 1.0))
        # )
        self.setSchedules(
            Schedule(
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(day=1),
            )
        )

    def onSchedule(self, date: datetime):
        date = date - timedelta(days=1)
        currentPrice = self.getPrice("BTCUSDT", date)
        comparePrice = self.getCurrentData("BTCUSDT", date, "99dMovingAverage")
        comp20Price = self.getCurrentData("BTCUSDT", date, "20dMovingAverage")
        yields100 = self.getCurrentData("BTCUSDT", date, "100dYield")
        yields20 = self.getCurrentData("BTCUSDT", date, "20dYield")
        if (
            yields100 > 0
            or yields20 > 50
            or self.portfolios["BTCMovingAverage"].getHoldingsCount("BTCUSDT") > 0
        ):
            if currentPrice > comparePrice:
                self.rebalanceByPercent(
                    "BTCMovingAverage", RebalanceOrders(RebalanceOrder("BTCUSDT", 1.0))
                )
            else:
                self.rebalanceByPercent(
                    "BTCMovingAverage", RebalanceOrders(RebalanceOrder("BTCUSDT", 0))
                )
