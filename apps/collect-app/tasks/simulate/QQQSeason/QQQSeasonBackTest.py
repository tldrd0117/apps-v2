from tasks.simulate.common.BackTest import (
    BackTest,
    Schedule,
    Interval,
    RebalanceOrders,
    RebalanceOrder,
)
from tasks.Constants import EntityType
from datetime import datetime


class QQQSeasonBackTest(BackTest):
    def initalize(self):
        self.losscutDf = None
        self.isLosscut = False
        self.setBalance(30000)
        self.setStartDate("20110131")
        self.setEndDate("20240229")
        entities = ["QQQ", "QLD", "TQQQ", "TLT", "SPY"]
        for entity in entities:
            self.addEntity(EntityType.USA_STOCK, entity)
        self.addPortfolio("QQQSeason", 0.33)
        self.addPortfolio("QLDSeason", 0.5)
        self.addPortfolio("TQQQSeason", 1)

        self.addSchedules(
            Schedule(
                name="monthly",
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(month=1),
            )
        )

    def onSchedule(self, name: str, date: datetime):
        swithYear = [2022, 2014, 2006]
        if name == "monthly":
            qqqPercent = 1
            qldPercent = 1
            tqqqPercent = 1
            if date.year in swithYear:
                qqqPercent = 0
                qldPercent = 0
                tqqqPercent = 0
            self.rebalanceByPercent(
                "QQQSeason",
                RebalanceOrders(
                    RebalanceOrder("QQQ", qqqPercent),
                ),
            )
            self.rebalanceByPercent(
                "QLDSeason",
                RebalanceOrders(
                    RebalanceOrder("QLD", qldPercent),
                ),
            )
            self.rebalanceByPercent(
                "TQQQSeason",
                RebalanceOrders(
                    RebalanceOrder("TQQQ", tqqqPercent),
                ),
            )
