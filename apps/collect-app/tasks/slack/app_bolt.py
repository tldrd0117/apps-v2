import asyncio

from dataSource.envs import getEnv

from slack_bolt import App
# from slack_bolt.adapter.socket_mode.async_handler import AsyncSocketModeHandler
from slack_bolt.adapter.socket_mode import SocketModeHandler
import logging

logger = logging.getLogger("bolt")

app = App(
    token=getEnv("SLACK_BOT_TOKEN"),
)

channelName = "#luigi-task"

client = app.client

def importModules():
    import tasks.slack.luigi.actions
    import tasks.slack.luigi.messages

@app.message("hello")
def message_hello(message, say):
    say("he!!")
    
# async def startBolt():
#     await AsyncSocketModeHandler(app, getEnv("SLACK_APP_TOKEN")).start_async()

def connectBolt():
    importModules()
    return SocketModeHandler(app, getEnv("SLACK_APP_TOKEN")).connect()
    # return await SocketModeHandler(app, getEnv("SLACK_APP_TOKEN")).connect_async()