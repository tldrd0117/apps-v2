from tasks.all import *
from ..app_bolt import app, channelName, client
from datetime import date, datetime
import luigi
import threading
from multiprocessing import Process


@app.action("echo")
def echoAction(ack, body, say):
    ack()
    block_id = body["message"]["attachments"][0]["blocks"][0]["block_id"]
    say(block_id)
    say(
        f"{body['state']['values'][block_id]['echo']['selected_option']['value']} selectedOption!"
    )


@app.action("getTaskParams")
def getTaskParamsAction(ack, body, say):
    ack()
    block_id = body["message"]["attachments"][0]["blocks"][0]["block_id"]
    taskName = body["state"]["values"][block_id]["getTaskParams"]["selected_option"][
        "value"
    ]
    params = getTaskParams(str(taskName))
    blocks = []
    for param in params:
        paramType = type(param[1]).__name__
        name = param[0]
        defaultValue = param[1]._default
        if paramType == "DateParameter":
            if defaultValue is not None and type(defaultValue) is not object:
                defaultValue = date.strftime(defaultValue, "%Y-%m-%d")
            else:
                defaultValue = datetime.today().strftime("%Y-%m-%d")
            blocks.append(
                {
                    "type": "input",
                    "element": {
                        "type": "datepicker",
                        "initial_date": defaultValue,
                        "placeholder": {
                            "type": "plain_text",
                            "text": "Select a date",
                            "emoji": True,
                        },
                        "action_id": name + "-action",
                    },
                    "label": {"type": "plain_text", "text": name, "emoji": True},
                }
            )
        else:
            if defaultValue is None:
                defaultValue = ""
            blocks.append(
                {
                    "type": "input",
                    "element": {
                        "initial_value": defaultValue,
                        "type": "plain_text_input",
                        "action_id": name + "-action",
                    },
                    "label": {"type": "plain_text", "text": name, "emoji": True},
                }
            )
    blocks.append(
        {
            "type": "actions",
            "elements": [
                {
                    "type": "button",
                    "text": {"type": "plain_text", "text": "Click Me", "emoji": True},
                    "value": taskName,
                    "action_id": "triggerTask",
                }
            ],
        }
    )
    client.chat_postMessage(channel=channelName, blocks=blocks)


@app.action("triggerTask")
def triggerTaskAction(ack, body, say):
    ack()
    blockIds = []
    for block in body["message"]["blocks"]:
        blockIds.append(block["block_id"])
    params = []
    taskName = body["actions"][0]["value"]
    for blockId in blockIds:
        if blockId in body["state"]["values"]:
            for key in body["state"]["values"][blockId].keys():
                if body["state"]["values"][blockId][key]["type"] == "datepicker":
                    dt = body["state"]["values"][blockId][key]["selected_date"]
                    pName = key.replace("-action", "")
                    params.append(pName + f'=datetime.strptime("{dt}", "%Y-%m-%d")')
                else:
                    pName = key.replace("-action", "")
                    params.append(
                        pName + f'="{body["state"]["values"][blockId][key]["value"]}"'
                    )
    paramString = ",".join(params)
    task = eval(f"{taskName}({paramString})")
    say(str("trigger: " + taskName + " " + str(params)))
    p = Process(target=runTask, args=(task,))
    p.start()
    p.join()


def runTask(task):
    if hasattr(task, "workers"):
        luigi.build([task], workers=task.workers, detailed_summary=True)
    else:
        luigi.build([task], workers=5, detailed_summary=True)
    # luigi.build([], workers=1, detailed_summary=True)
