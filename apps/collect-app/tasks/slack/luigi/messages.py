from tasks.all import getRunnableTasks, getTaskParams, taskMap
from ..app_bolt import app, channelName, client

@app.message("luigi task")
def luigiCmd(message, say):
    tasks = [{"text": {
                "type": "plain_text",
                "text": task,
                "emoji": True
            },
            "value": task} for task in getRunnableTasks()]
    client.chat_postMessage(
        channel=channelName,
        attachments= [
            {
                "blocks": [
                    {
                        "type": "actions",
                        "elements": [
                            {
                                "type": "static_select",
                                "placeholder": {
                                    "type": "plain_text",
                                    "text": "Select an Task",
                                    "emoji": True
                                },
                                "options": tasks,
                                "action_id": "getTaskParams"
                            }
                        ]
                    }
                ]
            }
        ])