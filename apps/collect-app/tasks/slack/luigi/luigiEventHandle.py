import luigi
from tasks.all import getRunnableTasks, getTaskParams, taskMap
from ..app_bolt import app, channelName, client



@luigi.Task.event_handler(luigi.Event.START)
def start(task):
    """Will be called directly before `run` on any Task subclass
    (i.e. all luigi Tasks)
    """
    print("Start Task!")
    taskName = task.__class__.__name__
    params = str(task.to_str_params())
    message = ":rocket:Start Task! \ntask:%s\nparams:%s" % (taskName, params)
    app.client.chat_postMessage(
        channel=channelName,
        attachments=[
		{
			"blocks": [
				{
					"type": "section",
					"text": {
						"type": "mrkdwn",
						"text": message
					}
				}
			]
		}
	])
    

@luigi.Task.event_handler(luigi.Event.SUCCESS)
def celebrate_success(task: luigi.Task):
    """Will be called directly after a successful execution
    of `run` on any Task subclass (i.e. all luigi Tasks)
    """
    print("Success Task!")
    taskName = task.__class__.__name__
    params = str(task.to_str_params())
    message = ":dart:Success Task! \ntask:%s\nparams:%s" % (taskName, params)
    app.client.chat_postMessage(
        channel=channelName,
        attachments=[
        {
            "color": "#36a64f",
            "blocks": [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": message
                    }
                }
            ]
        }
    ])
    

@luigi.Task.event_handler(luigi.Event.PROGRESS)
def celebrate_progress(task, progress):
    """Will be called when the Task's progress changes.
    E.g., if the Task's `run` method calls `self.set_progress_percentage(10)`,
    this callback will be invoked with `progress=10`.
    """
    app.client.chat_postMessage(channel=channelName, text="Progress!")

@luigi.Task.event_handler(luigi.Event.FAILURE)
def mourn_failure(task, exception):
    """Will be called directly after a failed execution
    of `run` on any JobTask subclass
    """
    error = ":exclamation:We failed with exception %s" % exception
    app.client.chat_postMessage(
        channel=channelName,
        attachments=[
        {
            "color": "#ff0000",
            "blocks": [
                {
                    "type": "section",
                    "text": {
                        "type": "mrkdwn",
                        "text": error
                    }
                }
            ]
        }
    ])