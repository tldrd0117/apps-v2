from tasks.collect.dartFinancialStatement.DartFinancialStatementTask import *
from tasks.collect.krxMarcap.KrxMarcapTask import *
from tasks.collect.seibroDividend.SeibroDividendTask import *
from tasks.collect.seibroStockNum.SeibroStockNumTask import *
from tasks.convert.adjuestedStock.AdjustedStockTask import *
import json
import logging


logger = logging.getLogger(__name__)

def run(taskName: str, params: str):
    try:
        task = None
        paramDict = json.loads(params)
        paramString = ""
        for key in paramDict:
            value = paramDict[key]
            if len(value) == 8 and value.isnumeric():
                value = datetime.strptime(value, "%Y%m%d")
            paramString += f"{key}={paramDict[key]},"
        cmd = f"{taskName}({paramString})"
        task = eval(cmd)
        print(f"cmd: {cmd} {task}")
        if task is None:
            raise Exception("invalid taskName")
        luigi.build([task], workers=1, detailed_summary=True)
    except Exception as e:
        print(e)
        raise e
