import api.binance.apis as apis
import time
from dataSource.mongoClient import getCollection
from datetime import datetime, timedelta
import tasks.collect.binance.service.collect_crypto_service as service


def test_getCryptoPriceAll():
    service.saveCryptoPrice()


def test_getCryptoPriceByDate():
    date = service.getLastHistoryDate()
    service.saveCryptoPriceByDate(date + timedelta(days=1))


def test_deletePrice():
    date = datetime(2024, 3, 4)
    service.deleteCryptoPrice(date)
