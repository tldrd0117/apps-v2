import api.binance.apis as apis
import time
from dataSource.mongoClient import getCollection
from datetime import datetime
import tasks.utils.dateUtils as dateUtils


def saveCryptoSymbols():
    res = apis.getExchangeInfo()
    symbols = [item["symbol"] for item in res["symbols"]]
    symbols.sort()
    getCollection("binance_crypto_symbols").drop()
    return getCollection("binance_crypto_symbols").insert_many(
        [{"symbol": sym} for sym in symbols]
    )


def getCryptoPriceAll(symbol):
    date = datetime(1980, 1, 1)
    allData = []
    beforeStartTime = None
    while date < dateUtils.getTodayDatetimeByHour(9):
        print(symbol, beforeStartTime, int(date.timestamp() * 1e3))
        if beforeStartTime == int(date.timestamp() * 1e3):
            break
        json = apis.getCryptoPrice(symbol=symbol, startTime=int(date.timestamp() * 1e3))
        for data in json:
            if len(data) <= 0 or isinstance(data[0], str):
                continue
            beforeStartTime = int(date.timestamp() * 1e3)
            date = datetime.fromtimestamp(int(data[0]) / 1e3)
            obj = {
                "symbol": symbol,
                "date": date,
                "open": data[1],
                "high": data[2],
                "low": data[3],
                "close": data[4],
                "adjClose": data[4],
                "volume": data[5],
                "unadjustedVolume": data[5],
                "vwap": 0,
                "label": "",
                "changeOverTime": 0,
                "change": 0,
                "changePercent": 0,
            }
            allData.append(obj)
    return allData


def getCryptoPriceByDate(symbol, startDate):
    date = startDate
    allData = []
    beforeStartTime = None
    print(date, dateUtils.getTodayDatetimeByHour(9))
    while date < dateUtils.getTodayDatetimeByHour(9):
        print(symbol, beforeStartTime, int(date.timestamp() * 1e3))
        if beforeStartTime == int(date.timestamp() * 1e3):
            break
        json = apis.getCryptoPrice(symbol=symbol, startTime=int(date.timestamp() * 1e3))
        for data in json:
            if len(data) <= 0 or isinstance(data[0], str):
                continue
            beforeStartTime = int(date.timestamp() * 1e3)
            date = datetime.fromtimestamp(int(data[0]) / 1e3)
            obj = {
                "symbol": symbol,
                "date": date,
                "open": data[1],
                "high": data[2],
                "low": data[3],
                "close": data[4],
                "adjClose": data[4],
                "volume": data[5],
                "unadjustedVolume": data[5],
                "vwap": 0,
                "label": "",
                "changeOverTime": 0,
                "change": 0,
                "changePercent": 0,
            }
            allData.append(obj)
        if beforeStartTime is None:
            break
    return allData


def getLastHistoryDate():
    fmpUSAHistory = getCollection("binance_crypto_price")
    latest_document = fmpUSAHistory.find_one(sort=[("date", -1)])
    if latest_document is not None:
        return latest_document["date"]
    else:
        return None


def saveCryptoPrice():
    symbols = getCollection("binance_crypto_symbols").find()
    for object in symbols:
        data = getCryptoPriceAll(object["symbol"])
        if len(data) > 0:
            getCollection("binance_crypto_price").insert_many(data)


def deleteCryptoPrice(date):
    getCollection("binance_crypto_price").delete_many({"date": {"$gt": date}})


def saveCryptoPriceByDate(date: datetime):
    symbols = getCollection("binance_crypto_symbols").find()
    for object in symbols:
        data = getCryptoPriceByDate(object["symbol"], date)
        if len(data) > 0:
            getCollection("binance_crypto_price").insert_many(data)
