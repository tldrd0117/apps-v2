import luigi
from tasks.baseTask import BaseTask, WrapperBaseTask
from tasks.targets.MongoFindTarget import MongoFindTarget
from tasks.utils.dateUtils.dateUtils import getDateRangeStr
from dataSource.mongoClient import getClient, getDB, getDBName
from tasks.collect.binance.service.collect_crypto_service import (
    saveCryptoPrice,
    saveCryptoSymbols,
    getLastHistoryDate,
)
from datetime import datetime, date


class CollectCryptoSymbolsTask(BaseTask):
    workers = 1

    def getKey(self):
        today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        return today

    def run(self):
        result = saveCryptoSymbols()
        count = len(result.inserted_ids)
        self.output().write({self.getKey(): count})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "binance_CollectCryptoSymbolsTask",
            [self.getKey()],
            "keys",
        )


class CollectCryptoPriceTask(BaseTask):
    workers = 1

    def getKey(self):
        today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        return today

    def requires(self):
        return CollectCryptoSymbolsTask()

    def run(self):
        today = datetime.today()
        saveCryptoPrice()
        self.output().write({self.getKey(): today})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "binance_CollectCryptoPriceTask",
            [self.getKey()],
            "keys",
        )
