import collect_stock_history_service as service
from datetime import datetime
from functools import wraps
import time
from tasks.collect.fmp.service.collect_stock_symbol_service import getUSAStockSymbols


def timeit(func):
    @wraps(func)
    def timeit_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        result = func(*args, **kwargs)
        end_time = time.perf_counter()
        total_time = end_time - start_time
        # first item in the args, ie `args[0]` is `self` {args} {kwargs}
        if total_time > 0.1:
            print(f"Function {func.__name__} Took {total_time:.4f} seconds")
        return result

    return timeit_wrapper


def test_saveTradableStockHistoryAll():
    result = service.saveTradableStockHistoryAll("2021-01-01", "2021-01-05")
    print(result)


def test_saveTradableStockHistory():
    result = service.saveTradableStockHistory("AAPL", "2021-01-01", "2021-01-05")
    print(result)


def test_batchSaveTradableStockHistory():
    service.batchSaveTradableStockHistory("2021-01-01", "2021-01-05")


def test_getLastHistoryDate():
    date = service.getLastHistoryDate()
    print(date)


def test_deleteByDate():
    service.deleteHistory(datetime(2024, 2, 29))


@timeit
def test_saveStockHistoryAsCsv():
    symbolList = getUSAStockSymbols()
    for obj in symbolList:
        result = service.saveUSAStockHistoryAsCsv(obj["symbol"])
        print(result)


@timeit
def test_saveBatchEODPrice():
    path = service.saveCsvBatchEODPrice(datetime(1988, 8, 4))
    print(path)


def test_saveEODPriceOfCSV():
    service.saveEODPriceOfCSV()
