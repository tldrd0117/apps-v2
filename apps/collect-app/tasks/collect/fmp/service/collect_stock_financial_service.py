import api.fmp.apis as apis
import tasks.collect.fmp.service.collect_stock_symbol_service as symbilService
import time
from dataSource.mongoClient import getCollection

BATCH_SIZE = 500


def saveTradableIncomeStatementsAnnual():
    tradableStockCodes = symbilService.getUSATradableStockSymbols()
    collection = getCollection("fmp_tradable_income_statements_annual")
    collection.drop()
    for stockCode in tradableStockCodes:
        dataList = apis.getIncomeStatements(stockCode["symbol"])
        if isinstance(dataList, list) and len(dataList) > 0:
            collection.insert_many(dataList)


def saveTradableIncomeStatementsQuater():
    tradableStockCodes = symbilService.getUSATradableStockSymbols()
    collection = getCollection("fmp_tradable_income_statements_quarter")
    collection.drop()
    for stockCode in tradableStockCodes:
        dataList = apis.getIncomeStatements(stockCode["symbol"], "quarter")
        if isinstance(dataList, list) and len(dataList) > 0:
            collection.insert_many(dataList)


def saveTradableBalanceSheetsAnnual():
    tradableStockCodes = symbilService.getUSATradableStockSymbols()
    collection = getCollection("fmp_tradable_balance_sheets_annual")
    for stockCode in tradableStockCodes:
        dataList = apis.getBalanceSheetsStatements(stockCode["symbol"])
        if isinstance(dataList, list) and len(dataList) > 0:
            collection.insert_many(dataList)


def saveTradableBalanceSheetsQuater():
    tradableStockCodes = symbilService.getUSATradableStockSymbols()
    collection = getCollection("fmp_tradable_balance_sheets_quarter")
    collection.drop()
    for stockCode in tradableStockCodes:
        dataList = apis.getBalanceSheetsStatements(stockCode["symbol"], "quarter")
        if isinstance(dataList, list) and len(dataList) > 0:
            collection.insert_many(dataList)


def saveTradableCashFlowStatementsAnnual():
    tradableStockCodes = symbilService.getUSATradableStockSymbols()
    collection = getCollection("fmp_tradable_cash_flow_statements_annual")
    for stockCode in tradableStockCodes:
        dataList = apis.getCashFlowStatements(stockCode["symbol"])
        if isinstance(dataList, list) and len(dataList) > 0:
            collection.insert_many(dataList)


def saveTradableCashFlowStatementsQuater():
    tradableStockCodes = symbilService.getUSATradableStockSymbols()
    collection = getCollection("fmp_tradable_cash_flow_statements_quarter")
    collection.drop()
    for stockCode in tradableStockCodes:
        dataList = apis.getCashFlowStatements(stockCode["symbol"], "quarter")
        if isinstance(dataList, list) and len(dataList) > 0:
            collection.insert_many(dataList)


def saveIncomeStatementsAnnual():
    usaStockCodes = symbilService.getUSAStockSymbols()
    collection = getCollection("fmp_income_statements_annual")
    collection.drop()
    dataLists = []
    for stockCode in usaStockCodes:
        dataList = apis.getIncomeStatements(stockCode["symbol"])
        if isinstance(dataList, list) and len(dataList) > 0:
            dataLists.extend(dataList)
            if len(dataLists) > BATCH_SIZE:
                collection.insert_many(dataLists)
                dataLists = []
    if len(dataLists) > 0:
        collection.insert_many(dataLists)


def convertValueToString(dataList):
    return list(
        map(
            lambda dic: {key: str(value) for key, value in dic.items()},
            dataList,
        )
    )


def saveIncomeStatementsQuater():
    usaStockCodes = symbilService.getUSAStockSymbols()
    collection = getCollection("fmp_income_statements_quarter")
    collection.drop()
    dataLists = []
    for stockCode in usaStockCodes:
        dataList = apis.getIncomeStatements(stockCode["symbol"], "quarter")
        if isinstance(dataList, list) and len(dataList) > 0:
            dataList = convertValueToString(dataList)
            dataLists.extend(dataList)
            if len(dataLists) > BATCH_SIZE:
                collection.insert_many(dataLists)
                dataLists = []
    if len(dataLists) > 0:
        collection.insert_many(dataLists)


def saveBalanceSheetsAnnual():
    usaStockCodes = symbilService.getUSAStockSymbols()
    collection = getCollection("fmp_balance_sheets_annual")
    collection.drop()
    dataLists = []
    for stockCode in usaStockCodes:
        dataList = apis.getBalanceSheetsStatements(stockCode["symbol"])
        if isinstance(dataList, list) and len(dataList) > 0:
            dataList = convertValueToString(dataList)
            dataLists.extend(dataList)
            if len(dataLists) > BATCH_SIZE:
                collection.insert_many(dataLists)
                dataLists = []
    if len(dataLists) > 0:
        collection.insert_many(dataLists)


def saveBalanceSheetsQuater():
    usaStockCodes = symbilService.getUSAStockSymbols()
    collection = getCollection("fmp_balance_sheets_quarter")
    collection.drop()
    dataLists = []
    for stockCode in usaStockCodes:
        dataList = apis.getBalanceSheetsStatements(stockCode["symbol"], "quarter")
        if isinstance(dataList, list) and len(dataList) > 0:
            dataList = convertValueToString(dataList)
            dataLists.extend(dataList)
            if len(dataLists) > BATCH_SIZE:
                collection.insert_many(dataLists)
                dataLists = []
    if len(dataLists) > 0:
        collection.insert_many(dataLists)


def saveCashFlowStatementsAnnual():
    usaStockCodes = symbilService.getUSAStockSymbols()
    collection = getCollection("fmp_cash_flow_statements_annual")
    collection.drop()
    dataLists = []
    for stockCode in usaStockCodes:
        dataList = apis.getCashFlowStatements(stockCode["symbol"])
        if isinstance(dataList, list) and len(dataList) > 0:
            dataList = convertValueToString(dataList)
            dataLists.extend(dataList)
            if len(dataLists) > BATCH_SIZE:
                collection.insert_many(dataLists)
                dataLists = []
    if len(dataLists) > 0:
        collection.insert_many(dataLists)


def saveCashFlowStatementsQuater():
    usaStockCodes = symbilService.getUSAStockSymbols()
    collection = getCollection("fmp_cash_flow_statements_quarter")
    collection.drop()
    dataLists = []
    for stockCode in usaStockCodes:
        dataList = apis.getCashFlowStatements(stockCode["symbol"], "quarter")
        if isinstance(dataList, list) and len(dataList) > 0:
            dataList = convertValueToString(dataList)
            dataLists.extend(dataList)
            if len(dataLists) > BATCH_SIZE:
                collection.insert_many(dataLists)
                dataLists = []
    if len(dataLists) > 0:
        collection.insert_many(dataLists)
