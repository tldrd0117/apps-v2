import pathlib
import tasks.collect.fmp.service.collect_stock_symbol_service as symbilService
from api.fmp.stock_historical_apis import getDailyChartEOD, batchEODPrice
from dataSource.mongoClient import getCollection
import time
from datetime import datetime, date
from pymongo import UpdateOne
import polars as pl
import os
from tasks.utils.dateUtils.dateUtils import getDateRangeStr, getTodayDatetime
import glob


def saveUSAStockHistory(code, startDate="1900-01-01", endDate="2100-01-01"):
    fmpUSAHistory = getCollection("fmp_usa_stock_history")
    dailyList = getDailyChartEOD(code, startDate, endDate)
    if len(dailyList) == 0:
        return {"date": None, "count": 0, "code": code}
    dailyList = [
        {
            **row,
            "symbol": dailyList["symbol"],
            "date": datetime.strptime(row["date"], "%Y-%m-%d"),
        }
        for row in dailyList["historical"]
    ]
    result = fmpUSAHistory.insert_many(dailyList)
    return {
        "count": len(result.inserted_ids),
        "code": code,
    }


def saveUSAStockHistoryAsCsv(code, startDate="1900-01-01", endDate="2100-01-01"):
    dailyList = getDailyChartEOD(code, startDate, endDate)
    if len(dailyList) == 0:
        return {"date": None, "count": 0, "code": code}
    dailyList = [
        {
            **row,
            "symbol": dailyList["symbol"],
            "date": datetime.strptime(row["date"], "%Y-%m-%d"),
        }
        for row in dailyList["historical"]
    ]

    today = datetime.strftime(datetime.now(), "%Y%m%d")
    os.makedirs(f"resources/collect/{code}/", exist_ok=True)
    pl.DataFrame(dailyList).write_csv(f"resources/collect/{code}/{today}.csv")
    return f"resources/collect/{code}/{today}.csv"


def saveTradableStockHistory(code, startDate="1900-01-01", endDate="2100-01-01"):
    fmpUSATradableHistory = getCollection("fmp_usa_tradable_history")
    dailyList = getDailyChartEOD(code, startDate, endDate)
    if len(dailyList) == 0:
        return {"date": None, "count": 0, "code": code}
    dailyList = [
        {
            **row,
            "symbol": dailyList["symbol"],
            "date": datetime.strptime(row["date"], "%Y-%m-%d"),
        }
        for row in dailyList["historical"]
    ]
    result = fmpUSATradableHistory.insert_many(dailyList)
    return {
        "count": len(result.inserted_ids),
        "code": code,
    }


def makeOperations(code, startDate, endDate):
    dailyList = getDailyChartEOD(code, startDate, endDate)
    if len(dailyList) == 0:
        return []
    dailyList = [
        {
            **row,
            "symbol": dailyList["symbol"],
            "date": datetime.strptime(row["date"], "%Y-%m-%d"),
        }
        for row in dailyList["historical"]
    ]
    operations = []
    for dailyData in dailyList:
        operations.append(
            UpdateOne(
                {
                    "symbol": dailyData["symbol"],
                    "date": dailyData["date"],
                },
                {"$set": dailyData},
                upsert=True,
            )
        )
    return operations


def batchSaveTradableStockHistory(startDate="1900-01-01", endDate="2100-01-01"):
    count = 0
    tradableStockCodes = symbilService.getUSATradableStockSymbols()
    fmpUSATradableHistory = getCollection("fmp_usa_tradable_history")
    allOpers = []
    for stockCode in tradableStockCodes:
        opers = makeOperations(stockCode["symbol"], startDate, endDate)
        if len(opers) == 0:
            continue
        allOpers.extend(opers)
        if len(allOpers) > 1000:
            result = fmpUSATradableHistory.bulk_write(allOpers)
            count = count + result.upserted_count
            print("saveTradableStockHistory Complete", str(result.upserted_count))
            allOpers = []
        time.sleep(0.22)

    if len(allOpers) > 0:
        result = fmpUSATradableHistory.bulk_write(allOpers)
        count = count + result.upserted_count
        print("saveTradableStockHistory Complete", str(result.upserted_count))


def saveTradableStockHistoryAll(startDate="1900-01-01", endDate="2100-01-01"):
    tradableStockCodes = symbilService.getUSATradableStockSymbols()
    for stockCode in tradableStockCodes:
        result = saveTradableStockHistory(stockCode["symbol"], startDate, endDate)
        print("saveTradableStockHistory Complete", result)


def saveUSAStockHistoryAll(startDate="1900-01-01", endDate="2100-01-01"):
    tradableStockCodes = symbilService.getUSAStockSymbols()
    for stockCode in tradableStockCodes:
        print("saveUSAStockHistory Start", stockCode)
        result = saveUSAStockHistory(stockCode["symbol"], startDate, endDate)
        print("saveUSAStockHistory Complete", result)


def getLastHistoryDate():
    fmpUSAHistory = getCollection("fmp_usa_stock_history")
    latest_document = fmpUSAHistory.find_one(sort=[("date", -1)])
    if latest_document is not None:
        return latest_document["date"]
    else:
        return None


def deleteHistory(startDate: datetime):
    collection = getCollection("fmp_usa_stock_history")
    collection.delete_many({"date": {"$gt": startDate}})


def saveCsvBatchEODPrice(date: datetime):
    dateStr = datetime.strftime(date, "%Y-%m-%d")
    os.makedirs("resources/collect/fmp", exist_ok=True)
    response = batchEODPrice(dateStr)
    todayStr = datetime.strftime(date, "%Y-%m-%d")
    with open(f"resources/collect/fmp/{todayStr}.csv", "wb") as f:
        f.write(response)
    return f"resources/collect/fmp/{todayStr}.csv"


EOD_TYPES = {
    "_id": pl.Utf8,
    "date": pl.Datetime,
    "open": pl.Float64,
    "high": pl.Float64,
    "low": pl.Float64,
    "close": pl.Float64,
    "adjClose": pl.Float64,
    "volume": pl.Float64,
    "symbol": pl.Utf8,
}


def saveAllEODPriceOfCSV():
    collection = getCollection("fmp_eodprice")
    filePath = f"resources/collect/fmp"
    files = glob.glob(f"{filePath}/*", recursive=True)
    filePaths = [pathlib.Path(file) for file in files]
    for filePath in filePaths:
        try:
            df = pl.read_csv(filePath, dtypes=EOD_TYPES)
            if df.shape[0] > 0:
                collection.insert_many(df.to_dicts())
        except Exception as e:
            print(e)
            continue
