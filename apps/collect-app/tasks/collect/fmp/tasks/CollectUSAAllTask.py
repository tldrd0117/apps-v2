import luigi
from tasks.baseTask import BaseTask, WrapperBaseTask
from tasks.targets.MongoFindTarget import MongoFindTarget
from tasks.utils.dateUtils.dateUtils import getDateRangeStr
from dataSource.mongoClient import getClient, getDB, getDBName
from tasks.collect.fmp.service.collect_stock_symbol_service import (
    saveStockSymbols,
    saveTradableStockSymbols,
    saveETFStockSymbols,
    saveFinancialStatementsStockSymbols,
    saveAvailableIndexesSymbols,
)

from tasks.collect.fmp.service.collect_stock_history_service import (
    saveTradableStockHistoryAll,
    batchSaveTradableStockHistory,
    saveUSAStockHistoryAll,
    getLastHistoryDate,
    saveAllEODPriceOfCSV,
    saveCsvBatchEODPrice,
)

from tasks.collect.fmp.service.collect_stock_financial_service import (
    saveIncomeStatementsAnnual,
    saveIncomeStatementsQuater,
    saveBalanceSheetsAnnual,
    saveBalanceSheetsQuater,
    saveCashFlowStatementsAnnual,
    saveCashFlowStatementsQuater,
)
from datetime import datetime, date
from tasks.utils.dateUtils.dateUtils import getDateRange


class CollectUSAStockSymbolTask(BaseTask):
    workers = 1

    def run(self):
        today = datetime.strftime(datetime.now(), "%Y%m%d")
        count = 0
        result = saveAvailableIndexesSymbols()
        count = count + len(result.inserted_ids)
        result = saveStockSymbols()
        count = count + len(result.inserted_ids)
        result = saveETFStockSymbols()
        count = count + len(result.inserted_ids)
        result = saveFinancialStatementsStockSymbols()
        count = count + len(result.inserted_ids)
        result = saveTradableStockSymbols()
        count = count + len(result.inserted_ids)
        self.output().write({today: count})

    def output(self):
        today = datetime.strftime(datetime.now(), "%Y%m%d")
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_CollectUSAStockSymbolTask",
            [today],
            "lastUpdateDate",
        )


class CollectUSAStockHistoryTask(BaseTask):
    workers = 1
    startDate = luigi.DateParameter(default=date(1900, 1, 1))
    endDate = luigi.DateParameter(default=date(2100, 1, 1))

    def getKey(self):
        today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        sDate = datetime.strftime(self.startDate, "%Y-%m-%d")
        eDate = datetime.strftime(self.endDate, "%Y-%m-%d")
        return today + "_" + sDate + "_" + eDate

    def requires(self):
        return CollectUSAStockSymbolTask()

    def run(self):
        today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        sDate = datetime.strftime(self.startDate, "%Y-%m-%d")
        eDate = datetime.strftime(self.endDate, "%Y-%m-%d")
        saveUSAStockHistoryAll(sDate, eDate)
        self.output().write({self.getKey(): today})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_CollectUSAStockHistoryTask",
            [self.getKey()],
            "keys",
        )


class CollectAllEODPriceTask(BaseTask):
    workers = 5
    startDate = luigi.DateParameter()
    endDate = luigi.DateParameter()

    def getKey(self):
        today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        return today

    def requires(self):
        tasks = []
        formatted = "{date:%Y%m%d}"
        startDate = formatted.format(date=self.startDate)
        endDate = formatted.format(date=self.endDate)
        dateRange = getDateRange(startDate, endDate)
        for dt in dateRange:
            tasks.append(CollectEODPriceTask(date=dt.date()))
        return tasks

    def run(self):
        saveAllEODPriceOfCSV()
        self.output().write({self.getKey(): 1})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_CollectAllEODPriceTask",
            [self.getKey()],
            "keys",
        )


class CollectEODPriceTask(BaseTask):
    workers = 5
    date = luigi.DateParameter()

    def getKey(self):
        formatted = "{date:%Y%m%d}"
        current = formatted.format(date=self.date)
        return current

    def run(self):
        formatted = "{date:%Y%m%d}"
        current = formatted.format(date=self.date)
        saveCsvBatchEODPrice(datetime.strptime(current, "%Y%m%d"))
        self.output().write({self.getKey(): 1})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_CollectEODPriceTask",
            [self.getKey()],
            "keys",
        )


class CollectUSAStockHistoryTaskLatest(BaseTask):
    workers = 1

    def getKey(self):
        today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        return today

    def requires(self):
        return CollectUSAStockSymbolTask()

    def run(self):
        today = datetime.today()
        lastDate = getLastHistoryDate()
        if lastDate < today:
            saveUSAStockHistoryAll(lastDate, today)
        self.output().write({self.getKey(): today})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_CollectUSAStockHistoryTaskLatest",
            [self.getKey()],
            "keys",
        )


class CollectUSAIncomeStatementTask(BaseTask):
    workers = 1
    period = luigi.Parameter(default="annual")

    def getKey(self):
        today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        return today + "_" + self.period

    def requires(self):
        return CollectUSAStockSymbolTask()

    def run(self):
        today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        if self.period == "annual":
            saveIncomeStatementsAnnual()
        else:
            saveIncomeStatementsQuater()
        self.output().write({self.getKey(): self.period})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_CollectUSAIncomeStatementTask",
            [self.getKey()],
            "keys",
        )


class CollectUSABalanceSheetsTask(BaseTask):
    workers = 1
    period = luigi.Parameter(default="annual")

    def getKey(self):
        today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        return today + "_" + self.period

    def requires(self):
        return CollectUSAStockSymbolTask()

    def run(self):
        today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        if self.period == "annual":
            saveBalanceSheetsAnnual()
        else:
            saveBalanceSheetsQuater()
        self.output().write({self.getKey(): self.period})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_CollectUSABalanceSheetsTask",
            [self.getKey()],
            "keys",
        )


class CollectUSACashFlowStatementsTask(BaseTask):
    workers = 1
    period = luigi.Parameter(default="annual")

    def getKey(self):
        today = datetime.strftime(datetime.now(), "%Y-%m-%d")
        return today + "_" + self.period

    def requires(self):
        return CollectUSAStockSymbolTask()

    def run(self):
        if self.period == "annual":
            saveCashFlowStatementsAnnual()
        else:
            saveCashFlowStatementsQuater()
        self.output().write({self.getKey(): self.period})

    def output(self):
        return MongoFindTarget(
            getClient(),
            getDBName(),
            "fmp_CollectUSACashFlowStatementsTask",
            [self.getKey()],
            "keys",
        )


class CollectUSAFinancialTask(WrapperBaseTask):
    workers = 1

    def requires(self):
        return [
            CollectUSAIncomeStatementTask(period="annual"),
            CollectUSAIncomeStatementTask(period="quater"),
            CollectUSABalanceSheetsTask(period="annual"),
            CollectUSABalanceSheetsTask(period="quater"),
            CollectUSACashFlowStatementsTask(period="annual"),
            CollectUSACashFlowStatementsTask(period="quater"),
        ]


class CollectForexTask(BaseTask):
    def run(self):
        pass
