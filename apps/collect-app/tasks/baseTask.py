import luigi


class BaseTask(luigi.Task):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        import tasks.slack.luigi.luigiEventHandle


class WrapperBaseTask(luigi.WrapperTask):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        import tasks.slack.luigi.luigiEventHandle
