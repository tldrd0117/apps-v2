import asyncio
from luigiTestTask import TestTask, TestProgressTask, TestParamTask
import luigi
from tasks.slack.app_bolt import connectBolt
import os


def test_testTask():
    # startBolt()
    task = TestTask()
    luigi.build([task], workers=1, detailed_summary=True)


def test_testParamTask():
    asyncio.run(connectBolt())
    task = TestParamTask(param1="test", param2="test2")
    luigi.build([task], workers=1, detailed_summary=True)


def test_testProgressTask():
    task = TestProgressTask()
    luigi.build([task], workers=1, detailed_summary=True)


# def test_slack():
#     slacker = SlackBot(token='xoxb-6144925539923-6130639077495-sbOExbwGShy9o6Rl7pDvjD7U',
#                    channels=['luigi-task'])
#     with notify(slacker):
#         luigi.build([TestTask()], workers=1, detailed_summary=True)
