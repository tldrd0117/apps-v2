import api.fmp.stock_list_apis as apis


def test_getSymbolList():
    json = apis.getSymbolList()
    assert len(json) > 0
    print(json)


def test_getETFSymbolList():
    json = apis.getETFSymbolList()
    assert len(json) > 0
    print(json)


def test_getFinancialStatementsSymbolList():
    json = apis.getFinancialStatementsSymbolList()
    assert len(json) > 0
    print(json)


def test_getTradableSymbolList():
    json = apis.getTradableSymbolList()
    assert len(json) > 0
    print(json)


def test_getAvailableIndexesSymolList():
    json = apis.getAvailableIndexesSymbolList()
    assert len(json) > 0
    print(json)
