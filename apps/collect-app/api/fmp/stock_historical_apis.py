from api.fmp.common import request, download


# 1min, 5min, 15min, 30min, 1hour, 4hour
def getIntradayChart(
    symbol: str,
    timeframe: str = "5min",
    startDate: str = "2021-01-01",
    endDate: str = "2021-01-10",
):
    params = {
        "from": startDate,
        "to": endDate,
    }
    json = request(
        f"https://financialmodelingprep.com/api/v3/historical-chart/{timeframe}/{symbol}",
        params=params,
    )
    return json


def getDailyChartEOD(
    symbol: str, startDate: str = "2021-01-01", endDate: str = "2021-01-10"
):
    params = {
        "from": startDate,
        "to": endDate,
    }
    json = request(
        f"https://financialmodelingprep.com/api/v3/historical-price-full/{symbol}",
        params=params,
    )
    return json


def batchEODPrice(date: str = "2021-01-04"):
    params = {
        "date": date,
    }
    contents = download(
        f"https://financialmodelingprep.com/api/v4/batch-request-end-of-day-prices",
        params=params,
    )
    return contents
