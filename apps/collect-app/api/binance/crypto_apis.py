from api.binance.common import request


def getExchangeInfo():
    json = request(f"https://api.binance.com/api/v3/exchangeInfo")
    return json


def getCryptoPrice(symbol="BTCUSDT", interval="1d", limit=1000, startTime=0):
    json = request(
        f"https://api.binance.com/api/v3/klines?symbol={symbol}&interval={interval}&limit={limit}&startTime={startTime}"
    )
    return json
