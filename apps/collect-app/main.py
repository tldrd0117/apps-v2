import asyncio
from typing import Union

from fastapi import FastAPI, Request, WebSocket, HTTPException
from tasks.taskExecutor import run
from tasks.slack.app_bolt import connectBolt
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse
import glob
import pathlib
import tasks.Constants as Constants

connectBolt()
app = FastAPI()
origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/resources/csv", StaticFiles(directory="resources/csv"), name="csv")
app.mount(
    "/resources/portfolios",
    StaticFiles(directory="resources/portfolios"),
    name="portfolios",
)


@app.get("/portfolios/list")
def getPortfoliosList():
    filePath = f"resources/portfolios"
    files = glob.glob(f"{filePath}/*", recursive=True)
    filenames = [pathlib.Path(file).name for file in files]
    return filenames


@app.get("/entity/{entityType}/eachsymbol/{symbol}")
def downloadEntity(entityType: str, symbol: str):
    entityType = Constants.EntityType(entityType)
    filePath = f"{Constants.getBaseDir(entityType)}/eachsymbol/history"
    files = glob.glob(f"{filePath}/{symbol}_*")
    filenames = [pathlib.Path(file).name for file in files]
    if len(filenames) > 0:
        return FileResponse(f"{filePath}/{filenames[0]}", filename=filenames[0])
    else:
        raise HTTPException(status_code=400, detail="Item not found")


@app.get("/test")
def test(symbol: str):
    return {"test": symbol}


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}


@app.post("/webhook/{taskName}")
async def runTask(taskName: str, request: Request):
    body = await request.body()
    run(taskName, body.decode("utf-8"))
    return {"result": "success"}


# debugging
# if __name__ == "__main__":
#     asyncio.run(startBolt())

# production
# asyncio.create_task(startBolt())


# @app.post("/slack/events")
# async def endpoints(req: Request):
#     return await getHandler().handle(req)
