# SeibroStockNumEventList


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rows** | [**List[SeibroStockNumEvent]**](SeibroStockNumEvent.md) |  | [optional] 
**total** | **float** |  | [optional] 

## Example

```python
from tasks.models.seibro_stock_num_event_list import SeibroStockNumEventList

# TODO update the JSON string below
json = "{}"
# create an instance of SeibroStockNumEventList from a JSON string
seibro_stock_num_event_list_instance = SeibroStockNumEventList.from_json(json)
# print the JSON string representation of the object
print SeibroStockNumEventList.to_json()

# convert the object into a dict
seibro_stock_num_event_list_dict = seibro_stock_num_event_list_instance.to_dict()
# create an instance of SeibroStockNumEventList from a dict
seibro_stock_num_event_list_form_dict = seibro_stock_num_event_list.from_dict(seibro_stock_num_event_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


