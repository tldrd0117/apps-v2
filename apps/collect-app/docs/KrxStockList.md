# KrxStockList


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rows** | [**List[KrxStock]**](KrxStock.md) |  | [optional] 
**total** | **float** |  | [optional] 

## Example

```python
from tasks.models.krx_stock_list import KrxStockList

# TODO update the JSON string below
json = "{}"
# create an instance of KrxStockList from a JSON string
krx_stock_list_instance = KrxStockList.from_json(json)
# print the JSON string representation of the object
print KrxStockList.to_json()

# convert the object into a dict
krx_stock_list_dict = krx_stock_list_instance.to_dict()
# create an instance of KrxStockList from a dict
krx_stock_list_form_dict = krx_stock_list.from_dict(krx_stock_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


