from pymongo import UpdateOne
from stock.datasources.mongoClient import getCollection
from datetime import datetime


def run():
    collection = getCollection("fmp_batch_eod_prices")
    cursor = collection.find({}, {"symbol": 1}).distinct("symbol")
    symbols = list(cursor)
    totalCount = len(symbols)
    index = 0
    for symbol in symbols:
        print(index, "/", totalCount, symbol)
        bulkUpdate = []
        cursor = collection.find({"symbol": symbol})
        items = list(cursor)
        for item in items:
            orgDate = item.get("date")
            if isinstance(orgDate, str):
                try:
                    new_date = datetime.strptime(
                        orgDate, "%Y-%m-%d"
                    )  # 날짜 형식에 맞게 변경
                except ValueError:
                    # 날짜 형식이 맞지 않는 경우 건너뜀
                    continue
            else:
                continue  # 이미 datetime 형식일 경우 변경하지 않음
            updateDoc = {
                "filter": {"_id": item["_id"]},
                "update": {"$set": {"date": new_date}},
            }
            bulkUpdate.append(updateDoc)
        if len(bulkUpdate) > 0:
            collection.bulk_write(
                [UpdateOne(update["filter"], update["update"]) for update in bulkUpdate]
            )
        index += 1
