'use client'
import Link from 'next/link'
import { Chart, CsvTable, LinkButton } from 'ui'

const Portfolios = ({ portfolio }) => {
  const path = `http://localhost:8083/resources/portfolios/${portfolio}/chart/history.csv`
  const orderPath = `http://localhost:8083/resources/portfolios/${portfolio}/chart/order.csv`
  const drawdownPath = `http://localhost:8083/resources/portfolios/${portfolio}/chart/drawdown.csv`
  return (
    <div className="p-8">
      <Link href={`/entity/list`}>
        <LinkButton>List</LinkButton>
      </Link>
      <h3 className="mt-4">{`${portfolio} Chart`}</h3>
      <Chart data={path} />
      <h3 className="mt-4">{`${portfolio} Drawdown`}</h3>
      <Chart data={drawdownPath} logScale={false} />
      <h3 className="mt-4">{`${portfolio} History`}</h3>
      <CsvTable data={path} />
      <h3 className="mt-4">{`${portfolio} Order`}</h3>
      <CsvTable data={orderPath} />
    </div>
  )
}

export default Portfolios
