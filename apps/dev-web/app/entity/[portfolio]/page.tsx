import React from 'react'
import Portfolios from './Portfolios'

const Page = ({
  params,
}: {
  params: { portfolio: string; chart: string }
}): React.JSX.Element => <Portfolios portfolio={params.portfolio} />

export default Page
