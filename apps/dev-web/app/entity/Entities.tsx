'use client'
import { Chart } from 'ui'

const Entities = () => {
  const aggressiveAssetMomentumScore =
    'http://localhost:8083/resources/csv/AggressiveAssetMomentumScore.csv'
  const aggressibeAssetProfit =
    'http://localhost:8083/resources/csv/AggressiveAssetProfit.csv'
  const aggressibeAssetClose =
    'http://localhost:8083/resources/csv/AggressiveAssetClose.csv'
  const defensiveAssetClosePerMovingAvg =
    'http://localhost:8083/resources/csv/DefensiveAssetClosePerMovingAvg.csv'
  const defensiveAssetClose =
    'http://localhost:8083/resources/csv/DefensiveAssetClose.csv'
  const canaryAssetMomentScore =
    'http://localhost:8083/resources/csv/CanaryAssetMomentumScore.csv'
  return (
    <>
      <h3>공격자산의 모멘텀 스코어 차트</h3>
      <Chart data={aggressiveAssetMomentumScore} />
      <h3>공격자산의 1,3,6,12개월 마다 수익률</h3>
      <Chart data={aggressibeAssetProfit} />
      <h3>공격자산의 종가</h3>
      <Chart data={aggressibeAssetClose} />
      <h3>안전자산의 12개월 이동 평균선</h3>
      <Chart data={defensiveAssetClosePerMovingAvg} />
      <h3>안전자산의 종가</h3>
      <Chart data={defensiveAssetClose} />
      <h3>카나리아 자산의 모멘텀 스코어</h3>
      <Chart data={canaryAssetMomentScore} />
    </>
  )
}

export default Entities
