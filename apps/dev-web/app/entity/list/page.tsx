import type { ReactNode } from 'react'
import PortfolioList from './PortfolioList'

const Page = (): ReactNode => <PortfolioList />

export default Page
