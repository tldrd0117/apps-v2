'use client'
import type { ReactNode } from 'react'
import React, { useEffect, useState } from 'react'
import Link from 'next/link'
import { LinkButton } from 'ui'

interface PortfolioListProps {
  children?: ReactNode
}

const PortfolioList: React.FC<PortfolioListProps> = () => {
  const [portfolioList, setPortfolioList] = useState<string[]>([])
  const fetchPortfolioList = async (): Promise<string[]> => {
    const path = `http://localhost:8083/portfolios/list`
    const data = await fetch(path)
    const response: string[] = (await data.json()) as string[]
    return response
  }
  useEffect(() => {
    fetchPortfolioList().then(
      (response) => {
        setPortfolioList(response)
      },
      (error) => {
        throw error
      }
    )
  }, [])
  return (
    <div className="flex flex-wrap mt-4">
      {portfolioList.map((item: string) => (
        <div className="ml-2 mb-2" key={item}>
          <Link href={`/entity/${item}`}>
            <LinkButton>{item}</LinkButton>
          </Link>
        </div>
      ))}
    </div>
  )
}

export default PortfolioList
