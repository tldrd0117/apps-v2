export interface EncData{
    enc: string
}

export interface ErrorBody{
    error: object
}

export interface BasicTypes{
    _id: string
    name: string
    uid: string
}

export interface SelectList{
    id: string
    value: string
}