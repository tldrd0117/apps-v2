import { KeyLike } from "jose";
import { TokenPayload } from "./interfaces/auth";
import { encrypt } from "./utils/common";
import { BASE_URL } from "./utils/env";

export const getTypes = async () => {
    const response = await fetch(`${BASE_URL}/user/types`);
    return await response.json();
};
