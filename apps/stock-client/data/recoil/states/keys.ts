const ATOM_KEYS = {
    TOKEN: "token",
    USER_INFO: "userInfo",
    SETTING_MAP: "settingList",
    SETTING_TYPES: "settingTypes",
    ROLE_TYPES: "roleTypes",
    SETTING_TYPE_SELECT_LIST: "settingTypeSelectList",
    ROLE_TYPE_SELECT_LIST: "roleTypeSelectList",
    NETWORK_ERROR_HANDLE: "networkErrorHandle"
}

export default ATOM_KEYS