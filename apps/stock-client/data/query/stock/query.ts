import { getDefaultHeader, makeQueryString } from "@/data/api/utils/common"
import { useMutation, useQuery } from "@tanstack/react-query"

export const useKrxStock = (market: string, code: string) => {
    return useQuery({
        queryKey: ["krxStock", market, code],
        queryFn: async () => {
            const queryString = makeQueryString({
                "market": market,
                "종목코드": code,
                "limit": 10000,
                "offset": 0,
                "startDate": "20180101",
                "endDate": "20181231",
            })
            const response = await fetch(`http://localhost:3001/stock/krxMarcap/range?${queryString}`)
            return await response.json()
        }
    })
}

export const useKrxStockMutation = () => {
    return useMutation({
        mutationFn: async ({ market, code }: any) => {
            const queryString = makeQueryString({
                "market": market,
                "종목코드": code,
                "limit": 10000,
                "offset": 0,
                "startDate": "20180101",
                "endDate": "20181231",
            })
            const response = await fetch(`http://localhost:3001/stock/krxMarcap/range?${queryString}`)
            return await response.json()
        }
    })
}