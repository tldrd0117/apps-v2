import React, { ChangeEventHandler, KeyboardEventHandler, Ref } from 'react'

export interface InputProps {
  name?: string
  type?: string
  value?: string
  placeholder?: string
  disabled?: boolean
  readOnly?: boolean
  className?: string
  children?: React.ReactNode
  onChange?: ChangeEventHandler<HTMLInputElement>
  onKeyDown?: KeyboardEventHandler<HTMLInputElement>
  onKeyUp?: KeyboardEventHandler<HTMLInputElement>
  onFocus?: React.FocusEventHandler<HTMLInputElement>
  onBlur?: React.FocusEventHandler<HTMLInputElement>
  onClick?: React.MouseEventHandler<HTMLInputElement>
}

const Input = React.forwardRef<HTMLInputElement, InputProps>(
  (props: InputProps, ref) => {
    const {
      name,
      type,
      value,
      readOnly,
      className,
      children,
      placeholder,
      disabled,
      onChange,
      onKeyDown,
      onKeyUp,
      onFocus,
      onBlur,
      onClick,
    } = props
    return (
      <input
        name={name}
        type={type}
        ref={ref}
        readOnly={readOnly}
        className={className}
        placeholder={placeholder}
        value={value}
        disabled={disabled}
        onChange={onChange}
        onKeyDown={onKeyDown}
        onKeyUp={onKeyUp}
        onFocus={onFocus}
        onBlur={onBlur}
        onClick={onClick}
      />
    )
  }
)

export default Input
