'use client'
import React, { useEffect, useRef } from 'react'
import * as d3 from 'd3'
import { addDays, format } from 'date-fns'

interface D3CandleChartProps {
  ticker: Array<{
    Date: Date
    Open: number
    High: number
    Low: number
    Close: number
    AdjClose: number
    Volume: number
  }>
}

export default function D3CandleChart(props: D3CandleChartProps) {
  const ref = useRef(null)
  const { ticker } = props
  useEffect(() => {
    if (ref.current && (ref.current as any).innerHTML === '') {
      ;(ref.current as any).appendChild(createChart())
    }
  }, [ticker])
  if (ticker.length < 2) return <></>
  const createChart = () => {
    const width = 928
    const height = 600
    const marginTop = 20
    const marginRight = 30
    const marginBottom = 30
    const marginLeft = 80
    // Declare the positional encodings.
    const x = d3
      .scaleLinear()
      .domain([0, ticker.length])
      .range([marginLeft, width - marginRight])
    const y = d3
      .scaleLog()
      .domain([
        d3.min(ticker, (d: any) => d.Low),
        d3.max(ticker, (d: any) => d.High),
      ])
      .rangeRound([height - marginBottom, marginTop])

    // Create the SVG container.
    const svg = d3
      .create('svg')
      .attr('viewBox', [0, 0, width, height])
      .attr(
        'style',
        'max-width: 100%; height: auto; height: intrinsic; font: 10px sans-serif;'
      )
      .style('-webkit-tap-highlight-color', 'transparent')
      .style('overflow', 'visible')
      .on('pointerenter pointermove', pointermoved)
      .on('pointerleave', pointerleft)
      .on('touchstart', (event) => event.preventDefault())

    svg
      .append('clipPath')
      .attr('id', 'clip')
      .append('rect')
      .attr('x', marginLeft)
      .attr('width', width - marginLeft)
      .attr('height', height - marginBottom)
    let tooltip: any
    function pointermoved(event: any) {
      function size(text: any, path: any) {
        const { x, y, width: w, height: h } = text.node().getBBox()
        text.attr('transform', `translate(${-w / 2},${15 - y})`)
        path.attr(
          'd',
          `M${-w / 2 - 10},5H-5l5,-5l5,5H${w / 2 + 10}v${h + 20}h-${w + 20}z`
        )
      }
      // const i = bisect(ticker, x.invert(d3.pointer(event)[0]));
      const i = Math.round(x.invert(d3.pointer(event)[0]))
      if (ticker[i] === undefined) return
      tooltip.style('display', null)
      tooltip.attr('transform', `translate(${x(i)},${y(ticker[i].Low)})`)

      const path = tooltip
        .selectAll('path')
        .data([,])
        .join('path')
        .attr('fill', 'white')
        .attr('stroke', 'black')

      const date = format(new Date(ticker[i].Date), 'yyyy-MM-dd')
      const close = '종가: ' + ticker[i].Close
      const open = '시가: ' + ticker[i].Open
      const high = '고가: ' + ticker[i].High
      const low = '저가: ' + ticker[i].Low
      const text = tooltip
        .selectAll('text')
        .data([,])
        .join('text')
        .call((text: any) =>
          text
            .selectAll('tspan')
            .data([date, close, open, high, low])
            .join('tspan')
            .attr('x', 0)
            .attr('y', (_: any, i: number) => `${i * 1.1}em`)
            .attr('font-weight', (_: any, i: any) => (i ? null : 'bold'))
            .text((d: any) => d)
        )
      size(text, path)
    }

    function pointerleft() {
      tooltip.style('display', 'none')
    }

    // Wraps the text with a callout path of the correct size, as measured in the page.

    // const view = svg.append("rect")
    //     .attr("class", "view")
    //     .attr("x", 0.5)
    //     .attr("y", 0.5)
    //     .attr("width", width - 1)
    //     .attr("height", height - 1);

    // Append the axes.
    const xAxis = d3
      .axisBottom(x)
      // const xAxis = d3.axisBottom<Date>(x)
      // .tickValues(ticker.map((d:any) => d.Date.toString()))
      .tickFormat((domainValue: d3.NumberValue, index: number): string => {
        const v = ticker[domainValue.valueOf()]
        if (v === undefined) {
          const lastDate = ticker.at(-1)?.Date
          if (lastDate) {
            return format(addDays(lastDate, 1), 'yyyy-MM-dd')
          } else {
            return ''
          }
        } else {
          return format(new Date(v.Date), 'yyyy-MM-dd')
        }
      })
    const yAxis = d3
      .axisLeft(y)
      .tickFormat(d3.format('~f'))
      .tickValues(d3.scaleLinear().domain(y.domain()).ticks())
    const gX = svg
      .append('g')
      .attr('class', 'x-axis')
      .attr('transform', `translate(0,${height - marginBottom})scale(1,1)`)
      .call(xAxis)
      .call((g) => g.select('.domain').remove())

    const gY = svg
      .append('g')
      .attr('class', 'y-axis')
      .attr('transform', `translate(${marginLeft},0)scale(1,1)`)
      .call(yAxis)
      .call((g) =>
        g
          .selectAll('.tick line')
          .clone()
          .attr('stroke-opacity', 0.2)
          .attr('x2', width - marginLeft - marginRight)
      )
      .call((g) => g.select('.domain').remove())

    // Create a group for each day of data, and append two lines to it.
    const g = svg
      .append('g')
      .attr('stroke-linecap', 'round')
      .attr('stroke', 'black')
      .attr('style', 'clip-path: url(#clip);')
      .selectAll('g')
      .data(ticker)
      .join('g')
      .attr('transform', (d, i) => `translate(${x(i)},0)scale(1,1)`)

    g.append('line')
      .attr('class', 'ticker-low-high')
      .attr('y1', (d) => y(d.Low))
      .attr('y2', (d) => y(d.High))
      .attr('stroke', (d) =>
        d.Open > d.Close
          ? d3.schemeSet1[0]
          : d.Close > d.Open
          ? d3.schemeSet1[2]
          : d3.schemeSet1[8]
      )
    g.append('line')
      .attr('class', 'ticker-open-close')
      .attr('y1', (d) => y(d.Open))
      .attr('y2', (d) => y(d.Close))
      .attr('stroke-width', 3) //x.bandwidth())
      .attr('stroke', (d) =>
        d.Open > d.Close
          ? d3.schemeSet1[0]
          : d.Close > d.Open
          ? d3.schemeSet1[2]
          : d3.schemeSet1[8]
      )

    // Append a title (tooltip).
    const formatDate = d3.utcFormat('%B %-d, %Y')
    const formatValue = d3.format('.2f')
    const formatChange = (
      (f) => (y0: any, y1: any) =>
        f((y1 - y0) / y0)
    )(d3.format('+.2%'))

    g.append('title').text(
      (d) => `${formatDate(new Date(d.Date))}
        Open: ${formatValue(d.Open)}
        Close: ${formatValue(d.Close)} (${formatChange(d.Open, d.Close)})
        Low: ${formatValue(d.Low)}
        High: ${formatValue(d.High)}`
    )

    const extent: [[number, number], [number, number]] = [
      [marginLeft, marginTop],
      [width - marginRight, height - marginTop],
    ]
    const zoom = d3
      .zoom()
      .scaleExtent([1, 10])
      .translateExtent(extent)
      .extent(extent)
      // .filter(filter)
      .on('zoom', zoomed)
    tooltip = svg.append('g').attr('class', 'tooltip')

    return Object.assign(svg.call(zoom as any).node()!, { reset })

    function zoomed(event: any) {
      x.range(
        [marginLeft, width - marginRight].map((d) => event.transform.applyX(d))
      )
      const start = x.invert(marginLeft)
      const end = x.invert(width - marginRight)
      const target = ticker.slice(start, end)
      y.domain([
        d3.min(target, (d: any) => d.Low),
        d3.max(target, (d: any) => d.High),
      ]).rangeRound([height - marginBottom, marginTop])

      svg.selectAll<SVGSVGElement, any>('.x-axis').call(xAxis)

      g.attr(
        'transform',
        (d, i) => `translate(${x(i)},0)scale(${event.transform.k},1)`
      )
    }

    function reset() {
      svg
        .transition()
        .duration(750)
        .call(zoom.transform as any, d3.zoomIdentity)
    }

    // prevent scrolling then apply the default filter
    function filter(event: any) {
      event.preventDefault()
      return (!event.ctrlKey || event.type === 'wheel') && !event.button
    }
  }

  return <>{<div ref={ref}></div>}</>
}
