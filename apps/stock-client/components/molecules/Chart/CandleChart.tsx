'use client'
import React from 'react';
import ReactApexChart from 'react-apexcharts'
import { format, parse, parseISO } from 'date-fns'
import { zonedTimeToUtc } from 'date-fns-tz';

interface CandleChartProps{
    data: Array<{ x: string, y: Array<number>}>
}

export default  function CandleChart(props: CandleChartProps){
    //open high low close
    const [chartData, setChartData] = React.useState<any>({
        series: [{
          name: 'candle',
          data: [...props?.data]
        }],
        options: {
          chart: {
            height: 350,
            type: 'candlestick',
          },
          title: {
            text: 'CandleStick Chart - Category X-axis',
            align: 'left'
          },
          plotOptions: {
            candlestick: {
              colors: {
                upward: '#DF7D46',
                downward: '#3C90EB'
              }
            }
          },
          annotations: {
            xaxis: [
              {
                x: '2021-11-11',
                borderColor: '#00E396',
                label: {
                  borderColor: '#00E396',
                  style: {
                    fontSize: '12px',
                    color: '#fff',
                    background: '#00E396'
                  },
                  orientation: 'horizontal',
                  offsetY: 7,
                  text: 'Annotation Test'
                }
              }
            ]
          },
          tooltip: {
            enabled: true,
          },
          xaxis: {
            type: 'category',
            labels: {
              formatter: function(val: string) {
                if(!val) return ""
                // console.log(val)
                let da = parseISO(new Date(val).toISOString())
                let date = format(da, 'yyyy-MM-dd')
                // console.log(date)
                return date
                // return dayjs(val).format('MMM DD HH:mm')
              },
                datetimeUTC: true,
            }
          },
          yaxis: {
            tooltip: {
              enabled: true
            }
          }
        },
        })
    React.useEffect(() => {
    
    }, [])
    return <>
        <ReactApexChart options={chartData.options} series={chartData.series} type="candlestick" height={500} />
    </>
}

// class CandleChart extends React.Component {
//     constructor(props: any) {
//       super(props);

//       let data: any = ;
//         this.state = data;
//     }

  

//     render() {
//       return (
//         <div id="chart">
//         </div>
//       );
//     }
//   }

//   export default CandleChart;
