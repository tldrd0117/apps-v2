'use client'
import React from 'react'
import dynamic from 'next/dynamic'
import { useKrxStock } from '@/data/query/stock/query'
const D3CandleChart = dynamic(
  () => import('@/components/molecules/Chart/D3CandleChart'),
  {
    ssr: false,
  }
)
export default function Stock() {
  const { data } = useKrxStock('kospi', '005930')
  console.log(data)
  return (
    <>
      <h1>Stock</h1>
      <D3CandleChart
        ticker={data?.list
          ?.filter((obj: any) => obj['시가'] > 0)
          .map((obj: any) => ({
            Date: new Date(obj.date),
            Open: obj['시가'],
            High: obj['고가'],
            Low: obj['저가'],
            Close: obj['종가'],
            AdjClose: obj['종가'],
            Volume: obj['거래량'],
          }))}
      />
    </>
  )
}
