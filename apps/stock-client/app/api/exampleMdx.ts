// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import fs from 'fs'
import path from 'path'
// import source from '!raw-loader!../public/example.mdx'

type Data = string

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
    const source = fs.readFileSync(path.join(__dirname, "..", "..", "public", "example.mdx"))
    res.status(200).send(source.toString())
}
