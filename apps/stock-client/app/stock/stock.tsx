'use client'
import React, { useEffect } from 'react';
import dynamic from 'next/dynamic';
import { useKrxStock, useKrxStockMutation } from '@/data/query/stock/query';
import { Select } from '@/components/molecules/Select/Select';
import { INPUT_STYLE_TYPE } from '@/components/molecules/Input/StylableInput';
import { TextInput } from '@/components/molecules/Input/TextInput';
import { GrayButton } from '@/components/atoms/Button/GrayButton';
import { useQueryClient } from '@tanstack/react-query';
const CandleChart = dynamic(() => import('@/components/molecules/Chart/CandleChart'), {
    ssr: false,
  })
const D3CandleChart = dynamic(() => import('@/components/molecules/Chart/D3CandleChart'), {
ssr: false,
})
export default function Stock(){
    const [market, setMarket] = React.useState("kospi")
    const [code, setCode] = React.useState("005930")
    const {data} = useKrxStock(market, code)
    const queryClient = useQueryClient()
    const types = [{
        id: "kospi",
        value: "KOSPI"
    }, {
        id: "kosdaq",
        value: "KOSDAQ"
    }]
    useEffect(() => {
        console.log(data)
        queryClient.invalidateQueries(["krxStock"])
    }, [data, queryClient])
    const handleCodeChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setCode(e.target.value)
    }
    const handleMarketChange = (itemData: any, e: React.MouseEvent<HTMLLIElement, MouseEvent>) => {
        setMarket(itemData.id)
    }
    return <>
        <h1>Stock</h1>
        <Select inputProps={{
                bgClassName: "mt-4 w-40",
                placeholder: "시장",
                inputStyleType: INPUT_STYLE_TYPE.UNDERLINE,
            }} contextMenuProps={{
                className: "mt-2 cardBg",
                firstListItemProps: {
                    className: "rounded-t-lg",
                },
                lastListItemProps: {
                    className: "rounded-b-lg",
                },
                listProps: {
                    className: "w-40",
                },
                listItemProps: {
                    className: "w-40",
                },
                listItemsData: types,
            }}
            onItemSelect={handleMarketChange}
            selected={types.find((item) => item.id === market)}
            />
        <TextInput value={code} onChange={handleCodeChange} inputStyleType={INPUT_STYLE_TYPE.UNDERLINE}/>
        <D3CandleChart ticker={data?.list?.filter((obj:any) => obj["시가"]>0).map((obj:any) => ({
            Date: new Date(obj.date),
            Open: obj["시가"],
            High: obj["고가"],
            Low: obj["저가"],
            Close: obj["종가"],
            AdjClose: obj["종가"],
            Volume: obj["거래량"]
        })) || []}/>
        {/* <CandleChart data={data?.list?.filter((obj:any) => obj["종가"]>0).map((obj:any) => ({
            x: new Date(obj.date),
            y: [obj["시가"], obj["고가"], obj["저가"], obj["종가"]]
        }))}/>  */}
    </>
}
