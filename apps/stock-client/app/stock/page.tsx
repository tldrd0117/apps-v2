import React from 'react'
import { dehydrate, Hydrate } from '@tanstack/react-query'
import getQueryClient from '../getQueryClient'
import { prefetchPublicKey } from '@/data/query/auth/prefetch'
import { AppBarContentsTemplate } from '@/components/templates/AppBarContentsTemplate'
import { VisitRecord } from '@/app/common/VisitRecord'
import Stock from './stock'

export default async function Page({ params }: { params: { id: string } }) {
  const id = (params?.id as string) || ''
  await prefetchPublicKey()
  const state = dehydrate(getQueryClient())
  return (
    <>
      <Hydrate state={state}>
        <AppBarContentsTemplate>
          <VisitRecord>
            <Stock />
          </VisitRecord>
        </AppBarContentsTemplate>
      </Hydrate>
    </>
  )
}
