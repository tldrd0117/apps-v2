'use client'

import { redirect, usePathname, useRouter } from 'next/navigation'
import React, { ReactNode } from 'react'
import { useRecoilState, useRecoilValue, useRecoilValueLoadable } from 'recoil'
import { isLoginSelector } from '@/data/recoil/selectors/user'

const LoginRequired = ({ children }: { children: ReactNode }) => {
  // const {data: loginState, isLoading} = useLoginState()
  const pathname = usePathname()
  const isLogin = useRecoilValue(isLoginSelector)
  if (!isLogin) {
    redirect(`/user/login?redirect=${pathname}`)
  }
  return <div>{isLogin ? children : null}</div>
}
export default LoginRequired
