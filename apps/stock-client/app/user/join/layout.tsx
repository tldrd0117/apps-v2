import { ContentsLayout } from '@/components/molecules/Layout/ContentsLayout'
import { PageLayout } from '@/components/molecules/Layout/PageLayout'
import { ReactNode } from 'react' // Add this import

export default function Layout({ children }: { children: ReactNode }) {
  // Change the type to ReactNode
  return (
    <PageLayout className="flex justify-center items-center">
      <ContentsLayout className="w-96 borderBg">{children}</ContentsLayout>
    </PageLayout>
  )
}
