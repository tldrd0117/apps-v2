import React from "react";
import Setting from "./setting";
import LoginRequired from "@/app/common/LoginRequired";
import { AppBarContentsTemplate } from "@/components/templates/AppBarContentsTemplate";

export default async function SettingPage() {
    return <>
        <Setting/>
    </>
}