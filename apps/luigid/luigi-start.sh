eval "$(pyenv init -)"
eval "pyenv activate collect"
eval "date +"%d-%m-%y""
proc=$(pgrep 'luigid')
current=$(date +"%d_%m_%y")

log_path="luigi/log/$current"

if [ ! -d "$log_path" ]; then
  mkdir -p "$log_path"
fi

. '../../utils/process.sh'
result=$(killProcessByPort $LUIGI_PORT)
echo $result
if [ "$result" = "Not found" ]; then
    echo "it is not found"
else
    echo "it is exist"
fi

luigid --pidfile luigi/pid --logdir $log_path --port $LUIGI_PORT
