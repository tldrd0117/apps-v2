#
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
from __future__ import annotations

import os
from datetime import datetime

from airflow import models
from airflow.operators.bash import BashOperator
from airflow.operators.python import PythonOperator
from airflow.providers.docker.operators.docker import DockerOperator
from docker.types import Mount
from dotenv import dotenv_values, find_dotenv, load_dotenv

ENV_ID = os.environ.get("SYSTEM_TESTS_ENV_ID")
DAG_ID = "runTestRunBackTest"

load_dotenv(find_dotenv())
config = dotenv_values()


def print_root():
    print(config)


with models.DAG(
    DAG_ID,
    schedule=None,
    start_date=datetime(2021, 1, 1),
    catchup=False,
    tags=["task-commander", "simulate", "docker"],
) as dag:
    t1 = PythonOperator(task_id="print_root", python_callable=print_root, dag=dag)
    t2 = DockerOperator(
        docker_url="unix://var/run/docker.sock",  # Set your docker URL
        image="task-commander:latest",
        command="poetry run dev runTestRunBackTest",
        network_mode="bridge",
        task_id="runTestRunBackTest",
        auto_remove="force",
        mounts=[
            Mount(
                source=config["PROJECT_ROOT"] + "/packages/stock-resources",
                target="/usr/src/app/packages/stock-resources",
                type="bind",
            )
        ],
        dag=dag,
    )
    t3 = BashOperator(
        task_id="print_hello", bash_command='echo "hello world!!!"', dag=dag
    )
    # t1 >> t2
    # t1 >> t3
    # t3 >> t4

    (
        # TEST BODY
        t1
        >> t2
        >> t3
    )
