import { Module } from '@nestjs/common';
import { AuthService } from 'auth/auth.service';
import { PostController } from './post.controller';
import { PostService } from './post.service';
import { UserService } from 'user/user.service';
import { TagsService } from 'tags/tags.service';
import { MongooseModule, getConnectionToken } from '@nestjs/mongoose';
import postSchema from './schemas/post.schema';
import userSchema from 'user/schemas/user.schema';
import roleSchema from 'role/schemas/role.schema';
import tagSchema from 'tags/schemas/tags.schema';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const AutoIncrementFactory = require('mongoose-sequence');

@Module({
    imports: [
        MongooseModule.forFeatureAsync(
            [
                {
                    name: 'Post',
                    useFactory: async (connection: any) => {
                        const AutoIncrement: any =
                            AutoIncrementFactory(connection);
                        postSchema.plugin(AutoIncrement, {
                            inc_field: 'order',
                        });
                        return postSchema;
                    },
                    inject: [getConnectionToken('log-site')],
                },
            ],
            'log-site',
        ),
        MongooseModule.forFeature(
            [
                { name: 'User', schema: userSchema },
                { name: 'Role', schema: roleSchema },
                { name: 'Tag', schema: tagSchema },
            ],
            'log-site',
        ),
    ],
    controllers: [PostController],
    providers: [AuthService, PostService, UserService, TagsService],
})
export class PostModule {}
