import { Test, TestingModule } from '@nestjs/testing';
import * as dotenv from 'dotenv';
import { parse } from 'date-fns';
import { MongooseModule, getConnectionToken } from '@nestjs/mongoose';
import postSchema from './schemas/post.schema';
import userSchema from 'user/schemas/user.schema';
import roleSchema from '../role/schemas/role.schema';
import tagSchema from '../tags/schemas/tags.schema';
import { PostController } from './post.controller';
import { AuthService } from '../auth/auth.service';
import { PostService } from './post.service';
import { UserService } from '../user/user.service';
import { TagsService } from '../tags/tags.service';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const AutoIncrementFactory = require('mongoose-sequence');
dotenv.config();

describe('PostService unit spec', () => {
    let postService: PostService;
    beforeEach(async () => {
        const module = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(process.env.MONGO_DATABASE_URL, {
                    connectionName: 'log-site',
                    dbName: 'log-site-dev',
                }),
                MongooseModule.forFeatureAsync(
                    [
                        {
                            name: 'Post',
                            useFactory: async (connection: any) => {
                                const AutoIncrement: any =
                                    AutoIncrementFactory(connection);
                                postSchema.plugin(AutoIncrement, {
                                    inc_field: 'order',
                                });
                                return postSchema;
                            },
                            inject: [getConnectionToken('log-site')],
                        },
                    ],
                    'log-site',
                ),
                MongooseModule.forFeature(
                    [
                        { name: 'User', schema: userSchema },
                        { name: 'Role', schema: roleSchema },
                        { name: 'Tag', schema: tagSchema },
                    ],
                    'log-site',
                ),
            ],
            controllers: [PostController],
            providers: [AuthService, PostService, UserService, TagsService],
        }).compile();
        postService = module.get<PostService>(PostService);
    });
    test('test create', async () => {
        const result = await postService.post({
            title: 'dwedewd',
            author: '64aa99d1661fde8ee14d60c5',
            authorName: '운영자',
            summary: 'dwedewd dwedwe 64bcfab540c3ba995e60c0ed 운영자',
            text: 'dewdwed',
            tags: ['dwedwe'],
            category: '64bcfab540c3ba995e60c0ed',
        });
        console.log(result);
    }, 1800000);
});
