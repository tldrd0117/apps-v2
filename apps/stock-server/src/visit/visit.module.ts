import { Module } from '@nestjs/common';
import { VisitController } from './visit.controller';
import { VisitService } from './visit.service';
import { PostService } from 'post/post.service';
import { UserService } from 'user/user.service';
import { TagsService } from 'tags/tags.service';
import { AuthService } from 'auth/auth.service';
import visitSchema from './schemas/visit.schema';
import visitHistorySchema from 'visitHistory/schemas/visitHistory.schema';
import postSchema from 'post/schemas/post.schema';
import userSchema from 'user/schemas/user.schema';
import roleSchema from 'role/schemas/role.schema';
import tagsSchema from 'tags/schemas/tags.schema';
import { MongooseModule, getConnectionToken } from '@nestjs/mongoose';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const AutoIncrementFactory = require('mongoose-sequence');
@Module({
    imports: [
        MongooseModule.forFeature(
            [{ name: 'User', schema: userSchema }],
            'log-site',
        ),
        MongooseModule.forFeature(
            [{ name: 'Role', schema: roleSchema }],
            'log-site',
        ),
        MongooseModule.forFeature(
            [{ name: 'Tag', schema: tagsSchema }],
            'log-site',
        ),
        MongooseModule.forFeatureAsync(
            [
                {
                    name: 'Post',
                    useFactory: async (connection: any) => {
                        const AutoIncrement: any =
                            AutoIncrementFactory(connection);
                        postSchema.plugin(AutoIncrement, {
                            inc_field: 'order',
                        });
                        return postSchema;
                    },
                    inject: [getConnectionToken('log-site')],
                },
            ],
            'log-site',
        ),
        MongooseModule.forFeature(
            [{ name: 'Visit', schema: visitSchema }],
            'log-site',
        ),
        MongooseModule.forFeature(
            [{ name: 'VisitHistory', schema: visitHistorySchema }],
            'log-site',
        ),
    ],
    controllers: [VisitController],
    providers: [
        AuthService,
        VisitService,
        PostService,
        UserService,
        TagsService,
    ],
})
export class VisitModule {}
