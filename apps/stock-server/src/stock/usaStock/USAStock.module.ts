import { Module } from '@nestjs/common';
import { USAStockController } from './USAStock.controller';

@Module({
    imports: [],
    controllers: [USAStockController],
})
export class USAStockModule {}
