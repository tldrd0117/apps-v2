import {
    Controller,
    Get,
    Post,
    Body,
    UseGuards,
    Query,
    StreamableFile,
    Res,
} from '@nestjs/common';
import responseUtils from 'common/utils/responseUtils';
import { createReadStream } from 'fs';
import { join } from 'path';
import type { Response } from 'express';

@Controller('usaStock')
export class USAStockController {
    @Get()
    getFile(
        @Query('filename') fileName,
        @Res({ passthrough: true }) res: Response,
    ): StreamableFile {
        const file = createReadStream(
            join(process.cwd(), 'resources', 'stock', fileName),
        );
        res.set({
            'Content-Type': 'application/json',
            'Content-Disposition': `attachment; filename="${fileName}"`,
        });
        return new StreamableFile(file);
    }
}
