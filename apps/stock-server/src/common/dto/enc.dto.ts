export interface EncData {
    enc: string;
}

export interface ErrorBody {
    error: object;
}
