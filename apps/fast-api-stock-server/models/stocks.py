from typing import Optional, Union
from odmantic import Model
from datetime import datetime

from pydantic import BaseModel


class EndOfDayPriceRequest(BaseModel):
    symbol: str
    skip: int = 0
    limit: int = 500
    startDate: str = "1975-01-01"
    endDate: Optional[str] = None


class EndOfDayPrice(Model):
    symbol: str
    date: Union[str, datetime]
    open: str
    high: str
    low: str
    close: str
    adjClose: str
    volume: str
    model_config = {
        "collection": "fmp_batch_eod_prices",
    }
