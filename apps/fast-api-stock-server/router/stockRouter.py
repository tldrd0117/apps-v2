from fastapi import APIRouter, Depends
from services.stockService import findEndOfDayStockPrices
from typing import List
from models.stocks import *
from datetime import datetime

router = APIRouter(prefix="/stock")


@router.get("/", response_model=List[EndOfDayPrice])
async def getEndOfDayStockPrice(query: EndOfDayPriceRequest = Depends()):
    result = await findEndOfDayStockPrices(
        query.symbol,
        query.startDate,
        query.endDate,
        skip=query.skip,
        limit=query.limit,
    )
    return result
