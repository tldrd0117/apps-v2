from typing import List, Union

from fastapi import FastAPI

from dataSource.envs import getEnv

from models.stocks import EndOfDayPrice
import json
from router import stockRouter

app = FastAPI()

app.include_router(stockRouter.router)


@app.get("/")
async def read_root():
    return {"hello": "world"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: Union[str, None] = None):
    return {"item_id": item_id, "q": q}
