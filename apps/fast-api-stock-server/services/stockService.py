from dataSource.mongoEngine import getEngine
from models.stocks import EndOfDayPrice
from datetime import datetime

engine = getEngine()


async def findEndOfDayStockPrices(
    symbol,
    startDate,
    endDate=None,
    sort=EndOfDayPrice.date.desc(),
    skip=0,
    limit=500,
) -> float:
    startDate = datetime.strptime(startDate, "%Y-%m-%d")
    if endDate:
        endDate = datetime.strptime(endDate, "%Y-%m-%d")
    else:
        endDate = datetime.now()
    queryContents = {
        "symbol": symbol,
        "date": {"$gte": startDate, "$lte": endDate},
    }
    return await engine.find(
        EndOfDayPrice, queryContents, sort=sort, skip=skip, limit=limit
    )
