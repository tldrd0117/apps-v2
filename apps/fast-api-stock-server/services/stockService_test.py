import pytest
from stockService import findEndOfDayStockPrices
from models.stocks import EndOfDayPrice


@pytest.mark.asyncio
async def test_findEndOfDayStockPrices():
    # Define test parameters
    # Call the async function
    result = await findEndOfDayStockPrices(
        "AAPL", "2021-01-01", sort=EndOfDayPrice.date.desc(), skip=0, limit=500
    )
    print(result)
    assert len(result) > 0
