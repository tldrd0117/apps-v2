from dataSource.envs import getEnv
from odmantic import AIOEngine, Model
from motor.motor_asyncio import AsyncIOMotorClient

engine = None


def createEngine():
    address = getEnv("EXTERNAL_MONGO_DATABASE_PATH")
    print(address)
    client = AsyncIOMotorClient(address)
    return AIOEngine(client=client, database="stock")


def getEngine():
    global engine
    if engine is None:
        engine = createEngine()

    return engine
