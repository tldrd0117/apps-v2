from stock.simulates.common.BackTest import (
    BackTest,
    RebalanceOrders,
    Schedule,
    Interval,
    RebalanceOrder,
    Constants,
)
from datetime import datetime
import os


class TestRunBackTest(BackTest):
    def initalize(self):
        self.portfolioPath = (
            f"{Constants.BASE_PROJECT_ROOT}/packages/stock-resources/portfolios"
        )
        self.setStartDate("20080101")
        self.setEndDate("20200110")
        self.addEntity("AAPL")
        self.addEntity("BIL")
        self.setBalance(10000)
        self.addPortfolio("testPortfolio2", 1.0)
        self.addSchedules(
            Schedule(
                name="testSchedule",
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(day=1),
            )
        )

    def onSchedule(self, name: str, date: datetime):
        print("onSchedule", date)
        self.rebalanceByPercent(
            "testPortfolio2",
            RebalanceOrders(
                RebalanceOrder("AAPL", 0.5),
                RebalanceOrder("BIL", 0.5),
            ),
        )

    def onScheduleEndOfMarket(self, name: str, date: datetime):
        print("portFolio", self.getPortfolio("testPortfolio2"))

    def onEndOfBackTest(self):
        files_and_directories = os.listdir(
            f"{Constants.BASE_PROJECT_ROOT}/packages/stock-resources/portfolios"
        )
        print(files_and_directories)
