import pymongo
from .envs import getEnv


def getDBName():
    return getEnv("STOCK_DB_NAME")


def getDBAddress():
    return getEnv("EXTERNAL_MONGO_DATABASE_PATH")


def getClient():
    return mc


def getDB():
    return mc[getDBName()]


def getCollection(collection):
    return getDB()[collection]


try:
    mc = pymongo.MongoClient(getEnv("EXTERNAL_MONGO_DATABASE_PATH"))
except Exception as e:
    print(e)
