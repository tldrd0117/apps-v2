import React from 'react'
import {
  Divider as MuiDivider,
  DividerProps as MuiDividerProps,
} from '@mui/material'

interface DividerProps extends MuiDividerProps {}

const Divider = ({ ...props }: DividerProps) => {
  return <MuiDivider {...props} />
}
export { Divider }
