import type { Meta, StoryObj } from '@storybook/react'
import React from 'react'
import { Typography } from './Typography'

const meta: Meta<typeof Typography> = {
  component: Typography,
}
export default meta

type Story = StoryObj<typeof Typography>

export const Variants: Story = {
  args: {
    variant: 'h1',
  },
  render: () => (
    <>
      <Typography variant="h1">h1</Typography>
      <Typography variant="h2">h2</Typography>
      <Typography variant="h3">h3</Typography>
      <Typography variant="h4">h4</Typography>
      <Typography variant="h5">h5</Typography>
      <Typography variant="h6">h6</Typography>
      <Typography variant="subtitle1">subtitle1</Typography>
      <Typography variant="subtitle2">subtitle2</Typography>
      <Typography variant="body1">body1</Typography>
      <Typography variant="body2">body2</Typography>
      <Typography variant="inherit">inherit</Typography>
    </>
  ),
}
