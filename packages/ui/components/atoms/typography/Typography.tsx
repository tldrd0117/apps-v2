import React from 'react'
import {
  Typography as MuiTypography,
  TypographyProps as MuiTypographyProps,
} from '@mui/material'

type TypographyProps = MuiTypographyProps & {}

const Typography = (props: TypographyProps) => {
  return <MuiTypography {...props} />
}
export { Typography }
