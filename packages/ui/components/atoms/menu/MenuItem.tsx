import React from 'react'
import {
  MenuItem as MuiMenuItem,
  MenuItemProps as MuiMenuItemProps,
} from '@mui/material'

interface MenuItemProps extends MuiMenuItemProps {}

const MenuItem = ({ ...props }: MenuItemProps) => {
  return <MuiMenuItem {...props} />
}
export { MenuItem }
