import React from 'react'
import { Menu as MuiMenu, MenuProps as MuiMenuProps } from '@mui/material'

interface MenuProps extends MuiMenuProps {}

const Menu = ({ ...props }: MenuProps) => {
  return <MuiMenu {...props} />
}
export { Menu }
