import type { Meta, StoryObj } from '@storybook/react'
import React, { MouseEvent } from 'react'
import { Menu } from './Menu'
import { Button } from '../button'
import { MenuItem } from './MenuItem'

const meta: Meta<typeof Menu> = {
  component: Menu,
}
export default meta

type Story = StoryObj<typeof Menu>

export const Basic: Story = {
  render: () => {
    const [anchorEl, setAnchorEl] = React.useState<any>(null)
    const open = Boolean(anchorEl)
    const handleClick = (event: MouseEvent) => {
      setAnchorEl(event.currentTarget)
    }
    const handleClose = () => {
      setAnchorEl(null)
    }
    return (
      <div>
        <Button
          id="basic-button"
          aria-controls={open ? 'basic-menu' : undefined}
          aria-haspopup="true"
          aria-expanded={open ? 'true' : undefined}
          onClick={handleClick}
        >
          Dashboard
        </Button>
        <Menu
          id="basic-menu"
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}
        >
          <MenuItem onClick={handleClose}>Profile</MenuItem>
          <MenuItem onClick={handleClose}>My account</MenuItem>
          <MenuItem onClick={handleClose}>Logout</MenuItem>
        </Menu>
      </div>
    )
  },
}
