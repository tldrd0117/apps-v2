import type { Meta, StoryObj } from '@storybook/react'
import React from 'react'
import { TextField } from './TextField'

const meta: Meta<typeof TextField> = {
  component: TextField,
}
export default meta

type Story = StoryObj<typeof TextField>

export const Outlined: Story = {
  args: {
    variant: 'outlined',
  },
}

export const Filled: Story = {
  args: {
    variant: 'filled',
  },
}

export const Standard: Story = {
  args: {
    variant: 'standard',
  },
}

export const Disabled: Story = {
  args: {
    disabled: true,
  },
}
