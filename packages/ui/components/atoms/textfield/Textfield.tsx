import React from 'react'
import {
  TextField as MuiTextField,
  TextFieldProps as MuiTextFieldProps,
} from '@mui/material'

type TextFieldProps = MuiTextFieldProps & {}

const TextField = (props: TextFieldProps) => {
  return <MuiTextField {...props} />
}
export { TextField }
