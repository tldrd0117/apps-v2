import type { Meta, StoryObj } from '@storybook/react'
import React from 'react'
import { Dialog } from './Dialog'
import { DialogTitle } from './DialogTitle'
import { DialogContent } from './DialogContent'
import { DialogContentText } from './DialogContentText'
import { DialogActions } from './DialogActions'
import { Button } from '../button/Button'

const meta: Meta<typeof Dialog> = {
  component: Dialog,
}
export default meta

type Story = StoryObj<typeof Dialog>

export const Basic: Story = {
  render: () => {
    const [open, setOpen] = React.useState(false)

    const handleClickOpen = () => {
      setOpen(true)
    }

    const handleClose = () => {
      setOpen(false)
    }
    return (
      <>
        <Button variant="outlined" onClick={handleClickOpen}>
          Open alert dialog
        </Button>
        <Dialog
          open={open}
          onClose={handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
        >
          <DialogTitle id="alert-dialog-title">
            {"Use Google's location service?"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              Let Google help apps determine location. This means sending
              anonymous location data to Google, even when no apps are running.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose}>Disagree</Button>
            <Button onClick={handleClose} autoFocus>
              Agree
            </Button>
          </DialogActions>
        </Dialog>
      </>
    )
  },
}
