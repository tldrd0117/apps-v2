import React from 'react'
import type { DialogContentProps as MuiDialogContentProps } from '@mui/material'
import { DialogContent as MuiDialogContent } from '@mui/material'

interface DialogContentProps extends MuiDialogContentProps {
  children?: React.ReactNode
}
export const DialogContent = ({ children, ...props }: DialogContentProps) => {
  return <MuiDialogContent {...props}>{children}</MuiDialogContent>
}
