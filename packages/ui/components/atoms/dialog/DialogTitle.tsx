import React from 'react'
import type { DialogTitleProps as MuiDialogTitleProps } from '@mui/material'
import { DialogTitle as MuiDialogTitle } from '@mui/material'

interface DialogTitleProps extends MuiDialogTitleProps {
  children?: React.ReactNode
}
export const DialogTitle = ({ children, ...props }: DialogTitleProps) => {
  return <MuiDialogTitle {...props}>{children}</MuiDialogTitle>
}
