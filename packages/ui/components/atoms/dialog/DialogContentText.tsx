import React from 'react'
import type { DialogContentTextProps as MuiDialogContentTextProps } from '@mui/material'
import { DialogContentText as MuiDialogContentText } from '@mui/material'

interface DialogContentTextProps extends MuiDialogContentTextProps {
  children?: React.ReactNode
}
export const DialogContentText = ({
  children,
  ...props
}: DialogContentTextProps) => {
  return <MuiDialogContentText {...props}>{children}</MuiDialogContentText>
}
