import React from 'react'
import type { DialogActionsProps as MuiDialogActionsProps } from '@mui/material'
import { DialogActions as MuiDialogActions } from '@mui/material'

interface DialogActionsProps extends MuiDialogActionsProps {
  children?: React.ReactNode
}
export const DialogActions = ({ children, ...props }: DialogActionsProps) => {
  return <MuiDialogActions {...props}>{children}</MuiDialogActions>
}
