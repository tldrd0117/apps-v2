import React from 'react'
import type { DialogProps as MuiDialogProps } from '@mui/material'
import { Dialog as MuiDialog } from '@mui/material'

interface DialogProps extends MuiDialogProps {
  children?: React.ReactNode
}
export const Dialog = ({ children, ...props }: DialogProps) => {
  return <MuiDialog {...props}>{children}</MuiDialog>
}
