import React from 'react'
import type { ButtonProps as MuiButtonProp } from '@mui/material'
import { Button as MuiButton } from '@mui/material'

interface ButtonProps extends MuiButtonProp {
  children?: React.ReactNode
}
export const Button = ({ children, ...props }: ButtonProps) => {
  return (
    <MuiButton {...props} type="button">
      {children}
    </MuiButton>
  )
}
