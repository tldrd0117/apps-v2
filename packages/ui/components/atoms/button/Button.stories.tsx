import type { Meta, StoryObj } from '@storybook/react'
import React from 'react'
import { Button } from './Button'

const meta: Meta<typeof Button> = {
  component: Button,
}
export default meta

type Story = StoryObj<typeof Button>

export const Text: Story = {
  args: {
    variant: 'text',
    children: 'Click me',
  },
}

export const Contained: Story = {
  args: {
    variant: 'contained',
    children: 'Click me',
  },
}

export const Outlined: Story = {
  args: {
    variant: 'outlined',
    children: 'Click me',
  },
}
