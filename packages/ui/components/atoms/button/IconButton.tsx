import React from 'react'
import type { IconButtonProps as MuiIconButtonProp } from '@mui/material'
import { IconButton as MuiIconButton } from '@mui/material'

interface IconButtonProps extends MuiIconButtonProp {
  children?: React.ReactNode
}
export const IconButton = ({ children, ...props }: IconButtonProps) => {
  return <MuiIconButton {...props}>{children}</MuiIconButton>
}
