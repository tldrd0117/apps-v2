import type { Meta, StoryObj } from '@storybook/react'
import React from 'react'
import { IconButton } from './IconButton'
import { Favorite } from '@mui/icons-material'

const meta: Meta<typeof IconButton> = {
  component: IconButton,
}
export default meta

type Story = StoryObj<typeof IconButton>

export const Icon: Story = {
  args: {
    children: (
      <>
        <Favorite color="primary" />
        <Favorite color="secondary" />
        <Favorite color="disabled" />
        <Favorite color="error" />
        <Favorite color="success" />
      </>
    ),
  },
  render: (args) => (
    <>
      <IconButton>
        <Favorite color="primary" />
      </IconButton>
      <IconButton>
        <Favorite color="secondary" />
      </IconButton>
      <IconButton>
        <Favorite color="disabled" />
      </IconButton>
      <IconButton>
        <Favorite color="error" />
      </IconButton>
      <IconButton>
        <Favorite color="success" />
      </IconButton>
    </>
  ),
}
