'use client'
import type { ReactNode } from 'react'
import React, { forwardRef } from 'react'

interface LinkButtonProps {
  onClick?: () => void
  href?: string
  children?: ReactNode
}

// `onClick`, `href`, and `ref` need to be passed to the DOM element
// for proper handling
const LinkButton = forwardRef<HTMLAnchorElement, LinkButtonProps>(
  ({ onClick, href, children }, ref) => {
    return (
      <a
        className="rounded px-2 py-1 bg-slate-500 text-white"
        href={href}
        onClick={onClick}
        ref={ref}
        suppressHydrationWarning
      >
        {children}
      </a>
    )
  }
)

LinkButton.displayName = 'LinkButton'

export { LinkButton }
