import React from 'react'

const TableDataCell = ({ children, ...props }: any) => {
  return <td {...props}>{children}</td>
}

export { TableDataCell }
