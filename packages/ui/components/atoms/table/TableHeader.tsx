import React from 'react'

const TableHeader = ({ children, ...props }: any) => {
  return <th {...props}>{children}</th>
}

export { TableHeader }
