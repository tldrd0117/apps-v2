import type { Meta, StoryObj } from '@storybook/react'
import React from 'react'
import { Table } from './Table'
import { TableHead } from './TableHead'
import { TableRow } from './TableRow'
import { TableHeader } from './TableHeader'
import { TableBody } from './TableBody'
import { TableDataCell } from './TableDataCell'

const meta: Meta<typeof Table> = {
  component: Table,
}
export default meta

type Story = StoryObj<typeof Table>

export const Basic: Story = {
  render: () => (
    <Table>
      <TableHead>
        <TableRow>
          <TableHeader>Header 1</TableHeader>
          <TableHeader>Header 2</TableHeader>
          <TableHeader>Header 3</TableHeader>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableDataCell>Cell 1</TableDataCell>
          <TableDataCell>Cell 2</TableDataCell>
          <TableDataCell>Cell 3</TableDataCell>
        </TableRow>
        <TableRow>
          <TableDataCell>Cell 4</TableDataCell>
          <TableDataCell>Cell 5</TableDataCell>
          <TableDataCell>Cell 6</TableDataCell>
        </TableRow>
      </TableBody>
    </Table>
  ),
}
