import React from 'react'

const TableRow = ({ children, ...props }: any) => {
  return <tr {...props}>{children}</tr>
}

export { TableRow }
