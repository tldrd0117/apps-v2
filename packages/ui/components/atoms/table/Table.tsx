import React from 'react'

const Table = ({ children, ...props }: any) => {
  return <table {...props}>{children}</table>
}

export { Table }
