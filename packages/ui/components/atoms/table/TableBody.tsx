import React from 'react'

const TableBody = ({ children, ...props }: any) => {
  return <tbody {...props}>{children}</tbody>
}

export { TableBody }
