import React from 'react'

const TableHead = ({ children, ...props }: any) => {
  return <thead {...props}>{children}</thead>
}

export { TableHead }
