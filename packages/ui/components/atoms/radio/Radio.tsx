import React from 'react'
import { Radio as MuiRadio, RadioProps as MuiRadioProps } from '@mui/material'

interface RadioProps extends MuiRadioProps {}

const Radio = (props: RadioProps) => {
  return <MuiRadio {...props} />
}
export { Radio }
