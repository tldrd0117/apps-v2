import type { Meta, StoryObj } from '@storybook/react'
import React from 'react'
import { Radio } from './Radio'

const meta: Meta<typeof Radio> = {
  component: Radio,
}
export default meta

type Story = StoryObj<typeof Radio>

export const DefaultChecked: Story = {
  args: {
    defaultChecked: true,
  },
}

export const Basic: Story = {
  args: {},
}

export const Disabled: Story = {
  args: {
    disabled: true,
  },
}

export const Checked: Story = {
  args: {
    checked: true,
  },
}
