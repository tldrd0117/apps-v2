import React from 'react'
import {
  Checkbox as MuiCheckbox,
  CheckboxProps as MuiCheckboxProps,
} from '@mui/material'

interface CheckboxProps extends MuiCheckboxProps {}

const Checkbox = (props: CheckboxProps) => {
  return <MuiCheckbox {...props} />
}
export { Checkbox }
