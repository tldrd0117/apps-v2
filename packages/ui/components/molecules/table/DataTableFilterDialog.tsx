import React from 'react'
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Button,
} from '../../atoms'
import { DataTableFilter } from './DataTable'

export interface DataTableFilterDialogProps {
  filters: DataTableFilter[]
}
const DataTableFilterDialog = ({ filters }: DataTableFilterDialogProps) => {
  return (
    <Dialog open={true}>
      <DialogTitle>Filter</DialogTitle>
      <DialogContent></DialogContent>
      <DialogActions>
        <Button>Save</Button>
        <Button>Cancel</Button>
      </DialogActions>
    </Dialog>
  )
}
export { DataTableFilterDialog }
