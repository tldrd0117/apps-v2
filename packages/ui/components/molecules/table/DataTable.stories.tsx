import type { Meta, StoryObj } from '@storybook/react'
import React from 'react'
import { DataTable } from './DataTable'

const meta: Meta<typeof DataTable> = {
  component: DataTable,
}
export default meta

type Story = StoryObj<typeof DataTable>

export const Basic: Story = {
  render: () => (
    <DataTable
      columns={[
        {
          id: 'column1',
          label: 'Column 1',
        },
        {
          id: 'column2',
          label: 'Column 2',
        },
        {
          id: 'column3',
          label: 'Column 3',
        },
      ]}
      data={[
        {
          id: 'row1',
          column1: 'Cell 1',
          column2: 'Cell 2',
          column3: 'Cell 3',
        },
        {
          id: 'row2',
          column1: 'Cell 4',
          column2: 'Cell 5',
          column3: 'Cell 6',
        },
      ]}
    />
  ),
}
