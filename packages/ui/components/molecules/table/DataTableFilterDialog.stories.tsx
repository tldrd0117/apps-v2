import type { Meta, StoryObj } from '@storybook/react'
import React from 'react'
import { DataTableFilterDialog } from './DataTableFilterDialog'

const meta: Meta<typeof DataTableFilterDialog> = {
  component: DataTableFilterDialog,
}
export default meta

type Story = StoryObj<typeof DataTableFilterDialog>

export const Basic: Story = {
  render: () => (
    <DataTableFilterDialog
      filters={[
        {
          id: 'filter1',
          label: 'Filter 1',
        },
        {
          id: 'filter2',
          label: 'Filter 2',
        },
        {
          id: 'filter3',
          label: 'Filter 3',
        },
      ]}
    />
  ),
}
