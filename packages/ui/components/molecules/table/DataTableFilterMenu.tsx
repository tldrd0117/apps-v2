import { Button, Dialog, MenuItem } from '@mui/material'
import { IconButton } from '../../atoms/button/IconButton'
import React, { MouseEvent, useRef } from 'react'
import { FilterAlt } from '@mui/icons-material'
import { Menu } from '../../atoms/menu'
import { Divider } from '../../atoms/divider/Divider'
import { DataTableFilter } from './DataTable'

export interface DataTableFilterMenuProps {
  filters?: DataTableFilter[]
  onItemClick?: (id: string) => void
}

const DataTableFilterMenu = ({
  filters = [],
  onItemClick,
}: DataTableFilterMenuProps) => {
  const [anchorEl, setAnchorEl] = React.useState<Element | null>(null)
  const open = Boolean(anchorEl)
  const handleClick = (event: MouseEvent) => {
    setAnchorEl(event.target as Element)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  const hanldeItemClick = (id: string) => {
    onItemClick?.(id)
    handleClose()
  }
  return (
    <div>
      <IconButton size="small" onClick={handleClick}>
        <FilterAlt fontSize="small" />
      </IconButton>
      <Menu open={open} anchorEl={anchorEl} onClose={() => handleClose()}>
        {filters.map((filter) => (
          <MenuItem key={filter.id} onClick={() => hanldeItemClick(filter.id)}>
            {filter.label}
          </MenuItem>
        ))}
        <Divider sx={{ my: 0.5 }} />
        <MenuItem onClick={() => handleClose()}>Add</MenuItem>
      </Menu>
    </div>
  )
}

export { DataTableFilterMenu }
