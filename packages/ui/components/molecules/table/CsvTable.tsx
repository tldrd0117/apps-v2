import React, { useEffect, useState } from 'react'
import Papa from 'papaparse'
import type { TableColumn } from 'react-data-table-component'
import DataTable from 'react-data-table-component'
import type { Primitive } from 'react-data-table-component/dist/DataTable/types'

interface CSVTableProps {
  data: string
}

type CsvTableDataType = Record<string, Primitive>

const CsvTable = ({ data }: CSVTableProps) => {
  const [parsedData, setParsedData] = useState<CsvTableDataType[]>([])
  const [columns, setColumns] = useState<TableColumn<CsvTableDataType>[]>([])
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    Papa.parse<CsvTableDataType>(data, {
      download: true,
      header: true,
      dynamicTyping: true,
      complete: (file) => {
        setParsedData(file.data)
        setColumns(
          file.meta.fields?.map((col) => ({
            name: col,
            selector: (row) => row[col],
            sortable: true,
          })) ?? []
        )
        setLoading(false)
      },
    })
  }, [data])
  return (
    <div>
      {loading ? (
        <div>loading...</div>
      ) : (
        <DataTable<CsvTableDataType>
          columns={columns}
          data={parsedData}
          pagination
        />
      )}
    </div>
  )
}

export { CsvTable }
