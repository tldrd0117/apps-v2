import type { Meta, StoryObj } from '@storybook/react'
import React from 'react'
import { DataTableFilterMenu } from './DataTableFilterMenu'

const meta: Meta<typeof DataTableFilterMenu> = {
  component: DataTableFilterMenu,
}
export default meta

type Story = StoryObj<typeof DataTableFilterMenu>

export const Basic: Story = {
  render: () => (
    <DataTableFilterMenu
      filters={[
        {
          id: 'filter1',
          label: 'Filter 1',
        },
        {
          id: 'filter2',
          label: 'Filter 2',
        },
        {
          id: 'filter3',
          label: 'Filter 3',
        },
      ]}
    />
  ),
}
