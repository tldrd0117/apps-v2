/**
 * Data Table Component
 *
 * This component is a wrapper around the Table component that provides a more feature-rich table experience.
 *
 * Features:
 * - filter
 * - selectable rows
 * - sorting
 * - pagination
 * - column visibility
 * - virtualization
 * - expandable rows
 * - editable rows
 * - density
 */

import React, { useMemo } from 'react'
import {
  Table,
  TableBody,
  TableDataCell,
  TableHead,
  TableHeader,
  TableRow,
} from '../../atoms'
import { DataTableFilterMenu } from './DataTableFilterMenu'

export interface DataTableFilter {
  label: string
  id: string
  operator?: string
  value?: string
}

interface Column {
  id: string
  label: string
  width?: number
  align?: 'left' | 'center' | 'right'
  sortable?: boolean
  filterValue?: string
  filterOperator?: string
  filterable?: boolean
  editable?: boolean
  visible?: boolean
}

interface DataTableProps {
  columns: Column[]
  data: any[]
  filterable?: boolean
  selectable?: boolean
  sortable?: boolean
  pagination?: boolean
  columnVisibility?: boolean
  virtualization?: boolean
  expandableRows?: boolean
  editableRows?: boolean
  density?: 'compact' | 'normal' | 'comfortable'
}

const stringFilterOperators = [
  'contains',
  'equals',
  'startsWith',
  'endsWith',
  'isEmpty',
  'isNotEmpty',
]

const numberFilterOperators = [
  'equals',
  'greaterThan',
  'greaterThanOrEqual',
  'lessThan',
  'lessThanOrEqual',
  'isEmpty',
  'isNotEmpty',
]

const booleanFilterOperators = ['equals', 'isEmpty', 'isNotEmpty']

const dateFilterOperators = [
  'equals',
  'greaterThan',
  'greaterThanOrEqual',
  'lessThan',
  'lessThanOrEqual',
  'isEmpty',
  'isNotEmpty',
]

const DataTable = ({ columns, data }: DataTableProps) => {
  const filters = useMemo(() => {}, [data, columns])
  const columnModel = useMemo(() => {}, [columns])
  return (
    <Table>
      <TableHead>
        <TableRow>
          {columns.map((column: Column) => (
            <TableHeader key={column.id}>
              {column.label} <DataTableFilterMenu />
            </TableHeader>
          ))}
        </TableRow>
      </TableHead>
      <TableBody>
        {data.map((row: any) => (
          <TableRow key={row.id}>
            {Object.keys(row).map((key) => {
              if (!columns.find((column: Column) => column.id === key)) {
                return null
              }
              return <TableDataCell key={key}>{row[key]}</TableDataCell>
            })}
          </TableRow>
        ))}
      </TableBody>
    </Table>
  )
}

export { DataTable }
