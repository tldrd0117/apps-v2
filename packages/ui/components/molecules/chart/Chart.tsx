import type { ChangeEvent } from 'react'
import React, { useEffect, useRef, useState } from 'react'
import type Dygraph from 'dygraphs'
import { Checkbox, Button } from '../../atoms'

interface ChartProps {
  data: string
  logScale?: boolean
}

const Chart = ({ data, logScale = true }: ChartProps) => {
  const ref = useRef<HTMLDivElement>(null)
  const g = useRef<Dygraph | null>(null)
  const [columns, setColumns] = useState<string[] | undefined>([])
  const [checks, setChecks] = useState<boolean[]>([])
  const [csvData, setCsvData] = useState<string>('')
  const [islogScale, setIsLogScale] = useState<boolean>(logScale)

  const readCSV = (url: string) => {
    return fetch(url)
      .then((response) => {
        // HTTP 응답이 성공적으로 완료되면, Blob 객체로 변환합니다.
        if (!response.ok) {
          throw new Error('Network response was not ok')
        }
        return response.blob()
      })
      .then((blob) => {
        return new Promise((resolve) => {
          const reader = new FileReader()
          reader.onload = (event) => {
            // 파일의 내용을 콘솔에 출력합니다.
            const contents = event.target?.result?.toString()
            const oneLine = contents?.split('\n')[0]
            const cols = oneLine?.split(',')
            resolve({ contents, cols })
          }
          // Blob 객체를 텍스트로 읽어옵니다.
          reader.readAsText(blob)
        })
      })
  }

  useEffect(() => {
    const fetchData = async () => {
      const { contents, cols }: { contents?: string; cols?: string[] } =
        (await readCSV(data)) as { contents?: string; cols?: string[] }
      setCsvData(contents || '')
      setColumns(cols?.slice(1) || [])

      setChecks(
        cols?.slice(1)?.map((col: string) => col.endsWith('totalAsset')) || []
      )
    }
    void fetchData()
  }, [data])

  useEffect(() => {
    if (ref.current && csvData) {
      const getDygraph = async () => {
        const Dygraph = (await import('dygraphs')).default
        g.current = new Dygraph(ref.current ?? '', csvData, {
          visibility: checks,
          logscale: islogScale,
          maxNumberWidth: 20,
        })
      }
      void getDygraph()
    }
  }, [ref, csvData, islogScale])

  const handleLogScale = () => {
    setIsLogScale(!islogScale)
  }

  return (
    <>
      <div ref={ref} />
      {columns?.map((col, i) => (
        <div key={col + i.toString()}>
          <Checkbox
            checked={checks[i]}
            key={i}
            onChange={(_: ChangeEvent<HTMLInputElement>) => {
              const chagnedChecks = [...checks]
              chagnedChecks[i] = !checks[i]
              g.current?.setVisibility(i, chagnedChecks[i])
              setChecks(chagnedChecks)
            }}
          />
          <label>{col}</label>
        </div>
      ))}
      current: {islogScale ? 'Log Scale' : 'Linear Scale'}
      <Button onClick={handleLogScale}>Toggle</Button>
    </>
  )
}

export { Chart }
