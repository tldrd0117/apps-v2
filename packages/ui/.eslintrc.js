module.exports = {
  extends: ['plugin:storybook/recommended', 'custom/react-internal'],
  'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
}
