import { ErrorCode, NatsTypescriptTemplateError } from './NatsTypescriptTemplateError';
import * as Nats from 'nats';
import * as streetlightStreetlightIdCommandTurnonChannel from "./channels/StreetlightStreetlightIdCommandTurnon";
import TurnOn from "./models/TurnOn";
export { streetlightStreetlightIdCommandTurnonChannel };
export { TurnOn };
export { ErrorCode, NatsTypescriptTemplateError };
/**
 * @class NatsAsyncApiClient
 *
 * The generated client based on your AsyncAPI document.
 */
export declare class NatsAsyncApiClient {
    private nc?;
    private js?;
    private codec?;
    private options?;
    /**
     * Try to connect to the NATS server with the different payloads.
     * @param options to use, payload is omitted if sat in the AsyncAPI document.
     */
    connect(options: Nats.ConnectionOptions, codec?: Nats.Codec<any>): Promise<void>;
    /**
     * Disconnect all clients from the server
     */
    disconnect(): Promise<void>;
    /**
     * Returns whether or not any of the clients are closed
     */
    isClosed(): boolean;
    /**
     * Try to connect to the NATS server with user credentials
     *
     * @param userCreds to use
     * @param options to connect with
     */
    connectWithUserCreds(userCreds: string, options?: Nats.ConnectionOptions, codec?: Nats.Codec<any>): Promise<void>;
    /**
     * Try to connect to the NATS server with user and password
     *
     * @param user username to use
     * @param pass password to use
     * @param options to connect with
     */
    connectWithUserPass(user: string, pass: string, options?: Nats.ConnectionOptions, codec?: Nats.Codec<any>): Promise<void>;
    /**
     * Try to connect to the NATS server which has no authentication
     
      * @param host to connect to
      * @param options to connect with
      */
    connectToHost(host: string, options?: Nats.ConnectionOptions, codec?: Nats.Codec<any>): Promise<void>;
    /**
     * Connects the client to the AsyncAPI server called local.
     * Local server used during development and testing
     */
    connectToLocal(codec?: Nats.Codec<any>): Promise<void>;
    /**
     * Subscribe to the `streetlight/{streetlight_id}/command/turnon`
     *
     * Channel for the turn on command which should turn on the streetlight
     *
     * @param onDataCallback to call when messages are received
     * @param streetlight_id parameter to use in topic
     * @param flush ensure client is force flushed after subscribing
     * @param options to subscribe with, bindings from the AsyncAPI document overwrite these if specified
     */
    subscribeToStreetlightStreetlightIdCommandTurnon(onDataCallback: (err?: NatsTypescriptTemplateError, msg?: TurnOn, streetlight_id?: string) => void, streetlight_id: string, flush?: boolean, options?: Nats.SubscriptionOptions): Promise<Nats.Subscription>;
    /**
     * JetStream pull function.
     *
     * Pull message from `streetlight/{streetlight_id}/command/turnon`
     *
     * Channel for the turn on command which should turn on the streetlight
     *
     * @param onDataCallback to call when messages are received
     * @param streetlight_id parameter to use in topic
     * @param options to pull message with, bindings from the AsyncAPI document overwrite these if specified
     */
    jetStreamPullStreetlightStreetlightIdCommandTurnon(onDataCallback: (err?: NatsTypescriptTemplateError, msg?: TurnOn, streetlight_id?: string, jetstreamMsg?: Nats.JsMsg) => void, streetlight_id: string): void;
    /**
     * Push subscription to the `streetlight/{streetlight_id}/command/turnon`
     *
     * Channel for the turn on command which should turn on the streetlight
     *
     * @param onDataCallback to call when messages are received
     * @param streetlight_id parameter to use in topic
     * @param flush ensure client is force flushed after subscribing
     * @param options to subscribe with, bindings from the AsyncAPI document overwrite these if specified
     */
    jetStreamPushSubscribeToStreetlightStreetlightIdCommandTurnon(onDataCallback: (err?: NatsTypescriptTemplateError, msg?: TurnOn, streetlight_id?: string, jetstreamMsg?: Nats.JsMsg) => void, streetlight_id: string, options: Nats.ConsumerOptsBuilder | Partial<Nats.ConsumerOpts>): Promise<Nats.JetStreamSubscription>;
    /**
     * Push subscription to the `streetlight/{streetlight_id}/command/turnon`
     *
     * Channel for the turn on command which should turn on the streetlight
     *
     * @param onDataCallback to call when messages are received
     * @param streetlight_id parameter to use in topic
     * @param flush ensure client is force flushed after subscribing
     * @param options to subscribe with, bindings from the AsyncAPI document overwrite these if specified
     */
    jetStreamPullSubscribeToStreetlightStreetlightIdCommandTurnon(onDataCallback: (err?: NatsTypescriptTemplateError, msg?: TurnOn, streetlight_id?: string, jetstreamMsg?: Nats.JsMsg) => void, streetlight_id: string, options: Nats.ConsumerOptsBuilder | Partial<Nats.ConsumerOpts>): Promise<Nats.JetStreamPullSubscription>;
    /**
     * JetStream fetch function.
     *
     * Pull message from `streetlight/{streetlight_id}/command/turnon`
     *
     * Channel for the turn on command which should turn on the streetlight
     *
     * @param onDataCallback to call when messages are received
     * @param streetlight_id parameter to use in topic
     * @param options to pull message with, bindings from the AsyncAPI document overwrite these if specified
     */
    jetStreamFetchStreetlightStreetlightIdCommandTurnon(onDataCallback: (err?: NatsTypescriptTemplateError, msg?: TurnOn, streetlight_id?: string, jetstreamMsg?: Nats.JsMsg) => void, streetlight_id: string, durable: string, options?: Partial<Nats.PullOptions>): void;
}
