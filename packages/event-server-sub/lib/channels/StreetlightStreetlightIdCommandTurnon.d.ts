import TurnOn from '../models/TurnOn';
import * as Nats from 'nats';
import { NatsTypescriptTemplateError } from '../NatsTypescriptTemplateError';
/**
 * Module which wraps functionality for the `streetlight/{streetlight_id}/command/turnon` channel
 * @module streetlightStreetlightIdCommandTurnon
 */
/**
 * Internal functionality to setup subscription on the `streetlight/{streetlight_id}/command/turnon` channel
 *
 * @param onDataCallback to call when messages are received
 * @param nc to subscribe with
 * @param codec used to convert messages
 * @param streetlight_id parameter to use in topic
 * @param options to subscribe with, bindings from the AsyncAPI document overwrite these if specified
 */
export declare function subscribe(onDataCallback: (err?: NatsTypescriptTemplateError, msg?: TurnOn, streetlight_id?: string) => void, nc: Nats.NatsConnection, codec: Nats.Codec<any>, streetlight_id: string, options?: Nats.SubscriptionOptions): Promise<Nats.Subscription>;
/**
 * Internal functionality to setup jetstrema pull on the `streetlight/{streetlight_id}/command/turnon` channel
 *
 * @param onDataCallback to call when messages are received
 * @param js client to pull with
 * @param codec used to convert messages
 * @param streetlight_id parameter to use in topic
 */
export declare function jetStreamPull(onDataCallback: (err?: NatsTypescriptTemplateError, msg?: TurnOn, streetlight_id?: string, jetstreamMsg?: Nats.JsMsg) => void, js: Nats.JetStreamClient, codec: Nats.Codec<any>, streetlight_id: string): void;
/**
 * Internal functionality to setup jetstream push subscription on the `streetlight/{streetlight_id}/command/turnon` channel
 *
 * @param onDataCallback to call when messages are received
 * @param nc to subscribe with
 * @param codec used to convert messages
 * @param streetlight_id parameter to use in topic
 * @param options to subscribe with, bindings from the AsyncAPI document overwrite these if specified
 */
export declare function jetStreamPushSubscribe(onDataCallback: (err?: NatsTypescriptTemplateError, msg?: TurnOn, streetlight_id?: string, jetstreamMsg?: Nats.JsMsg) => void, js: Nats.JetStreamClient, codec: Nats.Codec<any>, streetlight_id: string, options: Nats.ConsumerOptsBuilder | Partial<Nats.ConsumerOpts>): Promise<Nats.JetStreamSubscription>;
/**
 * Internal functionality to setup jetstream pull subscription on the `streetlight/{streetlight_id}/command/turnon` channel
 *
 * @param onDataCallback to call when messages are received
 * @param nc to subscribe with
 * @param codec used to convert messages
 * @param streetlight_id parameter to use in topic
 */
export declare function jetStreamPullSubscribe(onDataCallback: (err?: NatsTypescriptTemplateError, msg?: TurnOn, streetlight_id?: string, jetstreamMsg?: Nats.JsMsg) => void, js: Nats.JetStreamClient, codec: Nats.Codec<any>, streetlight_id: string, options: Nats.ConsumerOptsBuilder | Partial<Nats.ConsumerOpts>): Promise<Nats.JetStreamPullSubscription>;
/**
 * Internal functionality to setup jetstrema fetch on the `streetlight/{streetlight_id}/command/turnon` channel
 *
 * @param onDataCallback to call when messages are received
 * @param js client to fetch with
 * @param codec used to convert messages
 * @param streetlight_id parameter to use in topic
 */
export declare function jetsStreamFetch(onDataCallback: (err?: NatsTypescriptTemplateError, msg?: TurnOn, streetlight_id?: string, jetstreamMsg?: Nats.JsMsg) => void, js: Nats.JetStreamClient, codec: Nats.Codec<any>, streetlight_id: string, durable: string, options?: Partial<Nats.PullOptions>): void;
