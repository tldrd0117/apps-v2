"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.jetsStreamFetch = exports.jetStreamPullSubscribe = exports.jetStreamPushSubscribe = exports.jetStreamPull = exports.subscribe = void 0;
const TurnOn_1 = __importDefault(require("../models/TurnOn"));
const NatsTypescriptTemplateError_1 = require("../NatsTypescriptTemplateError");
/**
 * Module which wraps functionality for the `streetlight/{streetlight_id}/command/turnon` channel
 * @module streetlightStreetlightIdCommandTurnon
 */
/**
 * Internal functionality to setup subscription on the `streetlight/{streetlight_id}/command/turnon` channel
 *
 * @param onDataCallback to call when messages are received
 * @param nc to subscribe with
 * @param codec used to convert messages
 * @param streetlight_id parameter to use in topic
 * @param options to subscribe with, bindings from the AsyncAPI document overwrite these if specified
 */
function subscribe(onDataCallback, nc, codec, streetlight_id, options) {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        let subscribeOptions = Object.assign({}, options);
        try {
            let subscription = nc.subscribe(`streetlight.${streetlight_id}.command.turnon`, subscribeOptions);
            (() => __awaiter(this, void 0, void 0, function* () {
                var e_1, _a;
                try {
                    for (var subscription_1 = __asyncValues(subscription), subscription_1_1; subscription_1_1 = yield subscription_1.next(), !subscription_1_1.done;) {
                        const msg = subscription_1_1.value;
                        const unmodifiedChannel = `streetlight.{streetlight_id}.command.turnon`;
                        let channel = msg.subject;
                        const streetlightIdSplit = unmodifiedChannel.split("{streetlight_id}");
                        const splits = [
                            streetlightIdSplit[0],
                            streetlightIdSplit[1]
                        ];
                        channel = channel.substring(splits[0].length);
                        const streetlightIdEnd = channel.indexOf(splits[1]);
                        const streetlightIdParam = "" + channel.substring(0, streetlightIdEnd);
                        let receivedData = codec.decode(msg.data);
                        onDataCallback(undefined, TurnOn_1.default.unmarshal(receivedData), streetlightIdParam);
                    }
                }
                catch (e_1_1) { e_1 = { error: e_1_1 }; }
                finally {
                    try {
                        if (subscription_1_1 && !subscription_1_1.done && (_a = subscription_1.return)) yield _a.call(subscription_1);
                    }
                    finally { if (e_1) throw e_1.error; }
                }
                console.log("subscription closed");
            }))();
            resolve(subscription);
        }
        catch (e) {
            reject(NatsTypescriptTemplateError_1.NatsTypescriptTemplateError.errorForCode(NatsTypescriptTemplateError_1.ErrorCode.INTERNAL_NATS_TS_ERROR, e));
        }
    }));
}
exports.subscribe = subscribe;
/**
 * Internal functionality to setup jetstrema pull on the `streetlight/{streetlight_id}/command/turnon` channel
 *
 * @param onDataCallback to call when messages are received
 * @param js client to pull with
 * @param codec used to convert messages
 * @param streetlight_id parameter to use in topic
 */
function jetStreamPull(onDataCallback, js, codec, streetlight_id) {
    const stream = `streetlight.${streetlight_id}.command.turnon`;
    (() => __awaiter(this, void 0, void 0, function* () {
        const msg = yield js.pull(stream, 'durableName');
        const unmodifiedChannel = `streetlight.{streetlight_id}.command.turnon`;
        let channel = msg.subject;
        const streetlightIdSplit = unmodifiedChannel.split("{streetlight_id}");
        const splits = [
            streetlightIdSplit[0],
            streetlightIdSplit[1]
        ];
        channel = channel.substring(splits[0].length);
        const streetlightIdEnd = channel.indexOf(splits[1]);
        const streetlightIdParam = "" + channel.substring(0, streetlightIdEnd);
        let receivedData = codec.decode(msg.data);
        onDataCallback(undefined, TurnOn_1.default.unmarshal(receivedData), streetlightIdParam, msg);
    }))();
}
exports.jetStreamPull = jetStreamPull;
/**
 * Internal functionality to setup jetstream push subscription on the `streetlight/{streetlight_id}/command/turnon` channel
 *
 * @param onDataCallback to call when messages are received
 * @param nc to subscribe with
 * @param codec used to convert messages
 * @param streetlight_id parameter to use in topic
 * @param options to subscribe with, bindings from the AsyncAPI document overwrite these if specified
 */
function jetStreamPushSubscribe(onDataCallback, js, codec, streetlight_id, options) {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        try {
            let subscription = js.subscribe(`streetlight.${streetlight_id}.command.turnon`, options);
            (() => __awaiter(this, void 0, void 0, function* () {
                var e_2, _a;
                try {
                    for (var _b = __asyncValues(yield subscription), _c; _c = yield _b.next(), !_c.done;) {
                        const msg = _c.value;
                        const unmodifiedChannel = `streetlight.{streetlight_id}.command.turnon`;
                        let channel = msg.subject;
                        const streetlightIdSplit = unmodifiedChannel.split("{streetlight_id}");
                        const splits = [
                            streetlightIdSplit[0],
                            streetlightIdSplit[1]
                        ];
                        channel = channel.substring(splits[0].length);
                        const streetlightIdEnd = channel.indexOf(splits[1]);
                        const streetlightIdParam = "" + channel.substring(0, streetlightIdEnd);
                        let receivedData = codec.decode(msg.data);
                        onDataCallback(undefined, TurnOn_1.default.unmarshal(receivedData), streetlightIdParam);
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (_c && !_c.done && (_a = _b.return)) yield _a.call(_b);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
                console.log("subscription closed");
            }))();
            resolve(subscription);
        }
        catch (e) {
            reject(NatsTypescriptTemplateError_1.NatsTypescriptTemplateError.errorForCode(NatsTypescriptTemplateError_1.ErrorCode.INTERNAL_NATS_TS_ERROR, e));
        }
    }));
}
exports.jetStreamPushSubscribe = jetStreamPushSubscribe;
/**
 * Internal functionality to setup jetstream pull subscription on the `streetlight/{streetlight_id}/command/turnon` channel
 *
 * @param onDataCallback to call when messages are received
 * @param nc to subscribe with
 * @param codec used to convert messages
 * @param streetlight_id parameter to use in topic
 */
function jetStreamPullSubscribe(onDataCallback, js, codec, streetlight_id, options) {
    return new Promise((resolve, reject) => __awaiter(this, void 0, void 0, function* () {
        try {
            const subscription = yield js.pullSubscribe(`streetlight.${streetlight_id}.command.turnon`, options);
            (() => __awaiter(this, void 0, void 0, function* () {
                var e_3, _a;
                try {
                    for (var subscription_2 = __asyncValues(subscription), subscription_2_1; subscription_2_1 = yield subscription_2.next(), !subscription_2_1.done;) {
                        const msg = subscription_2_1.value;
                        const unmodifiedChannel = `streetlight.{streetlight_id}.command.turnon`;
                        let channel = msg.subject;
                        const streetlightIdSplit = unmodifiedChannel.split("{streetlight_id}");
                        const splits = [
                            streetlightIdSplit[0],
                            streetlightIdSplit[1]
                        ];
                        channel = channel.substring(splits[0].length);
                        const streetlightIdEnd = channel.indexOf(splits[1]);
                        const streetlightIdParam = "" + channel.substring(0, streetlightIdEnd);
                        let receivedData = codec.decode(msg.data);
                        onDataCallback(undefined, TurnOn_1.default.unmarshal(receivedData), streetlightIdParam);
                    }
                }
                catch (e_3_1) { e_3 = { error: e_3_1 }; }
                finally {
                    try {
                        if (subscription_2_1 && !subscription_2_1.done && (_a = subscription_2.return)) yield _a.call(subscription_2);
                    }
                    finally { if (e_3) throw e_3.error; }
                }
            }))();
            resolve(subscription);
        }
        catch (e) {
            reject(NatsTypescriptTemplateError_1.NatsTypescriptTemplateError.errorForCode(NatsTypescriptTemplateError_1.ErrorCode.INTERNAL_NATS_TS_ERROR, e));
        }
    }));
}
exports.jetStreamPullSubscribe = jetStreamPullSubscribe;
/**
 * Internal functionality to setup jetstrema fetch on the `streetlight/{streetlight_id}/command/turnon` channel
 *
 * @param onDataCallback to call when messages are received
 * @param js client to fetch with
 * @param codec used to convert messages
 * @param streetlight_id parameter to use in topic
 */
function jetsStreamFetch(onDataCallback, js, codec, streetlight_id, durable, options) {
    const stream = `streetlight.${streetlight_id}.command.turnon`;
    (() => __awaiter(this, void 0, void 0, function* () {
        var e_4, _a;
        let msgs = yield js.fetch(stream, durable, options);
        try {
            for (var msgs_1 = __asyncValues(msgs), msgs_1_1; msgs_1_1 = yield msgs_1.next(), !msgs_1_1.done;) {
                const msg = msgs_1_1.value;
                const unmodifiedChannel = `streetlight.{streetlight_id}.command.turnon`;
                let channel = msg.subject;
                const streetlightIdSplit = unmodifiedChannel.split("{streetlight_id}");
                const splits = [
                    streetlightIdSplit[0],
                    streetlightIdSplit[1]
                ];
                channel = channel.substring(splits[0].length);
                const streetlightIdEnd = channel.indexOf(splits[1]);
                const streetlightIdParam = "" + channel.substring(0, streetlightIdEnd);
                let receivedData = codec.decode(msg.data);
                onDataCallback(undefined, TurnOn_1.default.unmarshal(receivedData), streetlightIdParam, msg);
            }
        }
        catch (e_4_1) { e_4 = { error: e_4_1 }; }
        finally {
            try {
                if (msgs_1_1 && !msgs_1_1.done && (_a = msgs_1.return)) yield _a.call(msgs_1);
            }
            finally { if (e_4) throw e_4.error; }
        }
    }))();
}
exports.jetsStreamFetch = jetsStreamFetch;
//# sourceMappingURL=StreetlightStreetlightIdCommandTurnon.js.map