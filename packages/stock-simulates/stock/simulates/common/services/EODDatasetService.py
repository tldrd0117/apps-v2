import os
import glob
import pathlib
import stock.simulates.common.Constants as Constants
from datetime import datetime
from stock.datasources.envs import getEnv
import requests

import polars as pl

HISTORY_DTYPE = {
    "_id": pl.Utf8,
    "date": pl.Datetime,
    "open": pl.Float64,
    "high": pl.Float64,
    "low": pl.Float64,
    "close": pl.Float64,
    "adjClose": pl.Float64,
    "volume": pl.Int64,
    "unadjustedVolume": pl.Int64,
    "change": pl.Float64,
    "changePercent": pl.Float64,
    "vwap": pl.Float64,
    "label": pl.Utf8,
    "changeOverTime": pl.Float64,
    "symbol": pl.Utf8,
}


class EODDatasetService(object):
    def __init__(self) -> None:
        # by symbol, date
        self.symbolDateDictEntity = {}

        self.allEntities = pl.DataFrame(schema=HISTORY_DTYPE)
        self.loadedSymbols = []
        self.movingAverages = []
        self.yieldDates = []
        self.macds = []

    def getEntityLocal(self, symbol, entities=None):
        # baseDir = Constants.getBaseDir()
        # files = glob.glob(f"{baseDir}/symbol/history/*")
        # filenames = [pathlib.Path(file).name for file in files]
        if entities is None:
            entities = pl.DataFrame(schema=HISTORY_DTYPE)
        # for filename in filenames:
        #     data = filename.split("_")
        #     if len(data) < 3:
        #         continue
        #     fsymbol = data[0]
        #     if fsymbol == symbol:
        #         df = pl.read_parquet(f"{baseDir}/symbol/history/{filename}")
        #         df = df.cast(HISTORY_DTYPE)
        #         entities = pl.concat([entities, df])
        #         break
        return entities

    def getEntityRemote(self, symbol, entities=None):
        domain = getEnv("DOMAIN_ADDRESS")
        port = getEnv("FAST_API_STOCK_SERVER_PORT")
        url = f"{domain}:{port}/stock?symbol={symbol}&limit=99999"
        response = requests.get(url)
        print(response)
        if entities is None:
            entities = pl.DataFrame(schema=HISTORY_DTYPE)
        if response.status_code == 200:
            result = response.json()
            result = [
                {**item, "date": datetime.fromisoformat(item["date"])}
                for item in result
            ]
            entities = pl.DataFrame(result, schema=HISTORY_DTYPE)
            return entities

    def addMovingAverage(self, movingAverage):
        self.movingAverages.append(movingAverage)

    def addYieldDates(self, yieldDate):
        self.yieldDates.append(yieldDate)

    def addMACD(self, minSMA, maxSMA):
        self.macds.append([minSMA, maxSMA])

    def makePartition(self, entities):
        self.symbolDateDictEntity = entities.partition_by(
            "symbol", "date", as_dict=True
        )

    def mergeEntities(self, symbol, entities):
        if symbol in self.loadedSymbols:
            return self.allEntities
        self.allEntities = pl.concat([self.allEntities, entities], how="diagonal")
        self.loadedSymbols.append(symbol)
        return self.allEntities

    def applyMACD(self, entities: pl.DataFrame):
        def calculateMACD(symbol, currentDate, minSMA, maxSMA):
            mapping = entities.filter(
                (pl.col("date") == pl.col("date").dt.month_end())
                | (pl.col("date") == currentDate)
            )
            mapping = mapping.with_columns(
                pl.col("adjClose")
                .rolling_mean(window_size=f"{minSMA}_saturating", by="date")
                .alias(f"{minSMA}MovingAverage")
            )
            mapping = mapping.with_columns(
                pl.col("adjClose")
                .rolling_mean(window_size=f"{maxSMA}_saturating", by="date")
                .alias(f"{maxSMA}MovingAverage")
            )
            mapping = mapping.with_columns(
                (
                    pl.col(f"{minSMA}MovingAverage") - pl.col(f"{maxSMA}MovingAverage")
                ).alias(f"{minSMA}_{maxSMA}_MACD")
            )
            return mapping.filter(pl.col("date") == currentDate)[
                f"{minSMA}_{maxSMA}_MACD"
            ][0]

        for macd in self.macds:
            minSMA = macd[0]
            maxSMA = macd[1]
            entities = entities.with_columns(
                pl.struct(
                    [
                        pl.col("symbol"),
                        pl.col("date"),
                    ]
                )
                .apply(lambda x: calculateMACD(x["symbol"], x["date"], minSMA, maxSMA))
                .alias(f"{minSMA}_{maxSMA}_MACD")
            )
        return entities

    def applyMovingAverage(self, entities: pl.DataFrame):
        for movingAverage in self.movingAverages:
            entities = entities.with_columns(
                pl.col("adjClose")
                .rolling_mean(window_size=f"{movingAverage}_saturating", by="date")
                .alias(f"{movingAverage}MovingAverage")
            )
        return entities

    def applyMovingAverageByLastValue(self, entities: pl.DataFrame):
        def calculateMovingAverageByLastValue(symbol, currentDate, movingAverage):
            mapping = entities.filter(
                (pl.col("date") == pl.col("date").dt.month_end())
                | (pl.col("date") == currentDate)
            )
            mapping = mapping.with_columns(
                pl.col("adjClose")
                .rolling_mean(window_size=f"{movingAverage}_saturating", by="date")
                .alias(f"{movingAverage}MovingAverage")
            )
            return mapping.filter(pl.col("date") == currentDate)[
                f"{movingAverage}MovingAverage"
            ][0]

        for movingAverage in self.movingAverages:
            entities = entities.with_columns(
                pl.struct(
                    [
                        pl.col("symbol"),
                        pl.col("date"),
                    ]
                )
                .apply(
                    lambda x: calculateMovingAverageByLastValue(
                        x["symbol"], x["date"], movingAverage
                    )
                )
                .alias(f"{movingAverage}MovingAverage")
            )

        return entities

    def applyDateDiffClose(self, entities: pl.DataFrame):
        def test(symbol, date, diffDate):
            if (symbol, diffDate) not in self.symbolDateDictEntity:
                return None
            curClose = self.symbolDateDictEntity[(symbol, date)]["adjClose"][0]
            beforeClose = self.symbolDateDictEntity[(symbol, diffDate)]["adjClose"][0]
            return (
                (curClose - beforeClose) / beforeClose * 100
            )  # (현재가격 - 이전가격) / 이전가격 * 100

        for yieldDate in self.yieldDates:
            entities = entities.with_columns(
                pl.struct(
                    [
                        pl.col("symbol"),
                        pl.col("date"),
                        pl.col("date")
                        .dt.offset_by(f"-{yieldDate}_saturating")
                        .alias("diffDate"),
                    ]
                )
                .apply(lambda x: test(x["symbol"], x["date"], x["diffDate"]))
                .alias(f"{yieldDate}Yield"),
            )
        return entities

    def getEntity(self, symbol, ignoreTime=True):
        entities = self.getEntityLocal(symbol)
        if entities.shape[0] == 0:
            entities = self.getEntityRemote(symbol)
        entities = entities.unique("date")
        entities = entities.sort("date")
        if ignoreTime:
            entities = entities.with_columns(
                pl.col("date").dt.truncate("1d").alias("date")
            )
        entities = entities.set_sorted("date", descending=False)
        entities = entities.upsample(time_column="date", every="1d", by="symbol")
        entities = entities.fill_null(strategy="forward")
        self.makePartition(entities)
        entities = self.applyMovingAverageByLastValue(entities)
        entities = self.applyMACD(entities)
        entities = self.applyDateDiffClose(entities)
        entities = self.mergeEntities(symbol, entities)
        self.makePartition(entities)
        print("getEntity", symbol)
        print(entities.shape[0])
        self.allEntities = entities
        return entities

    def getEntityByDate(self, symbol, startDate, endDate):
        entities = self.allEntities.filter(
            (pl.col("date") >= startDate)
            & (pl.col("date") <= endDate)
            & (pl.col("symbol") == symbol)
        )
        return entities

    def getAllEntities(self):
        return self.allEntities

    def getEntitySymbolDateDict(self):
        return self.symbolDateDictEntity
