from stock.simulates.common.services.EODDatasetService import EODDatasetService
import polars as pl
from datetime import datetime


def test_getEntityLocal():
    local: pl.DataFrame = EODDatasetService().getEntityLocal("AAPL", None)
    print(local.shape)


def test_getEntityRemote():
    remote: pl.DataFrame = EODDatasetService().getEntityRemote("AAPL", None)
    print(remote)


def test_getEntity():
    result = EODDatasetService().getEntity("AAPL")
    print(result)
    # result = result.upsample(time_column="date", every="1d", by="symbol")
    # result = result.fill_null(strategy="forward")
    # print(result)


def test_mapping():
    service = EODDatasetService()
    service.addMovingAverage("12mo")
    entity = service.getEntity("AAPL")
    print(entity)


def test_rollingmean():
    df = pl.DataFrame(
        {
            "date": [
                datetime(2021, 1, 1),
                datetime(2021, 1, 2),
                datetime(2021, 1, 3),
                datetime(2021, 1, 4),
                datetime(2021, 1, 5),
            ],
            "a": [1, 2, 3, 4, 5],
        }
    )
    print(df)
    print(df.with_columns(pl.col("a").rolling_mean("2d", closed="right", by="date")))


def test_getSymbolDateDictEntity():
    service = EODDatasetService()
    service.getEntity("AAPL")
    obj = service.getEntitySymbolDateDict()
    # print(obj[("AAPL", datetime(2017, 1, 3))])
    # print(obj[("AAPL", datetime(2017, 1, 3))]["close"][0])
    # service.getAllEntities().partition_by(
    #         "symbol", "date", as_dict=True
    #     )
    # print(

    # )


def test_getAllEntitiesWithMovingAverage():
    service = EODDatasetService()
    service.addMovingAverage("1y")
    service.addMovingAverage("3y")
    service.addMovingAverage("1mo")
    service.getEntity("AAPL")
    df = service.getAllEntities()
    print(df["3yMovingAverage"])
    print(df["1yMovingAverage"])
    print(df["1moMovingAverage"])


def test_getAllEntitiesWithDateDiff():
    service = EODDatasetService()
    service.addYieldDates("1mo")
    service.getEntity("AAPL")
    df = service.getAllEntities()
    print(df["1moYield"])


def test_DBC():
    service = EODDatasetService()
    # service.addMovingAverage("1y")
    # service.getEntity("BIL")
    # service.getEntity("AAPL")
    # service.getEntity(EntityType.USA_STOCK, "QLD")
    # print(df["1yMovingAverage"])


def test_BTC():
    service = EODDatasetService()
    # service.getEntity(EntityType.CRYPTO, "BTCUSDT")
    # print(df["1yMovingAverage"])
