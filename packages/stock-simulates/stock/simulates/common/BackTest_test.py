from stock.simulates.common.BackTest import (
    BackTest,
    Portfolio,
    RebalanceOrders,
    ShareHolding,
    Schedule,
    Interval,
    RebalanceOrder,
)
from stock.simulates.common.services.EODDatasetService import EODDatasetService
from datetime import datetime


class TestBackTest(BackTest):
    def initalize(self):
        self.portfolioPath = (
            "packages/stock-simulates/stock/simulates/common/portfolios"
        )
        self.setStartDate("20080101")
        self.setEndDate("20200110")
        self.addEntity("AAPL")
        self.addEntity("BIL")
        self.setBalance(10000)
        self.addPortfolio("testPortfolio", 1.0)
        self.addSchedules(
            Schedule(
                name="testSchedule",
                startDate=self.startDate,
                endDate=self.endDate,
                interval=Interval(day=1),
            )
        )

    def onSchedule(self, name: str, date: datetime):
        print("onSchedule", date)
        self.rebalanceByPercent(
            "testPortfolio",
            RebalanceOrders(
                RebalanceOrder("AAPL", 0.5),
                RebalanceOrder("BIL", 0.5),
            ),
        )

    def onScheduleEndOfMarket(self, name: str, date: datetime):
        print("portFolio", self.getPortfolio("testPortfolio"))


def test_backtest():
    TestBackTest().run()
