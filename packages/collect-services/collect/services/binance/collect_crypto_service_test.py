import time
from stock.datasource.mongoClient import getCollection
from datetime import datetime, timedelta
import collect.services.binance.collect_crypto_service as service


def test_getCryptoPriceAll():
    service.saveCryptoPrice()


def test_getCryptoPriceByDate():
    date = service.getLastHistoryDate()
    service.saveCryptoPriceByDate(date + timedelta(days=1))


def test_deletePrice():
    date = datetime(2024, 3, 4)
    service.deleteCryptoPrice(date)


def test_hello():
    print("hello")
