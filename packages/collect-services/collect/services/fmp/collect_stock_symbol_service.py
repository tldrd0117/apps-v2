import collect.apis.fmp as apis
from stock.datasources.mongoClient import getCollection


def saveStockSymbols():
    json = apis.getSymbolList()
    getCollection("fmp_stock_symbols").drop()
    return getCollection("fmp_stock_symbols").insert_many(json)


def saveETFStockSymbols():
    json = apis.getETFSymbolList()
    getCollection("fmp_etf_symbols").drop()
    return getCollection("fmp_etf_symbols").insert_many(json)


def saveTradableStockSymbols():
    json = apis.getTradableSymbolList()
    getCollection("fmp_tradable_symbols").drop()
    return getCollection("fmp_tradable_symbols").insert_many(json)


def getTradableStockExchanges():
    collection = getCollection("fmp_tradable_symbols")
    return collection.find({}, {"exchangeShortName": 1}).distinct("exchangeShortName")


def getUSATradableStockSymbols():
    collection = getCollection("fmp_tradable_symbols")
    usaMarketNames = ["NASDAQ", "NYSE", "AMEX"]
    return collection.find(
        {"$or": [{"exchangeShortName": name} for name in usaMarketNames]}
    )


def getUSAStockSymbols():
    collection = getCollection("fmp_stock_symbols")
    usaMarketNames = ["NASDAQ", "NYSE", "AMEX"]
    return collection.find(
        {"$or": [{"exchangeShortName": name} for name in usaMarketNames]}
    )


def saveFinancialStatementsStockSymbols():
    json = apis.getFinancialStatementsSymbolList()
    getCollection("fmp_financial_statements_symbols").drop()
    symbols = list(map(lambda x: {"symbol": x}, json))
    return getCollection("fmp_financial_statements_symbols").insert_many(symbols)


def saveAvailableIndexesSymbols():
    json = apis.getAvailableIndexesSymbolList()
    getCollection("fmp_available_indexes_symbols").drop()
    return getCollection("fmp_available_indexes_symbols").insert_many(json)
