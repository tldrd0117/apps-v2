import collect.services.fmp.collect_stock_financial_service as service


def test_saveTradableIncomeStatementsAnnual():
    service.saveTradableIncomeStatementsAnnual()


def test_saveTradableIncomeStatementsQuater():
    service.saveTradableIncomeStatementsQuater()


def test_saveTradableBalanceSheets():
    service.saveTradableBalanceSheetsQuater()


def test_saveTradableCashFlowStatements():
    service.saveTradableCashFlowStatementsAnnual()


def test_saveTradableCashFlowStatementsQuater():
    service.saveTradableCashFlowStatementsQuater()


def test_saveIncomeStatementsAnnual():
    service.saveIncomeStatementsAnnual()


def test_saveIncomeStatementsQuater():
    service.saveIncomeStatementsQuater()


def test_saveBalanceSheetsAnnual():
    service.saveBalanceSheetsAnnual()


def test_saveBalanceSheetsQuater():
    service.saveBalanceSheetsQuater()


def test_saveCashFlowStatementsAnnual():
    service.saveCashFlowStatementsAnnual()


def test_saveCashFlowStatementsQuater():
    service.saveCashFlowStatementsQuater()
