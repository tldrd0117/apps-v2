from .collect_batch_eod_prices_service import *


def test_getBatchEODPrice():
    data = getBatchEODPrice("2021-01-03")
    print(data)


def test_saveBatchEODPrice():
    saveBatchEODPrice("1974-01-03")


def test_saveBatchEODPrices():
    saveBatchEODPrices("19740103", "19740110")


def test_getLatestPriceDate():
    date = getLatestPriceDate("19740102", "19740112")
    print(date)
