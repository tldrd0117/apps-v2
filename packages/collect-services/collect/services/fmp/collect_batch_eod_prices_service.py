from stock.datasources.mongoClient import getCollection
import common.utils.dateUtils as dateUtils
import collect.apis.fmp as apis
import polars as pl
import time


def saveBatchEODPrices(startDatetime, endDatetime=dateUtils.getTodayDatetimeStr()):
    """
    지정된 시작 날짜와 종료 날짜 사이의 모든 날짜에 대해 일별 종가 데이터를 저장합니다.

    Parameters
    ----------
    startDatetime : str
        데이터 수집을 시작할 날짜.
        example: "20210101"
    endDatetime : str
        데이터 수집을 종료할 날짜.
        example: "20210110"
    """
    startDatetime = getLatestPriceDate(startDatetime, endDatetime)
    startDatetime = dateUtils.format(startDatetime, "%Y-%m-%d", "%Y%m%d")
    for date in dateUtils.getDateRangeStr(startDatetime, endDatetime):
        print(f"saveBatchEODPrice({date}) start")
        saveBatchEODPrice(date)


def convertDownloadedDataToJson(byteData):
    """
    다운로드된 바이트 데이터를 JSON 형식으로 변환합니다.

    Parameters
    ----------
    byteData : bytes
        CSV 형식의 바이트 데이터.

    Returns
    -------
    list of dict
        변환된 데이터를 담고 있는 사전 객체의 리스트.
    """
    df = pl.read_csv(byteData, infer_schema_length=0)
    return df.to_dicts()


def getBatchEODPrice(date):
    """
    지정된 날짜에 대한 일별 종가 데이터를 가져옵니다.

    Parameters
    ----------
    date : str
        데이터를 조회할 날짜.

    Returns
    -------
    list of dict or None
        조회된 데이터. 데이터가 없거나 조회에 실패한 경우 None을 반환.
    """

    byteData = apis.batchEODPrices(date)
    if byteData is None or len(byteData) == 0:
        return None
    return convertDownloadedDataToJson(byteData)


def saveBatchEODPrice(date, isReset=False):
    """
    지정된 날짜에 대한 일별 종가 데이터를 데이터베이스에 저장합니다.

    Parameters
    ----------
    date : str
        저장할 데이터의 날짜.
    """
    collection = getCollection("fmp_batch_eod_prices")
    isExist = checkExistPriceData(date)
    if isExist:
        print(f"{date} is already exist!")
    if isExist and isReset:
        collection.delete_many({"date": date})
        print(f"{date} is deleted!")
    elif not isExist:
        data = getBatchEODPrice(date)
        if data is not None:
            print(f"saveBatchEODPrice({date}) insert")
            collection.insert_many(data)
            print(f"saveBatchEODPrice({date}) is done!")
        else:
            print(f"saveBatchEODPrice({date}) failed!")


def checkExistPriceData(date):
    """
    지정된 날짜에 대한 가격 데이터가 데이터베이스에 존재하는지 확인합니다.

    Parameters
    ----------
    date : str
        확인하고자 하는 데이터의 날짜. 'YYYY-MM-DD' 형식의 문자열이어야 합니다.

    Returns
    -------
    bool
        지정된 날짜의 가격 데이터가 데이터베이스에 존재하면 True, 그렇지 않으면 False를 반환합니다.
    """
    collection = getCollection("fmp_batch_eod_prices")
    return collection.count_documents({"date": date}) > 0


def getLatestPriceDate(startDatetime, endDatetime):
    """
    데이터베이스에서 가장 최근의 가격 데이터 날짜를 조회합니다.

    이 함수는 'fmp_batch_eod_prices' 컬렉션을 조회하여 저장된 가격 데이터 중
    가장 최근 날짜를 찾아 반환합니다. 데이터가 없는 경우 None을 반환할 수 있습니다.

    Returns
    -------
    str or None
        데이터베이스에 저장된 가장 최근의 가격 데이터 날짜('YYYY-MM-DD' 형식)를 문자열로 반환합니다.
        데이터가 없는 경우 None을 반환합니다.
    """
    collection = getCollection("fmp_batch_eod_prices")
    cursor = (
        collection.find(
            {
                "date": {
                    "$gte": dateUtils.format(startDatetime),
                    "$lte": dateUtils.format(endDatetime),
                }
            },
            {"date": 1},
        )
        .sort("date", -1)
        .limit(1)
    )
    result = list(cursor)
    return result[0]["date"] if len(result) > 0 else None
