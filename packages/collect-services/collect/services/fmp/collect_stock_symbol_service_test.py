import collect.services.fmp.collect_stock_symbol_service as service


def test_saveStockSymbols():
    service.saveStockSymbols()


def test_saveETFStockSymbols():
    service.saveETFStockSymbols()


def test_saveTradableStockSymbols():
    service.saveTradableStockSymbols()


def test_saveFinancialStatementsStockSymbols():
    service.saveFinancialStatementsStockSymbols()


def test_saveAvailableIndexesSymbols():
    service.saveAvailableIndexesSymbols()


def test_getTradableStockExchanges():
    exchanges = service.getTradableStockExchanges()
    print(exchanges)


def test_getUsaTradableStockSymbols():
    symbols = service.getUSATradableStockSymbols()
    symbols = list(symbols)
    print(symbols)
    print(len(symbols))
