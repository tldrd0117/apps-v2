// This file is auto-generated by @hey-api/openapi-ts

export type EndOfDayPrice = {
    symbol?: string;
    date?: string;
    open?: string;
    high?: string;
    low?: string;
    close?: string;
    adjClose?: string;
    volume?: string;
};

export type GetDummyResponse = EndOfDayPrice;

export type $OpenApiTs = {
    '/dummy': {
        get: {
            res: {
                /**
                 * OK
                 */
                200: EndOfDayPrice;
            };
        };
    };
};