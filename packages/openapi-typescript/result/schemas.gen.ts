// This file is auto-generated by @hey-api/openapi-ts

export const $EndOfDayPrice = {
    type: 'object',
    properties: {
        symbol: {
            type: 'string'
        },
        date: {
            type: 'string',
            format: 'date-time'
        },
        open: {
            type: 'string'
        },
        high: {
            type: 'string'
        },
        low: {
            type: 'string'
        },
        close: {
            type: 'string'
        },
        adjClose: {
            type: 'string'
        },
        volume: {
            type: 'string'
        }
    }
} as const;