from datetime import datetime, timedelta, date
from dateutil.relativedelta import relativedelta


def getDateRange(startDate, endDate):
    if type(startDate) == str:
        startDate = datetime.strptime(startDate, "%Y%m%d")
    if type(endDate) == str:
        endDate = datetime.strptime(endDate, "%Y%m%d")
    dateRange = []
    while startDate <= endDate:
        dateRange.append(startDate)
        startDate = startDate + timedelta(days=1)
    return dateRange


def format(date, orgFormat="%Y%m%d", format="%Y-%m-%d"):
    date = datetime.strptime(date, orgFormat)
    return date.strftime(format)


def getDateRangeStr(startDate, endDate, format="%Y-%m-%d"):
    if type(startDate) == str:
        startDate = datetime.strptime(startDate, "%Y%m%d")
    if type(endDate) == str:
        endDate = datetime.strptime(endDate, "%Y%m%d")
    dateRange = []
    while startDate <= endDate:
        dateRange.append(startDate.strftime(format))
        startDate = startDate + timedelta(days=1)
    return dateRange


def getDateMonthRange(startDate, endDate):
    if type(startDate) == str:
        startDate = datetime.strptime(startDate, "%Y%m%d")
    if type(endDate) == str:
        endDate = datetime.strptime(endDate, "%Y%m%d")
    dateRange = []
    while startDate <= endDate:
        dateRange.append(startDate)
        startDate = startDate + relativedelta(months=1)
    return dateRange


def convertNumpyDtToDt(numpyDatetime):
    return datetime.utcfromtimestamp(numpyDatetime.astype(int) * 1e-9)


def getTodayDatetimeStr():
    return getTodayDatetime().strftime("%Y-%m-%d")


def getTodayDatetime():
    today = date.today()
    today_with_time = datetime(
        year=today.year,
        month=today.month,
        day=today.day,
    )
    return today_with_time


def getTodayDatetimeByHour(hour):
    today = date.today()
    today_with_time = datetime(
        year=today.year, month=today.month, day=today.day, hour=hour
    )
    return today_with_time


def convertDateToDatetime(today):
    today_with_time = datetime(
        year=today.year,
        month=today.month,
        day=today.day,
    )
    return today_with_time
