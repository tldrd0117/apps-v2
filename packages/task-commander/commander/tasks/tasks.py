from typing import Optional
from collect.services.fmp import *
import typer
from simulate.backtests import *
from commander.app import getApp
from typing_extensions import Annotated
from stock.datasources.envs import getConfig

app = getApp()


@app.command("runTestRunBackTest")
def runTestRunBackTest():
    backTest = TestRunBackTest()
    backTest.run()


@app.command("collectBatchEODPrices")
def collectBatchEODPrices(
    start_datetime,
    end_datetime: Annotated[Optional[str], typer.Argument()] = datetime.now().strftime(
        "%Y%m%d"
    ),
):
    saveBatchEODPrices(start_datetime, end_datetime)


@app.command()
def hello(name: Annotated[str, typer.Argument()]):
    print(f"Hello, {name}")


@app.command()
def environment():
    print(getConfig())
