# SeibroStockNumEvent


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**발행일** | **datetime** |  | [optional] 
**기업명** | **str** |  | [optional] 
**주식종류** | **str** |  | [optional] 
**발행형태** | **str** |  | [optional] 
**횟수** | **float** |  | [optional] 
**발행사유** | **str** |  | [optional] 
**액면가** | **float** |  | [optional] 
**주당발행가** | **float** |  | [optional] 
**상장일** | **datetime** |  | [optional] 
**발행주식수** | **float** |  | [optional] 
**종목코드** | **str** |  | [optional] 
**종목명** | **str** |  | [optional] 
**create_at** | **datetime** |  | [optional] 

## Example

```python
from stock.models.seibro_stock_num_event import SeibroStockNumEvent

# TODO update the JSON string below
json = "{}"
# create an instance of SeibroStockNumEvent from a JSON string
seibro_stock_num_event_instance = SeibroStockNumEvent.from_json(json)
# print the JSON string representation of the object
print SeibroStockNumEvent.to_json()

# convert the object into a dict
seibro_stock_num_event_dict = seibro_stock_num_event_instance.to_dict()
# create an instance of SeibroStockNumEvent from a dict
seibro_stock_num_event_form_dict = seibro_stock_num_event.from_dict(seibro_stock_num_event_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


