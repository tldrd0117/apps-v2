# KrxStock


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**종목코드** | **str** |  | [optional] 
**종목명** | **str** |  | [optional] 
**소속부** | **str** |  | [optional] 
**종가** | **float** |  | [optional] 
**대비** | **float** |  | [optional] 
**등락률** | **float** |  | [optional] 
**시가** | **float** |  | [optional] 
**고가** | **float** |  | [optional] 
**저가** | **float** |  | [optional] 
**거래량** | **float** |  | [optional] 
**거래대금** | **float** |  | [optional] 
**시가총액** | **float** |  | [optional] 
**상장주식수** | **float** |  | [optional] 
**var_date** | **datetime** |  | [optional] 
**market** | **str** |  | [optional] 

## Example

```python
from stock.models.krx_stock import KrxStock

# TODO update the JSON string below
json = "{}"
# create an instance of KrxStock from a JSON string
krx_stock_instance = KrxStock.from_json(json)
# print the JSON string representation of the object
print KrxStock.to_json()

# convert the object into a dict
krx_stock_dict = krx_stock_instance.to_dict()
# create an instance of KrxStock from a dict
krx_stock_form_dict = krx_stock.from_dict(krx_stock_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


