# EndOfDayPrice


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**symbol** | **str** |  | [optional] 
**var_date** | **datetime** |  | [optional] 
**open** | **str** |  | [optional] 
**high** | **str** |  | [optional] 
**low** | **str** |  | [optional] 
**close** | **str** |  | [optional] 
**adj_close** | **str** |  | [optional] 
**volume** | **str** |  | [optional] 

## Example

```python
from stock.models.end_of_day_price import EndOfDayPrice

# TODO update the JSON string below
json = "{}"
# create an instance of EndOfDayPrice from a JSON string
end_of_day_price_instance = EndOfDayPrice.from_json(json)
# print the JSON string representation of the object
print EndOfDayPrice.to_json()

# convert the object into a dict
end_of_day_price_dict = end_of_day_price_instance.to_dict()
# create an instance of EndOfDayPrice from a dict
end_of_day_price_form_dict = end_of_day_price.from_dict(end_of_day_price_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


