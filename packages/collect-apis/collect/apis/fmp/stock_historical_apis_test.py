import collect.apis.fmp as apis


def test_getIntradayChart():
    json = apis.getIntradayChart("AAPL", "5min", "2021-01-01", "2021-01-10")
    assert len(json) > 0


def test_getDailyChartEOD():
    json = apis.getDailyChartEOD("AAPL", "1900-01-01", "2200-01-01")
    assert json["symbol"] == "AAPL"
    assert len(json["historical"]) > 0
    print(json)


def test_batchPrice():
    contents = apis.batchEODPrice("1962-01-01")
    with open("resources/collect/test.csv", "wb") as f:
        f.write(contents)
