from collect.apis.fmp.common import request


def getIncomeStatements(symbol, period="annual"):
    params = {
        "period": period,
    }
    json = request(
        f"https://financialmodelingprep.com/api/v3/income-statement/{symbol}",
        params=params,
    )
    return json


def getBalanceSheetsStatements(symbol, period="annual"):
    params = {
        "period": period,
    }
    json = request(
        f"https://financialmodelingprep.com/api/v3/balance-sheet-statement/{symbol}",
        params=params,
    )
    return json


def getCashFlowStatements(symbol, period="annual"):
    params = {
        "period": period,
    }
    json = request(
        f"https://financialmodelingprep.com/api/v3/cash-flow-statement/{symbol}",
        params=params,
    )
    return json
