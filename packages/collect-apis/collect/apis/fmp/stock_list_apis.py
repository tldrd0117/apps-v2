from .common import request


# stock symbol list
def getSymbolList():
    json = request("https://financialmodelingprep.com/api/v3/stock/list")
    return json


# etf symbol list
def getETFSymbolList():
    json = request("https://financialmodelingprep.com/api/v3/etf/list")
    return json


# financial statement symbol list
def getFinancialStatementsSymbolList():
    json = request(
        "https://financialmodelingprep.com/api/v3/financial-statement-symbol-lists"
    )
    return json


# available traded symbol list
def getTradableSymbolList():
    json = request("https://financialmodelingprep.com/api/v3/available-traded/list")
    return json


def getAvailableIndexesSymbolList():
    json = request("https://financialmodelingprep.com/api/v3/symbol/available-indexes")
    return json
