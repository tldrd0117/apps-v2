import collect.apis.fmp.bulk_and_batch_apis as apis
import polars as pl


def test_batchEODPrices():
    print("start@@")
    data = apis.batchEODPrices("2022-03-04")
    assert len(data) > 0
    table = pl.read_csv(data, infer_schema_length=0)
    print(table)
