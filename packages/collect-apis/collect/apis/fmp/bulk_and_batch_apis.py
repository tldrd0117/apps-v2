from .common import request, download


def batchEODPrices(date):
    params = {
        "date": date,
    }
    json = download(
        f"https://financialmodelingprep.com/api/v4/batch-request-end-of-day-prices",
        params=params,
    )
    return json
