import collect.apis.fmp.financial_statements_apis as apis


def test_getIncomeStatements():
    json = apis.getIncomeStatements("AAPL", "annual")
    assert len(json) > 0
    print(json)


def test_getBalanceSheetsStatements():
    json = apis.getBalanceSheetsStatements("AAPL", "annual")
    assert len(json) > 0


def test_getCashFlowStatements():
    json = apis.getCashFlowStatements("AAPL", "annual")
    assert len(json) > 0
