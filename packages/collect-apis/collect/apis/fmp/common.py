import requests
from stock.datasources.envs import getEnv
from tenacity import retry, stop_after_attempt, wait_fixed
import polars as pl


@retry(
    stop=stop_after_attempt(60), wait=wait_fixed(1), reraise=True
)  # 3초 간격으로 20번 재시도
def request(url, params=None):
    if params is None:
        params = {
            "apikey": getEnv("FMP_API_KEY"),
        }
    else:
        params["apikey"] = getEnv("FMP_API_KEY")
    return requests.get(url, params=params).json()


@retry(
    stop=stop_after_attempt(60), wait=wait_fixed(3), reraise=True
)  # 3초 간격으로 20번 재시도
def download(url, params=None):
    if params is None:
        params = {
            "apikey": getEnv("FMP_API_KEY"),
        }
    else:
        params["apikey"] = getEnv("FMP_API_KEY")
    response = requests.get(url, params=params, stream=True)
    response.raise_for_status()
    return response.content


def requestCrypto(url, params=None):
    return requests.get(url, params=params).json()
