import collect.apis as apis
from datetime import datetime
import json
import common.utils.dateUtils as dateUtils


def test_getBTCCryptoPrice():
    date = datetime(1980, 1, 1)
    while date < dateUtils.getTodayDatetimeByHour(9):
        json = apis.getCryptoPrice(startTime=int(date.timestamp() * 1e3))
        for data in json:
            date = datetime.fromtimestamp(data[0] / 1e3)
            print(date)


def test_getExchangeInfo():
    res = apis.getExchangeInfo()
    symbols = [item["symbol"] for item in res["symbols"]]
    symbols.sort()
    print(symbols)
