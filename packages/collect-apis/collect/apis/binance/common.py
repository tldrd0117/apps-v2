import requests
from tenacity import retry, stop_after_attempt, wait_fixed


@retry(
    stop=stop_after_attempt(60), wait=wait_fixed(1), reraise=True
)  # 3초 간격으로 20번 재시도
def request(url, params=None):
    return requests.get(url, params=params).json()
