declare class TurnOn {
    private _lumen;
    private _additionalProperties?;
    constructor(input: {
        lumen: number;
        additionalProperties?: Map<string, any>;
    });
    get lumen(): number;
    set lumen(lumen: number);
    get additionalProperties(): Map<string, any> | undefined;
    set additionalProperties(additionalProperties: Map<string, any> | undefined);
    marshal(): string;
    static unmarshal(json: string | object): TurnOn;
}
export default TurnOn;
