
echo "setup start"
echo "base setup"

directory="/Users/iseongjae/Documents/deploy/apps-v2/"

mkdir -p "$directory/apps"
mkdir -p "$directory/apps/database/data"
mkdir -p "$directory/apps/webdriver/downloads"

cd "$directory"
echo "directory create"

source /etc/profile
source /Users/iseongjae/.zprofile
source /Users/iseongjae/.zshrc

echo "profile .zshrc .zprofile exec"

pyenv virtualenv 3.9.7 collect
eval "pyenv activate collect"

curl -sSL https://install.python-poetry.org | python3 -
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
eval 'export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm'
nvm install 18.17.0
nvm use 18.17.0
node -v
npm -v
npm install -g pnpm
pnpm -v
echo "node install complete"

echo $PNPM_HOME
yes | pnpm prune
yes | pnpm install
pnpm setup

echo "logsite setup"

envsubst < $directory/apps/containers/namespaces.yaml | kubectl apply -f -
kubectl config set-context --current --namespace=log-site

envsubst < $directory/apps/containers/mongodb-oper.yaml | kubectl delete -f -
envsubst < $directory/apps/containers/mongodb-express.yaml | kubectl delete -f -
envsubst < $directory/apps/containers/webdriver.yaml | kubectl delete -f -

./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/mongodb-oper.yaml | kubectl apply -f -
./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/mongodb-express.yaml | kubectl apply -f -
./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/webdriver.yaml | kubectl apply -f -

echo "airflow setup"

source $directory/apps-v2/utils/process.sh
result=$(killProcessByPort 30052)
FILE=$directory/apps/task-airflow/get_helm.sh
if test -f "$FILE"; then
    echo "$FILE exists."
else
    curl -fsSL -o $FILE https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    chmod 700 $directory/apps/task-airflow/get_helm.sh
    sudo ./$directory/task-airflow/get_helm.sh
fi
helm uninstall airflow -n airflow
kubectl config set-context --current --namespace=airflow

envsubst < $directory/apps/containers/airflow-pvc.yaml | kubectl delete -f -
envsubst < $directory/apps/containers/airflow-pv.yaml | kubectl delete  -f -
envsubst < $directory/apps/containers/airflow.yaml | kubectl delete  -f -

./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/airflow-pv.yaml | kubectl apply -f -
./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/airflow-pvc.yaml | kubectl apply  -f -
./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/airflow.yaml | kubectl apply  -f -
helm repo add apache-airflow https://airflow.apache.org
helm upgrade --install airflow apache-airflow/airflow -f $directory/apps/task-airflow/values.yaml --namespace airflow --create-namespace

echo "setup complete"
