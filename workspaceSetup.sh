
echo "setup start"
echo "base setup"

directory="/home/tldrd/projects/apps-v2/"

mkdir -p "$directory/apps"
mkdir -p "$directory/apps/database/data"
mkdir -p "$directory/apps/webdriver/downloads"

cd "$directory"
echo "directory create"

curl https://pyenv.run | bash
echo "pyenv download"
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
echo "pyenv path update"
echo $ADMIN_PASSWORD | sudo -S apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
xz-utils tk-dev libffi-dev liblzma-dev python-openssl git
echo "pyenv path update"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
echo "pyenv start"
source /home/tldrd/.bashrc
echo ".bashrc exec"

pyenv install 3.9.7
pyenv virtualenv 3.9.7 collect
eval "pyenv activate collect"
curl -sSL https://install.python-poetry.org | python3 -

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
eval 'export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm'
nvm install 18.17.0
nvm use 18.17.0
node -v
npm -v
npm install -g pnpm
pnpm -v
source /home/tldrd/.bashrc
echo "node install complete"

echo $PNPM_HOME
yes | pnpm prune
yes | pnpm install --prod
pnpm setup
source /home/tldrd/.bashrc
pnpm add turbo

echo "logsite setup"

envsubst < $directory/apps/containers/namespaces.yaml | kubectl apply -f -
kubectl config set-context --current --namespace=log-site

envsubst < $directory/apps/containers/mongodb-oper.yaml | kubectl delete -f -
envsubst < $directory/apps/containers/mongodb-express.yaml | kubectl delete -f -
envsubst < $directory/apps/containers/webdriver.yaml | kubectl delete -f -

./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/mongodb-oper.yaml | kubectl apply -f -
./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/mongodb-express.yaml | kubectl apply -f -
./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/webdriver.yaml | kubectl apply -f -

echo "airflow setup"

source '$directory/apps-v2/utils/process.sh'
result=$(killProcessByPort 30052)
FILE=/home/tldrd/projects/apps-v2/apps/task-airflow/get_helm.sh
if test -f "$FILE"; then
    echo "$FILE exists."
else
    curl -fsSL -o $FILE https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
    chmod 700 $directory/apps/task-airflow/get_helm.sh
    sudo ./$directory/task-airflow/get_helm.sh
fi
helm uninstall airflow -n airflow
kubectl config set-context --current --namespace=airflow

envsubst < $directory/apps/containers/airflow-pvc.yaml | kubectl delete -f -
envsubst < $directory/apps/containers/airflow-pv.yaml | kubectl delete  -f -
envsubst < $directory/apps/containers/airflow.yaml | kubectl delete  -f -

./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/airflow-pv.yaml | kubectl apply -f -
./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/airflow-pvc.yaml | kubectl apply  -f -
./node_modules/.bin/dotenv -- envsubst < $directory/apps/containers/airflow.yaml | kubectl apply  -f -
helm repo add apache-airflow https://airflow.apache.org
helm upgrade --install airflow apache-airflow/airflow -f $directory/apps/task-airflow/values.yaml --namespace airflow --create-namespace --debug

echo "dumb init setup"
echo $ADMIN_PASSWORD | sudo -S wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.5/dumb-init_1.2.5_x86_64
echo $ADMIN_PASSWORD | sudo -S chmod +x /usr/local/bin/dumb-init

echo "setup complete"
