
isExistProcess() {
    PORT=$1
    proc=$(lsof -t -i:$PORT)
    if [ -z "$proc" ]; then
        echo "No"
    else
        echo $proc
    fi
}

killProcessByPort() {
    PORT=$1
    proc=$(lsof -t -i:$PORT)
    if [ -z "$proc" ]; then
        echo "Not found"
    else
        kill -9 $proc
        echo $proc
    fi
}
