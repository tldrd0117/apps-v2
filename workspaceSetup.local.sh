
echo "setup start"
echo "base setup"

directory="/Users/iseongjae/Documents/projects/apps-v2/"

mkdir -p "$directory/apps"
mkdir -p "$directory/apps/database/data"
mkdir -p "$directory/apps/webdriver/downloads"

cd "$directory"
echo "directory create"

pyenv virtualenv 3.9.7 collect
eval "pyenv activate collect"

curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
eval 'export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm'
nvm install 18.17.0
nvm use 18.17.0

echo "logsite setup"

envsubst < $directory/apps/containers/namespaces.yaml | kubectl apply -f -
kubectl config set-context --current --namespace=log-site

envsubst < $directory/apps/containers/mongodb-oper.yaml | kubectl delete -f -
envsubst < $directory/apps/containers/mongodb-express.yaml | kubectl delete -f -
envsubst < $directory/apps/containers/webdriver.yaml | kubectl delete -f -

./node_modules/.bin/dotenv -o -e .env.local -- envsubst < $directory/apps/containers/mongodb-oper.yaml | kubectl apply -f -
./node_modules/.bin/dotenv -o -e .env.local -- envsubst < $directory/apps/containers/mongodb-express.yaml | kubectl apply -f -
./node_modules/.bin/dotenv -o -e .env.local -- envsubst < $directory/apps/containers/webdriver.yaml | kubectl apply -f -

echo "airflow setup"

source '$directory/apps-v2/utils/process.sh'
result=$(killProcessByPort 30052)
FILE=$directory/apps/task-airflow/get_helm.sh

curl -fsSL -o $FILE https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 $directory/apps/task-airflow/get_helm.sh
sudo ./$directory/task-airflow/get_helm.sh

helm uninstall airflow -n airflow
kubectl config set-context --current --namespace=airflow

envsubst < $directory/apps/containers/airflow-pvc.yaml | kubectl delete -f -
envsubst < $directory/apps/containers/airflow-pv.yaml | kubectl delete  -f -
envsubst < $directory/apps/containers/airflow.yaml | kubectl delete  -f -

./node_modules/.bin/dotenv -o -e .env.local -- envsubst < $directory/apps/containers/airflow-pv.yaml | kubectl apply -f -
./node_modules/.bin/dotenv -o -e .env.local -- envsubst < $directory/apps/containers/airflow-pvc.yaml | kubectl apply  -f -
./node_modules/.bin/dotenv -o -e .env.local -- envsubst < $directory/apps/containers/airflow.yaml | kubectl apply  -f -
helm repo add apache-airflow https://airflow.apache.org
helm repo update
helm upgrade --install airflow apache-airflow/airflow -f $directory/apps/task-airflow/values.yaml --namespace airflow --create-namespace

echo "dumb init setup"
echo $ADMIN_PASSWORD | sudo -S wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.5/dumb-init_1.2.5_x86_64
echo $ADMIN_PASSWORD | sudo -S chmod +x /usr/local/bin/dumb-init

echo "setup complete"
