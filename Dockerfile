FROM python:3.9-alpine as base
WORKDIR /usr/src/app
COPY ./apps ./apps
COPY ./packages ./packages
COPY ./.env ./.env